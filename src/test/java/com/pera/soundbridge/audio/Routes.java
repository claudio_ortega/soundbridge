/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio;

import com.pera.soundbridge.audio.filter.ISignalInPlaceFilter;
import com.pera.soundbridge.audio.sink.ISink;
import com.pera.soundbridge.audio.sink.sim.VesselSequenceChecker;
import com.pera.soundbridge.audio.source.ISource;
import com.pera.soundbridge.audio.util.AudioLineUtils;
import com.pera.util.TaggedThreadFactory;
import com.pera.soundbridge.vessel.*;
import junit.framework.TestCase;
import org.apache.logging.log4j.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

class Routes
{
    private final Logger logger = LogManager.getLogger();
    private final List<Route> routes;
    private final long durationMSec;

    static class Route
    {
        Route(
            String iName,
            ISignalInPlaceFilter inFilter,
            ISource iSource,
            List<ISink> iSinks)
        {
            name = iName;
            filter = inFilter;
            source = iSource;
            sinks = iSinks;
            checker = VesselSequenceChecker.create ( iName, VesselType.Signal, 50 );
        }

        private final String name;
        private final ISource source;
        private final ISignalInPlaceFilter filter;
        private final List<ISink> sinks;
        private final VesselSequenceChecker checker;
    }

    static Routes create (
        final List<Route> routes,
        final int duration,
        final TimeUnit timeunit)
    {
        return new Routes(routes, duration, timeunit );
    }

    private Routes (
        final List<Route> inRoutes,
        final int duration,
        final TimeUnit timeunit)
    {
        durationMSec = timeunit.toMillis((long)duration);
        routes = inRoutes;
    }

    void execute() throws Exception
    {
        final AtomicBoolean cancel = new AtomicBoolean ( false );
        final CountDownLatch latch = new CountDownLatch ( routes.size() + 1 );

        final ThreadPoolExecutor executor = new ThreadPoolExecutor(
            routes.size()+1,
            routes.size()+1,
            0,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>( ),
            TaggedThreadFactory.create( getClass(), "1" ) );

        for (Route route : routes)
        {
            route.source.init();
            for ( ISink sink : route.sinks )
            {
                sink.init ();
            }
        }

        for (Route route : routes)
        {
            final Runnable routeRunnable =
            () ->
            {
                while ( !cancel.get() )
                {
                    try {
                        final Vessel vessel = route.source.produce(2, TimeUnit.SECONDS);

                        if (vessel != null)
                        {
                            route.checker.accept ( vessel );

                            route.filter.filter ( vessel );

                            for (ISink sink : route.sinks)
                            {
                                sink.consume(vessel);
                            }
                        }
                    }
                    catch ( Exception e )
                    {
                        logger.warn ( e.getMessage () );
                        logger.debug ( e.getMessage (), e );
                    }
                }

                latch.countDown ();
            };

            executor.submit( routeRunnable );
        }

        final Runnable timerCheckRunnable = () ->
        {
            try
            {
                Thread.sleep( 1_000 );

                for (Route route : routes)
                {
                    route.source.start();
                }

                Thread.sleep ( durationMSec );

                for (Route route : routes)
                {
                    route.source.stop();
                }
            }
            catch ( Exception e )
            {
                logger.warn ( e.getMessage () );
                logger.debug ( e.getMessage (), e );
            }
            finally
            {
                cancel.set ( true );
                latch.countDown();
            }
        };

        executor.submit( timerCheckRunnable );

        final boolean timeoutA = !latch.await ( 2L * durationMSec, TimeUnit.MILLISECONDS );
        logger.info ( "latch.await() timeoutA: " + timeoutA );

        executor.shutdownNow ( );
    }

    public void close() throws Exception {
        for (Route route : routes)
        {
            route.source.close();
            for ( ISink sink : route.sinks )
            {
                sink.close ();
            }
        }
    }

    void report( boolean validate )
    {
        routes.forEach(
            x -> iReport( x, validate )
        );
    }

    private void iReport ( final Route route, boolean validate ) {
        final Map<String, Number> counters = new HashMap<>();

        logger.info("route name: " + route.name);
        logger.info("  source description: " + route.source.getId());
        logger.info("  source produced samples: " + route.source.getProducedSourceSamplesCount());
        logger.info("  source dropped samples: " + route.source.getDroppedSourceSamplesCount());
        logger.info("  total vessels: " + route.checker.getTotalCount());
        logger.info("  out of sequence vessels: " + route.checker.getVesselOutOfSequenceCount());

        if (validate) {
            TestCase.assertEquals(
                "out of sequence vessels count is not zero",
                0, route.checker.getVesselOutOfSequenceCount());
            TestCase.assertTrue(
                "total vessels count is zero",
                route.checker.getTotalCount() > 0);
            TestCase.assertTrue(
                "source sample count is zero",
                route.source.getProducedSourceSamplesCount() > 0);
            TestCase.assertEquals(
                "source dropped count is not zero: " + route.source.getDroppedSourceSamplesCount(),
                0, route.source.getDroppedSourceSamplesCount());
        }

        route.sinks.forEach(
            sink ->
            {
                sink.getCounters(counters);

                logger.info("  sink description: " + sink.getId());
                logger.info("    sink consumed samples: " + sink.getConsumedSinkSamplesCount());
                logger.info("    sink dropped samples: " + sink.getDroppedSinkSamplesCount());

                if (validate) {
                    TestCase.assertTrue(
                        "consumed count is zero",
                        sink.getConsumedSinkSamplesCount() > 0);
                    TestCase.assertEquals(
                        "sink dropped count is not zero: " + sink.getDroppedSinkSamplesCount(),
                        0, route.source.getDroppedSourceSamplesCount());
                }
            }
        );

        route.source.getCounters(counters);
        route.checker.getCounters(counters);
        route.sinks.forEach( sink -> sink.getCounters(counters) );

        logger.info( "  counters ----" );
        AudioLineUtils.printCounters(
            counters,
            (Map.Entry<String,Number> x) -> logger.info( "    {}:{}", () -> x.getKey(), () -> x.getValue() )
        );
    }
}

