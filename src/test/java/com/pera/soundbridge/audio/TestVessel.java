/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio;

import com.pera.TestCommon;
import com.pera.soundbridge.vessel.Vessel;
import junit.framework.TestCase;
import org.apache.logging.log4j.*;
import org.junit.Test;

public class TestVessel extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Test
    public void test1()
    {
        final Vessel vesselA = Vessel.createFromAudioBuffer(
            "",
            456546756756L,
            99,
            2,
            2,
            new byte[] {
                1,1, // first left sample    (257)
                1,2, // first right sample   (258)
                1,3, // second left sample   (259)
                1,4  // second right sample  (260)
            },
            0,
            8
        );

        final byte[] bufferA = vesselA.produceUDPBuffer ();
        final Vessel vesselB = Vessel.createFromUDPBuffer(bufferA,0,bufferA.length);

        logger.info(String.format("vesselA:%s", vesselA));
        logger.info(String.format("vesselB:%s", vesselB));
        TestCase.assertEquals(vesselA.toString(), vesselB.toString());
    }
}