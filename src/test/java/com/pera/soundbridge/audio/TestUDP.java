/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio;

import com.pera.TestCommon;
import com.pera.soundbridge.audio.common.FSEnum;
import com.pera.soundbridge.audio.filter.NullInPlaceFilter;
import com.pera.soundbridge.net.EndpointBuilder;
import com.pera.soundbridge.net.UDPAdaptor;
import com.pera.soundbridge.audio.sink.ISink;
import com.pera.soundbridge.audio.sink.sim.DebugSink;
import com.pera.soundbridge.audio.source.ISource;
import com.pera.soundbridge.audio.source.sim.SimulatedSourceBuilder;
import com.pera.soundbridge.audio.source.sim.SimulatedSource;
import com.pera.util.CollectionsUtil;
import org.junit.Test;
import org.apache.logging.log4j.*;
import java.util.concurrent.TimeUnit;

public class TestUDP extends TestCommon
{
    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void test00() throws Exception
    {
        final ISource simulatorSource = SimulatedSource.create (
            SimulatedSourceBuilder
                .init ( )
                .setId( "simulated-source-1" )
                .setNumberOfChannels ( 2 )
                .setSampleSizeInBytes ( 2 )
                .setSamplingFrequencyEnum ( FSEnum.value8000 )
                .setDc ( 0.0 )
                .setGain ( 0.5 )
                .setSignalFrequency( 220.0 )
                .setBufferLengthInMSec( 50 )
                .setMaximumEnqueuedBuffers( 20 )
                .create ( ) );

        final ISink debugSink = DebugSink.create ( "debug-sink", 50 );

        final UDPAdaptor udpAdaptor = UDPAdaptor.create (
            // source
            EndpointBuilder
                .init ( )
                .setId( "udp-endpoint-source" )
                .setMaximumEnqueuedBuffers( 100 )
                .setIpAddress ( "localhost" )
                .setPort ( 1730 )
                .create ( ),
            // sink
            EndpointBuilder
                .init ( )
                .setId( "udp-endpoint-sink" )
                .setIpAddress ( "localhost" )
                .setPort ( 1730 )
                .create ( )
        );

        final Routes r = Routes.create (
            CollectionsUtil.createList(
                new Routes.Route(
                    "route #1",
                    NullInPlaceFilter.create(),
                    simulatorSource,
                    CollectionsUtil.createList(udpAdaptor) ),
                new Routes.Route(
                    "route #2",
                    NullInPlaceFilter.create(),
                        udpAdaptor,
                    CollectionsUtil.createList(debugSink) )
            ),
            3,
            TimeUnit.SECONDS );

        r.execute ();
        r.close();
        r.report(true);
    }
}

