/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio;

import com.google.common.base.Preconditions;
import com.pera.TestCommon;
import com.pera.soundbridge.audio.common.AudioCommon;
import com.pera.soundbridge.audio.common.FSEnum;
import com.pera.soundbridge.audio.sink.ISink;
import com.pera.soundbridge.audio.sink.sim.DebugSink;
import com.pera.soundbridge.audio.source.ISource;
import com.pera.soundbridge.audio.sink.hw.AudioSink;
import com.pera.soundbridge.audio.sink.hw.AudioSinkBuilder;
import com.pera.soundbridge.audio.sink.sim.FileSink;
import com.pera.soundbridge.audio.source.sim.SimulatedSource;
import com.pera.soundbridge.audio.source.sim.SimulatedSourceBuilder;
import com.pera.soundbridge.audio.common.DriversInfo;
import com.pera.soundbridge.main.TestingMain;
import com.pera.soundbridge.restinfo.SoundBridgeConfig;
import com.pera.util.*;
import org.apache.logging.log4j.*;
import org.junit.Test;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestAudioSink extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void testALlHw()
    {
        final int status = TestingMain.main( "--list-all" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }

    @Test
    public void testCompatHw()
    {
        final int status = TestingMain.main( "--list-all" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }

    @Test
    public void testListAllDevices()
    {
        final DriversInfo info = AudioCommon.fetchDriversInfoFromHardware( false, false, SoundBridgeConfig.create());
        logger.info("info: {}", GsonUtil.serialize( info ) );

        final List<String> lDeviceList = AudioCommon.getAllLines( info );
        lDeviceList.forEach( e -> logger.info("e:{}", e ));
        Preconditions.checkState( lDeviceList.size() > 0 );
    }

    @Test
    public void testListAllCompatibleDevices()
    {
        final DriversInfo info = AudioCommon.fetchDriversInfoFromHardware( true, true, SoundBridgeConfig.create());
        logger.info("info: {}", GsonUtil.serialize( info ) );

        final List<String> lDeviceList = AudioCommon.getDescriptions( info.getCompatibleSinkMixers() );
        lDeviceList.forEach( e -> logger.info("e:{}", e ));
        Preconditions.checkState( lDeviceList.size() > 0 );
    }

    @Test
    public void testSuggestedDevice()
    {
        final DriversInfo info = AudioCommon.fetchDriversInfoFromHardware( true, true, SoundBridgeConfig.create());
        logger.info("info: {}", GsonUtil.serialize( info ) );

        final List<String> suggested = AudioCommon.getSuggestedHashes( info, "" );
        logger.info("suggested: {}", suggested);
    }

    @Test
    public void testSendSamplesToHw() throws Exception
    {
        final ISource lSignalSource = SimulatedSource.create (
            SimulatedSourceBuilder
                .init ( )
                .setId("sim")
                .setNumberOfChannels ( 1 )
                .setSampleSizeInBytes ( 2 )
                .setSamplingFrequencyEnum ( FSEnum.value8000 )
                .setDc ( 0.0 )
                .setGain ( 1.0 )
                .setSignalFrequency( 50.0 )
                .setSquareModulationFrequency( 1.0 )
                .setSquareModulationDutyCycle( 0.5 )
                .setBufferLengthInMSec( 20 )
                .setMaximumEnqueuedBuffers( 40 )
                .create ( ) );

        final ISink fileSink = FileSink.create (getClass().getSimpleName() );

        final ISink debugSink = DebugSink.create (getClass().getSimpleName(), 50 );

        final DriversInfo driversInfo = AudioCommon.fetchDriversInfoFromHardware( false, true, SoundBridgeConfig.create());
        logger.info("info: {}", GsonUtil.serialize( driversInfo ) );

        final List<String> sinkHashes = AudioCommon.getHashes( driversInfo.getCompatibleSinkMixers() );
        logger.info("compatible sources hashes: {}", sinkHashes );

        Preconditions.checkArgument( sinkHashes.size() > 0 );

        final ISink hwSink = AudioSink.create (
            AudioSinkBuilder
                .init ( )
                .setId("hw-sink")
                .setNumberOfChannels ( 1 )
                .setSamplingFrequencyEnum ( FSEnum.value8000 )
                .setMaximumEnqueuedBuffers(50)
                .setAudioMixerDescriptionRegexp ( sinkHashes.get(0) )
                .create ( ) );

        final Routes r = Routes.create (
            CollectionsUtil.createList(
                new Routes.Route(
                    "route #1",
                    v -> {},
                    lSignalSource,
                    CollectionsUtil.createList(fileSink, hwSink, debugSink))
            ),
            2,
            TimeUnit.SECONDS );

        r.execute ();
        r.close();
        r.report(true);
    }
}

