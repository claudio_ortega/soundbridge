/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio;

import com.pera.TestCommon;
import com.pera.soundbridge.audio.common.FSEnum;
import com.pera.soundbridge.audio.filter.NullInPlaceFilter;
import com.pera.soundbridge.audio.sink.ISink;
import com.pera.soundbridge.audio.sink.sim.DebugSink;
import com.pera.soundbridge.audio.sink.sim.FileSink;
import com.pera.soundbridge.audio.source.ISource;
import com.pera.soundbridge.audio.source.sim.SimulatedSourceBuilder;
import com.pera.soundbridge.audio.source.sim.SimulatedSource;
import com.pera.util.CollectionsUtil;
import org.junit.Test;
import org.apache.logging.log4j.*;


import java.util.concurrent.TimeUnit;

public class TestSim extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel() {
        return Level.DEBUG;
    }

    @Test
    public void testReadSamplesFromSimulator() throws Exception
    {
        logger.info("testReadSamplesFromSimulator -- START");

        final ISource lSimSource = SimulatedSource.create(
            SimulatedSourceBuilder
                .init()
                .setNumberOfChannels(2)
                .setSampleSizeInBytes(1)
                .setSamplingFrequencyEnum(FSEnum.value8000)
                .setDc(0.0)
                .setGain(0.5)
                .setSignalFrequency(220.0)
                .setSquareModulationFrequency(1.0)
                .setBufferLengthInMSec( 20 )
                .setMaximumEnqueuedBuffers( 50 )
                .create());

        final ISink lDebugSink = DebugSink.create("1", 50);

        final ISink fileSink = FileSink.create (getClass().getSimpleName() );

        final Routes r = Routes.create (
            CollectionsUtil.createList(
                new Routes.Route(
                "route #1",
                NullInPlaceFilter.create(),
                lSimSource,
                CollectionsUtil.createList(fileSink, lDebugSink) )
            ),
            5,
            TimeUnit.SECONDS );

        r.execute ();
        r.close();
        r.report(true);
    }
}
