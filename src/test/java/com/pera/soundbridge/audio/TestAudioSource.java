/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio;

import com.google.common.base.Preconditions;
import com.pera.TestCommon;
import com.pera.soundbridge.audio.common.AudioCommon;
import com.pera.soundbridge.audio.common.FSEnum;
import com.pera.soundbridge.audio.filter.NullInPlaceFilter;
import com.pera.soundbridge.audio.sink.ISink;
import com.pera.soundbridge.audio.source.ISource;
import com.pera.soundbridge.audio.sink.sim.DebugSink;
import com.pera.soundbridge.audio.sink.sim.FileSink;
import com.pera.soundbridge.audio.source.hw.*;
import com.pera.soundbridge.audio.common.DriversInfo;
import com.pera.soundbridge.restinfo.SoundBridgeConfig;
import com.pera.util.*;
import org.apache.logging.log4j.*;
import org.junit.Test;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestAudioSource extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void testListAllDevices()
    {
        final DriversInfo info = AudioCommon.fetchDriversInfoFromHardware( true, false, SoundBridgeConfig.create());
        logger.info("info: {}", GsonUtil.serialize( info ) );

        final List<String> lDeviceList = AudioCommon.getAllLines( info );
        lDeviceList.forEach( e -> logger.info("e:{}", e ));
        Preconditions.checkState( lDeviceList.size() > 0 );
    }

    @Test
    public void testListAllCompatibleDevices()
    {
        final DriversInfo info = AudioCommon.fetchDriversInfoFromHardware( true, false, SoundBridgeConfig.create());
        logger.info("info: {}", GsonUtil.serialize( info ) );

        final List<String> lDeviceList = AudioCommon.getDescriptions( info.getCompatibleSourceMixers() );
        lDeviceList.forEach( e -> logger.info("e:{}", e ));
        Preconditions.checkState( lDeviceList.size() > 0 );
    }

    @Test
    public void testSuggestedDevice()
    {
        final DriversInfo info = AudioCommon.fetchDriversInfoFromHardware( true, true, SoundBridgeConfig.create());
        logger.info("info: {}", GsonUtil.serialize( info ) );

        final List<String> suggested = AudioCommon.getSuggestedHashes( info, "" );
        logger.info("suggested: {}", suggested);
    }

    @Test
    public void testReadSamplesFromHw() throws Exception
    {
        final DriversInfo driversInfo = AudioCommon.fetchDriversInfoFromHardware( true, true, SoundBridgeConfig.create());
        logger.info("info: {}", GsonUtil.serialize( driversInfo ) );

        final List<String> suggested = AudioCommon.getSuggestedHashes( driversInfo, "" );
        logger.info("suggested: {}", suggested);

        Preconditions.checkState( suggested.size() > 0 );

        final ISource lSignalSource = AudioSource.create(
            AudioSourceBuilder
                .init().setId("audio-hw")
                .setNumberOfChannels(1)
                .setSamplingFrequencyEnum(FSEnum.value8000)
                .setAudioMixerDescriptionRegexp(suggested.get(0))
                .setBufferLengthInMSec( 20 )
                .setMaximumEnqueuedBuffers( 200 )
                .create());

        final ISink lDebugSink = DebugSink.create("1", 50);

        final ISink fileSink = FileSink.create (getClass().getSimpleName() );

        final Routes r = Routes.create (
            CollectionsUtil.createList(
                new Routes.Route(
                    "router #1",
                    NullInPlaceFilter.create (),
                    lSignalSource,
                    CollectionsUtil.createList(fileSink, lDebugSink) )
            ),
            2,
            TimeUnit.SECONDS );

        r.execute ();
        r.close();
        r.report(true);

        logger.info("testReadSamplesFromHw -- END");
    }
}
