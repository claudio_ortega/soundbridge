/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio;

import com.pera.TestCommon;
import com.pera.soundbridge.net.LineCodec;
import com.pera.soundbridge.vessel.Vessel;
import junit.framework.TestCase;
import org.apache.logging.log4j.*;
import org.junit.Test;
import java.util.LinkedList;
import java.util.List;

public class TestLineCodec extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    private final static Vessel vesselA1 = Vessel.createFromAudioBuffer(
        "",
        99999999999999L,
        99,
        2,
        2,
        new byte[] {
            1,1, // first left sample    (257)
            1,2, // first right sample   (258)
            1,3, // second left sample   (259)
            1,4  // second right sample  (260)
        },
        0,
        8
    );

    private final static Vessel vesselA2 = Vessel.createFromAudioBuffer(
        "",
        99999999999999L,
        86,
        2,
        2,
        new byte[] {
            10,10, // first left sample
            10,12, // first right sample
            10,13, // second left sample
            10,14  // second right sample
        },
        0,
        8
    );

    private static LineCodec.SerializerDeserializer<Vessel> iGetSerializerDeserializer() {
        return new LineCodec.SerializerDeserializer<Vessel>() {
            @Override
            public Vessel deserialize(byte[] buffer, int startIndex, int length) {
                return Vessel.createFromUDPBuffer( buffer, startIndex, length );
            }

            @Override
            public boolean isNull(Vessel in) {
                return in.isNull();
            }

            @Override
            public byte[] serialize(Vessel in) {
                return in.produceUDPBuffer();
            }
        };
    }

    @Test
    public void test1A()
    {
        final List<Vessel> vesselList = new LinkedList<> ();

        {
            final byte[] bufferA1 = LineCodec.create("1", 2).constructBufferFromPayload(
                vesselA1,
                iGetSerializerDeserializer()
            );
            
            LineCodec.create("1", 2).extractPayloadsFromBuffer (
                bufferA1,
                bufferA1.length,
                vesselList,
                iGetSerializerDeserializer()
            );

            TestCase.assertEquals(1, vesselList.size());
            final Vessel vesselB = vesselList.get(0);
            logger.info(String.format("vesselB:%s", vesselB));
            TestCase.assertEquals(vesselA1.toString(), vesselB.toString());
        }
    }

    @Test
    public void test1B()
    {
        final List<Vessel> vesselList = new LinkedList<> ();

        {
            final byte[] bufferA1 = LineCodec.create("1", 2).constructBufferFromPayload(
                vesselA1,
                iGetSerializerDeserializer()
            );

            LineCodec.create("1", 2).extractPayloadsFromBuffer (
                bufferA1,
                bufferA1.length,
                vesselList,
                iGetSerializerDeserializer()
            );

            TestCase.assertEquals(1, vesselList.size());
            final Vessel vesselB1 = vesselList.get(0);
            TestCase.assertEquals(vesselA1.toString(), vesselB1.toString());
        }

        {
            final byte[] bufferA2 = LineCodec.create("1", 2).constructBufferFromPayload(
                vesselA2,
                iGetSerializerDeserializer()
            );

            LineCodec.create("1", 2).extractPayloadsFromBuffer (
                bufferA2,
                bufferA2.length,
                vesselList,
                iGetSerializerDeserializer()
            );

            TestCase.assertEquals(2, vesselList.size());
            final Vessel vesselB2 = vesselList.get(1);
            TestCase.assertEquals(vesselA2.toString(), vesselB2.toString());
        }
    }

    @Test
    public void test2AB()
    {
        final byte[] bufferA1 = LineCodec.create("1", 2).constructBufferFromPayload(
            vesselA1,
            iGetSerializerDeserializer()
        );

        final byte[] bufferA2 = LineCodec.create("1", 2).constructBufferFromPayload(
            vesselA2,
            iGetSerializerDeserializer()
        );

        final byte[] bufferA3 = concatArrays ( new byte[][] { bufferA1, bufferA2 } );

        iTestSplittingInN( bufferA3, 1 );
    }

    @Test
    public void test2ABSplitIn2()
    {
        final byte[] bufferA1 = LineCodec.create("1", 2).constructBufferFromPayload(
            vesselA1,
            iGetSerializerDeserializer()
        );

        final byte[] bufferA2 = LineCodec.create("1", 2).constructBufferFromPayload(
            vesselA2,
            iGetSerializerDeserializer()
        );

        final byte[] bufferA3 = concatArrays ( new byte[][] { bufferA1, bufferA2 } );
        iTestSplittingInN( bufferA3, 2 );
    }

    @Test
    public void test2ABSplitIn3()
    {
        final byte[] bufferA1 = LineCodec.create("1", 2).constructBufferFromPayload(
            vesselA1,
            iGetSerializerDeserializer()
        );

        final byte[] bufferA2 = LineCodec.create("1", 2).constructBufferFromPayload(
            vesselA2,
            iGetSerializerDeserializer()
        );

        final byte[] bufferA3 = concatArrays ( new byte[][] { bufferA1, bufferA2 } );
        iTestSplittingInN( bufferA3, 3 );
    }

    @Test
    public void test2ABSplitInAllWays()
    {
        final byte[] bufferA1 = LineCodec.create("1", 2).constructBufferFromPayload(
            vesselA1,
            iGetSerializerDeserializer()
        );

        final byte[] bufferA2 = LineCodec.create("1", 2).constructBufferFromPayload(
            vesselA2,
            iGetSerializerDeserializer()
        );

        final byte[] bufferA3 = concatArrays ( new byte[][] { bufferA1, bufferA2 } );

        for ( int i=4; i<bufferA3.length/2; i++ )
        {
            iTestSplittingInN( bufferA3, i );
        }
    }

    @Test
    public void testWithZerosInside()
    {
        final byte[] bufferA1 = LineCodec.create("1", 2).constructBufferFromPayload(
            vesselA1,
            iGetSerializerDeserializer()
        );

        final byte[] bufferA2 = LineCodec.create("1", 2).constructBufferFromPayload(
            vesselA2,
            iGetSerializerDeserializer()
        );

        byte[] bufferA4 = concatArrays ( new byte[][] { bufferA1, new byte[40], bufferA2 } );
        iTestSplittingInN( bufferA4, 1 );
    }

    private void iTestSplittingInN(byte[] buff, int n )
    {
        final LineCodec lc = LineCodec.create ("1", 2);
        final List<Vessel> vesselList = new LinkedList<> ();

        final byte[][] splitArray = splitArray(buff, n );

        for ( byte[] buffer : splitArray )
        {
            lc.extractPayloadsFromBuffer (
                buffer,
                buffer.length,
                vesselList,
                iGetSerializerDeserializer()
            );
        }

        TestCase.assertEquals ( 2, vesselList.size () );

        final Vessel vesselB1 = vesselList.get(0);
        TestCase.assertEquals ( vesselA1.toString (), vesselB1.toString () );

        final Vessel vesselB2 = vesselList.get(1);
        TestCase.assertEquals ( vesselA2.toString (), vesselB2.toString () );
    }

    private static byte[] concatArrays ( byte[][] x )
    {
        int totalLength = 0;
        for ( byte[] n : x )
        {
            totalLength += n.length;
        }

        final byte r[] = new byte[totalLength];

        int currentLength = 0;
        for ( byte[] n : x )
        {
            System.arraycopy( n, 0, r, currentLength, n.length );
            currentLength += n.length;
        }

        return r;
    }

    private static byte[][] splitArray ( byte[] in, int n )
    {
        final int splitN = in.length / n;

        final byte[][] ret = new byte[n][];

        for (int i=0; i<n-1; i++)
        {
            ret[i] = new byte[splitN];
            System.arraycopy( in, i*splitN, ret[i], 0, splitN );
        }

        final int lastLength = in.length - (n-1) * splitN;

        ret[n-1] = new byte[lastLength];
        System.arraycopy( in, (n-1)*splitN, ret[n-1], 0, lastLength );

        return ret;
    }
}

