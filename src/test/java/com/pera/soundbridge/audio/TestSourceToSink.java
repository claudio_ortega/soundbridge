/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio;

import com.pera.TestCommon;
import com.pera.soundbridge.audio.common.AudioCommon;
import com.pera.soundbridge.audio.common.FSEnum;
import com.pera.soundbridge.audio.sink.ISink;
import com.pera.soundbridge.audio.sink.hw.AudioSink;
import com.pera.soundbridge.audio.sink.hw.AudioSinkBuilder;
import com.pera.soundbridge.audio.source.ISource;
import com.pera.soundbridge.audio.source.hw.*;
import com.pera.soundbridge.audio.common.DriversInfo;
import com.pera.soundbridge.restinfo.SoundBridgeConfig;
import com.pera.util.CollectionsUtil;
import org.junit.Ignore;
import org.junit.Test;
import org.apache.logging.log4j.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestSourceToSink extends TestCommon
{
    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Ignore
    @Test
    public void testHwToHw() throws Exception
    {
        final DriversInfo info = AudioCommon.fetchDriversInfoFromHardware( true, true, SoundBridgeConfig.create());
        final List<String> suggested = AudioCommon.getSuggestedHashes( info, "" );

        final ISource lSignalSource = AudioSource.create(
            AudioSourceBuilder
                .init().setId("audio-hw")
                .setNumberOfChannels ( 1 )
                .setSamplingFrequencyEnum ( FSEnum.value8000 )
                .setAudioMixerDescriptionRegexp ( suggested.get(0) )
                .setMaximumEnqueuedBuffers( 100 )
                .setBufferLengthInMSec( 20 )
                .create());

        final ISink hwSink = AudioSink.create (
            AudioSinkBuilder
                .init ( )
                .setId("hw-sink")
                .setNumberOfChannels ( 1 )
                .setSamplingFrequencyEnum ( FSEnum.value8000 )
                .setAudioMixerDescriptionRegexp ( suggested.get(0) )
                .setMaximumEnqueuedBuffers( 100 )
                .create ( ) );

        final Routes r = Routes.create (
            CollectionsUtil.createList(
                new Routes.Route(
                    "route #1",
                    v -> {},
                    lSignalSource,
                    CollectionsUtil.createList( hwSink ) )
            ),
            5,
            TimeUnit.SECONDS );

        r.execute ();
        r.close();
        r.report(true);
    }
}

