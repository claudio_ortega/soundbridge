/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio;

import com.pera.TestCommon;
import com.pera.soundbridge.audio.util.AudioLineUtils;
import junit.framework.TestCase;
import org.junit.Test;
import org.apache.logging.log4j.*;

public class TestAudioLineUtils extends TestCommon
{
    @Override
    protected Level getLoggingLevel() {
        return Level.DEBUG;
    }

    @Test
    public void testByteConversions()
    {
        TestCase.assertEquals ( 0, AudioLineUtils.getIntFromBytes ( ( byte ) 0, ( byte ) 0 ) );
        TestCase.assertEquals ( ( byte ) 0, AudioLineUtils.getMsbFromInt (0) );
        TestCase.assertEquals ( ( byte ) 0, AudioLineUtils.getLsbFromInt (0) );

        TestCase.assertEquals ( 1, AudioLineUtils.getIntFromBytes ( ( byte ) 0, ( byte ) 1 ) );
        TestCase.assertEquals ( ( byte ) 0, AudioLineUtils.getMsbFromInt (1) );
        TestCase.assertEquals ( ( byte ) 1, AudioLineUtils.getLsbFromInt (1) );

        TestCase.assertEquals ( 256, AudioLineUtils.getIntFromBytes ( ( byte ) 1, ( byte ) 0 ) );
        TestCase.assertEquals ( ( byte ) 1, AudioLineUtils.getMsbFromInt (256) );
        TestCase.assertEquals ( ( byte ) 0, AudioLineUtils.getLsbFromInt (256) );

        TestCase.assertEquals ( 257, AudioLineUtils.getIntFromBytes ( ( byte ) 1, ( byte ) 1 ) );
        TestCase.assertEquals ( ( byte ) 1, AudioLineUtils.getMsbFromInt (257) );
        TestCase.assertEquals ( ( byte ) 1, AudioLineUtils.getLsbFromInt (257) );

        TestCase.assertEquals ( 0x3e8, AudioLineUtils.getIntFromBytes ( ( byte ) 0x03, ( byte ) 0xe8 ) );
        TestCase.assertEquals ( ( byte ) 0x03, AudioLineUtils.getMsbFromInt (0x3e8) );
        TestCase.assertEquals ( ( byte ) 0xe8, AudioLineUtils.getLsbFromInt (0x3e8) );

        TestCase.assertEquals ( -1, AudioLineUtils.getIntFromBytes ( ( byte ) 0xff, ( byte ) 0xff ) );
        TestCase.assertEquals ( ( byte ) 0xff, AudioLineUtils.getMsbFromInt (-1) );
        TestCase.assertEquals ( ( byte ) 0xff, AudioLineUtils.getLsbFromInt (-1) );

        TestCase.assertEquals ( -2, AudioLineUtils.getIntFromBytes ( ( byte ) 0xff, ( byte ) 0xfe ) );
        TestCase.assertEquals ( ( byte ) 0xff, AudioLineUtils.getMsbFromInt (-2) );
        TestCase.assertEquals ( ( byte ) 0xfe, AudioLineUtils.getLsbFromInt (-2) );
    }
}

