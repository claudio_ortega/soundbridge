/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.station;

import com.pera.TestCommon;
import com.pera.soundbridge.audio.common.FSEnum;
import com.pera.soundbridge.net.EndpointBuilder;
import com.pera.soundbridge.audio.sink.ISink;
import com.pera.soundbridge.audio.sink.sim.DebugSink;
import com.pera.soundbridge.audio.source.ISource;
import com.pera.soundbridge.audio.source.sim.SimulatedSourceBuilder;
import com.pera.soundbridge.audio.source.sim.SimulatedSource;
import com.pera.soundbridge.audio.util.AudioLineUtils;
import com.pera.soundbridge.vessel.*;
import com.pera.util.CollectionsUtil;
import org.junit.Test;
import org.apache.logging.log4j.*;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TestStations extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void test00() throws Exception
    {
        final ISource operatorSideSource = SimulatedSource.create(
            SimulatedSourceBuilder
                .init()
                .setId("operator")
                .setNumberOfChannels(1)
                .setSampleSizeInBytes(2)
                .setSamplingFrequencyEnum(FSEnum.value8000)
                .setDc(0.0)
                .setGain(0.5)
                .setSignalFrequency(220.0)
                .setBufferLengthInMSec(50)
                .setMaximumEnqueuedBuffers( 50 )
                .setSquareModulationFrequency(1)
                .create()
        );

        final ISink operatorSideSink = DebugSink.create("operator", 50);

        final Station operatorSideStation = Station.create(
            "",
            StationBuilder.StationTypeEnum.os,
            operatorSideSource,
            // Sink
            EndpointBuilder
                .init()
                .setId("operator")
                .setIpAddress("")
                .setPort(1731)
                .create(),
            // Source
            EndpointBuilder
                .init()
                .setId("operator")
                .setMaximumEnqueuedBuffers(20)
                .setIpAddress("127.0.0.1")
                .setPort(1730)
                .create(),
            CollectionsUtil.createList(operatorSideSink),
            v -> v,
            v -> v,
            v -> v,
            v -> Vessel.NULL,
            v -> Vessel.NULL,
            1
        );

        operatorSideStation.init();
        operatorSideStation.start();

        // ------------------------------------------
        final ISource vhfSideSource = SimulatedSource.create(
            SimulatedSourceBuilder
                .init()
                .setId("vhf")
                .setNumberOfChannels(1)
                .setSampleSizeInBytes(2)
                .setSamplingFrequencyEnum(FSEnum.value8000)
                .setDc(0.0)
                .setGain(0.5)
                .setSignalFrequency(220.0)
                .setBufferLengthInMSec(50)
                .setMaximumEnqueuedBuffers(20)
                .setSquareModulationFrequency(2)
                .create() );

        final ISink vhfSideSink = DebugSink.create("vhf", 50);

        final Station vhfSideStation = Station.create(
            "",
            StationBuilder.StationTypeEnum.vs,
            vhfSideSource,
            // Sink
            EndpointBuilder
                .init()
                .setId("vhf")
                .setIpAddress("127.0.0.1")
                .setPort(1730)
                .create(),
            // Source
            EndpointBuilder
                .init()
                .setId("vhf")
                .setMaximumEnqueuedBuffers(20)
                .setIpAddress("127.0.0.1")
                .setPort(1731)
                .create(),
            CollectionsUtil.createList(vhfSideSink),
            v -> v,
            v -> v,
            v -> v,
            v -> Vessel.NULL,
            v -> Vessel.NULL,
            1
        );

        vhfSideStation.init();
        vhfSideStation.start();

        new Thread(
            () ->
            {
                try
                {
                    logger.info( "start waiting.." );
                    Thread.sleep( TimeUnit.SECONDS.toMillis( 5 ) );
                    logger.info( "stop waiting.." );

                    operatorSideStation.stop();
                    vhfSideStation.stop();
                }
                catch ( Exception ex )
                {
                    logger.info( ex.toString(), ex );
                }
            }
        ).start();

        logger.info( "waiting on the operator side first.." );
        final boolean timedOutA = !operatorSideStation.waitForStop(1, TimeUnit.DAYS);
        logger.info( "operatorSideStation.waitForStop() is over, timedOutA:{}", timedOutA );

        logger.info( "waiting on the vhf side first.." );
        final boolean timedOutB = !vhfSideStation.waitForStop(1, TimeUnit.DAYS);
        logger.info( "vhfSideStation.waitForStop() is over, timedOutB:{}", timedOutB );

        final Map<String, Number> counters = new HashMap<>();
        operatorSideStation.getCounters( counters );
        vhfSideStation.getCounters( counters );

        logger.info( "counters ----" );
        AudioLineUtils.printCounters(
                counters,
                (Map.Entry<String,Number> x) -> logger.info( "    {}:{}", () -> x.getKey(), () -> x.getValue() )
        );

        logger.info( "closing..." );
        operatorSideStation.close();
        operatorSideSource.close();
        operatorSideSink.close();

        vhfSideStation.close();
        vhfSideSource.close();
        vhfSideSink.close();

        logger.info( "done" );
    }
}

