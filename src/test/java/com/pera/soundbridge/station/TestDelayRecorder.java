/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.station;

import com.google.common.base.Preconditions;
import com.pera.*;
import com.pera.soundbridge.vessel.*;
import com.pera.util.*;
import org.junit.*;
import org.apache.logging.log4j.*;

import java.util.concurrent.atomic.AtomicLong;

public class TestDelayRecorder extends TestCommon {
    private final static Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void test00() throws Exception {

        final String originatorId = "12345678";

        final DelayRecorder delayRecorder = DelayRecorder.create(
            VesselType.Probe,
            originatorId,
            System::currentTimeMillis );

        delayRecorder.getFilterA().accept(Vessel.createProbe(originatorId, 1));
        Thread.sleep(500);
        delayRecorder.getFilterB().accept(Vessel.createProbe(originatorId, 1));

        delayRecorder.getFilterA().accept(Vessel.createProbe(originatorId, 2));
        Thread.sleep(500);
        delayRecorder.getFilterB().accept(Vessel.createProbe(originatorId, 2));

        delayRecorder.getFilterA().accept(Vessel.createProbe(originatorId, 3));
        Thread.sleep(502);
        delayRecorder.getFilterB().accept(Vessel.createProbe(originatorId, 3));

        delayRecorder.getFilterA().accept(Vessel.createProbe(originatorId, 4));
        Thread.sleep(502);
        delayRecorder.getFilterB().accept(Vessel.createProbe(originatorId, 4));

        final DelayRecorder.Result result = delayRecorder.compute();
        logger.info("json: {}", GsonUtil.serializeAll(result));
    }

    @Test
    public void test01() {

        final String originatorId = "12345678";

        final AtomicLong fakeTime = new AtomicLong( System.currentTimeMillis() );

        final DelayRecorder delayRecorder = DelayRecorder.create(
            VesselType.Probe,
            originatorId,
            fakeTime::get);

        delayRecorder.getFilterA().accept(Vessel.createProbe(originatorId, 1));
        delayRecorder.getFilterB().accept(Vessel.createProbe(originatorId, 1));

        delayRecorder.getFilterA().accept(Vessel.createProbe(originatorId, 2));
        delayRecorder.getFilterB().accept(Vessel.createProbe(originatorId, 2));

        delayRecorder.getFilterA().accept(Vessel.createProbe(originatorId, 3));
        fakeTime.incrementAndGet();
        delayRecorder.getFilterB().accept(Vessel.createProbe(originatorId, 3));

        final DelayRecorder.Result result = delayRecorder.compute();
        logger.info("json: {}", GsonUtil.serializeAll(result));
        Preconditions.checkState( result.totalCount == 3 );
        Preconditions.checkState( closeEnough( result.meanDelayMSec, 0.333f, 0.001f ) );
        Preconditions.checkState( closeEnough( result.stdDevDelayMSec, 0.471f, 0.001f ) );
    }

    private static boolean closeEnough( float a, float b, float threshold ) {
        return Math.abs( a - b ) < threshold * ( Math.abs(a) + Math.abs(b ) );
    }
}
