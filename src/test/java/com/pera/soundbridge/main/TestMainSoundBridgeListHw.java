/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.main;

import com.google.common.base.Preconditions;
import com.pera.TestCommon;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

public class TestMainSoundBridgeListHw extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void test00()
    {
        final int status = TestingMain.main( "--list-all" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }

    @Test
    public void test01()
    {
        final int status = TestingMain.main( "--list-compat", "--list-all" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }
}