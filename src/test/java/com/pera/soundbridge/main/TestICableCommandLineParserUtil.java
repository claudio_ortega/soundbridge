/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.main;

import com.pera.*;
import com.pera.soundbridge.cmdline.*;
import com.pera.util.*;
import org.junit.*;
import org.apache.logging.log4j.*;

public class TestICableCommandLineParserUtil extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void test00()
    {
        final SoundBridgeStartupConfigInfo config = SoundBridgeCommandLineParser
            .create()
            .getConfigFromArgs( new String[] { "--help" }, SoundBridgeStartupConfigInfo.DEFAULT) ;

        logger.info( "config:{}", GsonUtil.serializeAll( config ) );
    }

    @Test
    public void test01()
    {
        final SoundBridgeStartupConfigInfo config = SoundBridgeCommandLineParser
            .create()
            .getConfigFromArgs( new String[] { "--use-rest=true", "--rest-port", "6666" }, SoundBridgeStartupConfigInfo.DEFAULT ) ;

        logger.info( "config:{}", GsonUtil.serializeAll( config ) );
    }
}

