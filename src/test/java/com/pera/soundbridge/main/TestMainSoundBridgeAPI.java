/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.main;

import com.google.common.base.Preconditions;
import com.pera.TestCommon;
import com.pera.soundbridge.ISoundBridge;
import com.pera.soundbridge.SoundBridge;
import com.pera.soundbridge.audio.common.DriversInfo;
import com.pera.soundbridge.audio.common.FSEnum;
import com.pera.soundbridge.station.StationBuilder;
import com.pera.util.GsonUtil;
import com.pera.util.SystemUtil;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

public class TestMainSoundBridgeAPI extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void test00() throws Exception {
        final ISoundBridge soundBridge = SoundBridge.create();
        logger.info( "validFS:{}", GsonUtil.serialize( soundBridge.getValidFS() ) );
        soundBridge.stop();
    }

    @Test
    public void test01() throws Exception {
        final ISoundBridge soundBridge = SoundBridge.create();

        soundBridge.getSoundBridgeConfig().setFastAudioDeviceTest(false);
        logger.info("(slow before)" );
        final DriversInfo driversInfoA = soundBridge.getDriversInfo();
        logger.info("(slow after) driversInfo:{}", GsonUtil.serialize(driversInfoA));

        soundBridge.getSoundBridgeConfig().setFastAudioDeviceTest(true);
        logger.info("(fast before)" );
        final DriversInfo driversInfoB = soundBridge.getDriversInfo();
        logger.info("(fast after) driversInfo:{}", GsonUtil.serialize(driversInfoB));

        Preconditions.checkState( GsonUtil.serialize(driversInfoA).equals( GsonUtil.serialize(driversInfoB) ) );

        soundBridge.stop();
    }

    @Test
    public void test03() throws Exception
    {
        final ISoundBridge soundBridge = SoundBridge.create();

        final int status = soundBridge.start(
            false,
            false,
            false,
            false,
            false,
            true );  // no args are being passed

        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );

        SystemUtil.sleepMsec( 2_000 );

        soundBridge.getSoundBridgeConfig()
            .setStationType( StationBuilder.StationTypeEnum.full_duplex )
            .setAudioSourceTypeEnum( StationBuilder.AudioSinkOrSourceTypeEnum.hw )
            .setAudioSourceHash( "" )   // ""d09a:1c7f" )
            .setAudioSinkTypeEnum( StationBuilder.AudioSinkOrSourceTypeEnum.hw )
            .setAudioSinkHash( "" )     // ""d09a:1c7f" )
            .setAudioSamplingFrequency( FSEnum.value44100 )
            .setBufferLengthInMSec( 10 )
            .setThisSideIP( SystemUtil.getAValidLocalIP( "", true ) )
            .setOtherSideIP( SystemUtil.getAValidLocalIP( "", true ) );

        logger.info( "config:{}", GsonUtil.serialize( soundBridge.getSoundBridgeConfig() ) );

        soundBridge.startStation();
        logger.info( "start" );
        SystemUtil.sleepMsec( 10_000 );

        soundBridge.stopStation();
        logger.info( "stop" );
        SystemUtil.sleepMsec( 2_000 );

        soundBridge.stop();
        logger.info( "terminate" );
    }
}