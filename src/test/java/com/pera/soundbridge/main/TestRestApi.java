/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.main;

import com.google.common.base.*;
import com.pera.*;
import com.pera.util.*;
import org.junit.*;
import org.apache.logging.log4j.*;
import java.io.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

public class TestRestApi extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void test00() throws InterruptedException
    {
        final CountDownLatch latch = new CountDownLatch(2);
        final AtomicInteger status = new AtomicInteger(1);

        new Thread(
            () ->
            {
                try {
                    final File config = new File("./src/test/java/com/pera/soundbridge/main/config-test-rest-os.properties");
                    Preconditions.checkState( config.exists() );

                    final int ret = TestingMain.main(
                        "--config-file=" + config.getAbsolutePath(),
                        "--use-rest=true",
                        "--rest-host", "localhost",
                        "--rest-port", "9090");

                    logger.info("status:{}", ret);

                    status.set(ret);
                }

                finally
                {
                    latch.countDown();
                }
            }
        ).start();

        new Thread(
            () ->
            {
                try {

                    Thread.sleep( 15_000 );

                    final RestUtil.RestReturn ret1 = RestUtil.sendGet(
                        "http",
                        "localhost",
                        9090,
                        "/get-latest-config",
                        1000);
                    logger.info("(1) lRestReturn: [" + ret1 + "]");
                    Preconditions.checkState(ret1.getStatus() == 200);
                    Preconditions.checkState(!ret1.getContent().isEmpty());

                    Thread.sleep( 1_000 );

                    final RestUtil.RestReturn ret2 = RestUtil.sendPut (
                        "http",
                        "localhost",
                        9090,
                        "/ptt-off",
                        1000,
                        "" );
                    logger.info ( "(2) lRestReturn: [" + ret2 + "]" );
                    Preconditions.checkState ( ret2.getStatus () == 200 );
                    Preconditions.checkState ( ! ret2.getContent ().isEmpty () );

                    Thread.sleep( 1_000 );

                    final RestUtil.RestReturn ret3 = RestUtil.sendPut (
                        "http",
                        "localhost",
                        9090,
                        "/shutdown-app",
                        1000,
                        "" );
                    logger.info ( "(3) lRestReturn: [" + ret3 + "]" );
                    Preconditions.checkState ( ret3.getStatus () == 200 );
                    Preconditions.checkState ( ! ret3.getContent ().isEmpty () );

                    Thread.sleep( 5_000 );
                }
                catch ( Exception e )
                {
                    logger.error(e.toString(), e);
                }
                finally
                {
                    latch.countDown();
                }
            }
        ).start();

        logger.info("waiting on the latch..");
        final boolean timedOut = !latch.await(1, TimeUnit.MINUTES);
        Preconditions.checkState( !timedOut );
        Preconditions.checkState( status.get() == 0 );
        logger.info("test done.");
    }
}