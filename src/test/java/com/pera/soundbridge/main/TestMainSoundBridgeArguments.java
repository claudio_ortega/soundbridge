/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.main;

import com.google.common.base.Preconditions;
import com.pera.TestCommon;
import org.junit.Test;
import org.apache.logging.log4j.*;

public class TestMainSoundBridgeArguments extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void test00()
    {
        final int status = TestingMain.main( "" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == -1 );
    }

    @Test
    public void test01()
    {
        final int status = TestingMain.main( "--help" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }

    @Test
    public void test02()
    {
        final int status = TestingMain.main( "--ver" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }

    @Test
    public void test03()
    {
        final int status = TestingMain.main( "--check" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }
}