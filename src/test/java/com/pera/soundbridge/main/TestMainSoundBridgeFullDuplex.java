/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.main;

import com.google.common.base.Preconditions;
import com.pera.TestCommon;
import org.junit.Test;
import org.apache.logging.log4j.*;

public class TestMainSoundBridgeFullDuplex extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void test00()
    {
        final int status = TestingMain.main(
            "--config-file=./src/test/data/config-fullduplex.properties" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }
}