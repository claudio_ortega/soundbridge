/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.main;

import com.google.common.base.*;
import com.pera.*;
import org.apache.logging.log4j.*;
import org.junit.*;

public class TestMainSoundBridgeNetworkLoopback extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void test00()
    {
        final int status = TestingMain.main(
            "--config-file=./src/test/data/config-network-loopback.properties" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }
}