/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.gpio;

import com.pera.TestCommon;
import org.junit.Test;
import org.apache.logging.log4j.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class TestGPIO extends TestCommon {

    private final Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel() {
        return Level.DEBUG;
    }

    @Test
    public void test00()
    {
        try( final IGPIOController controller = GPIOController.create().init(false) )
        {
            final CountDownLatch countDownLatch = new CountDownLatch(1);
            final AtomicBoolean lStopSignal = new AtomicBoolean(false);
            final AtomicBoolean pttOn = new AtomicBoolean(false);

            new Thread(
                () ->
                {
                    try {
                        while (!lStopSignal.get()) {
                            final boolean lPttOn = pttOn.get();
                            logger.info("pttOn:{}", lPttOn);
                            controller.setPinState( 17, lPttOn );
                            pttOn.set(!lPttOn);

                            Thread.sleep(500);
                        }
                    } catch (InterruptedException ex) {
                        logger.error(ex.getMessage(), ex);
                    } finally {
                        countDownLatch.countDown();
                    }
                }
            ).start();

            logger.info("start pulsating..");
            Thread.sleep(4_000);
            lStopSignal.set(true);
            logger.info("stop pulsating..");

            final boolean timeout = ! countDownLatch.await(5, TimeUnit.SECONDS);
            logger.info("end pulsating with timeout: {}", timeout );
        }
        catch (Exception ex)
        {
            logger.error(ex.getMessage(), ex);
        }
    }
}