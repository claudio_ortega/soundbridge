/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera;

import com.pera.util.*;
import org.junit.*;
import org.apache.logging.log4j.Level;

public abstract class TestCommon
{
    protected Level getLoggingLevel()
    {
        return Level.INFO;
    }

    @BeforeClass
    public static void beforeClass()
    {
        // IMPORTANT !!!! this has to happen BEFORE ANY call to LogManager.getLogger();
        LogUtil.initLog4j();
    }

    @Before
    public void beforeInstance()
    {
        LogConfigurator.singleton().setLevel( getLoggingLevel ().toString () );
    }
}

