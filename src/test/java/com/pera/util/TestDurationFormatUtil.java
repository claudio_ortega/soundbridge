/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import com.pera.*;
import org.junit.*;
import org.apache.logging.log4j.*;

public class TestDurationFormatUtil extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Test
    public void test00() 
    {
        {
            final String a = TimeUtil.formatDuration(1234);
            logger.info("a:{}", a);
        }

        {
            final String a = TimeUtil.formatDuration(1234 * 10);
            logger.info("a:{}", a);
        }

        {
            final String a = TimeUtil.formatDuration(1234 * 100);
            logger.info("a:{}", a);
        }

        {
            final String a = TimeUtil.formatDuration(1234 * 1000);
            logger.info("a:{}", a);
        }

        {
            final String a = TimeUtil.formatDuration(1234 * 10000);
            logger.info("a:{}", a);
        }

        {
            final String a = TimeUtil.formatDuration(1234 * 100000);
            logger.info("a:{}", a);
        }

        {
            final String a = TimeUtil.formatDuration(1234 * 100000000L);
            logger.info("a:{}", a);
        }
    }
}