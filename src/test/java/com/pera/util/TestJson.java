/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import com.pera.TestCommon;
import com.pera.soundbridge.restinfo.SoundBridgeStatus;
import org.json.JSONObject;
import org.junit.Test;
import org.apache.logging.log4j.*;

public class TestJson extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Test
    public void test00 ()
    {
        final String serialized = GsonUtil.serialize(
            SoundBridgeStatus
                .create()
                .setAppStartTime( 5 )
                .setStationIsOperational( true )
        );
        logger.info("serialized: {}", serialized );

        // see https://www.programcreek.com/java-api-examples/org.json.JSONTokener
        final JSONObject json = new JSONObject(serialized);
        final Long startTime = json.getLong("appStartTime");
        logger.info("startTime:{}", startTime );
    }
}