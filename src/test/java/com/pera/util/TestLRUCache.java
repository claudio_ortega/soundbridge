/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import com.google.common.base.*;
import com.pera.*;
import org.apache.logging.log4j.*;
import org.junit.*;

public class TestLRUCache extends TestCommon
{
    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void test00()
    {
        final LRUCache cache = new LRUCache(3);

        cache.set( "1", "a" );
        cache.set( "2", "b" );
        cache.set( "3", "c" );
        cache.set( "4", "d" );

        Preconditions.checkNotNull( cache.get( "4" ) );
        Preconditions.checkState( cache.get( "1" ) == null );
    }
}

