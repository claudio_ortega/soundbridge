/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import com.google.common.base.*;
import com.pera.*;
import org.junit.*;
import org.apache.logging.log4j.*;

import java.io.*;


public class TestOrderedProperties extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Test
    public void test00 () throws Exception
    {
        final File fileA = new File( "./src/test/java/com/pera/util/abc.properties");
        final File fileB = new File( "./src/test/java/com/pera/util/abc-out.properties");

        {
            final OrderedProperties pA = OrderedProperties.create().fromFile(fileA);
            logger.info("p:{}", pA.getMap());
            Preconditions.checkState(pA.getMap().size() == 11 );
            pA.toFile(fileB);
        }

        {
            final OrderedProperties pB = OrderedProperties.create().fromFile(fileB);
            logger.info("p:{}", pB.getMap());
            Preconditions.checkState(pB.getMap().size() == 11 );
        }
    }
}