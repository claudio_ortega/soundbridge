/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import com.pera.TestCommon;
import org.junit.Test;
import org.apache.logging.log4j.*;
import java.util.Locale;
import java.util.Set;

public class TestSystemUtil extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Test
    public void test01 ()
    {
        final String pid = SystemUtil.getPIDForThisProcess( "" );
        logger.info( "pid:{}", pid );

        final Set<Integer> allJVMs = SystemUtil.getJavaVMsMatching( null );
        logger.info( "JVMs:{}", allJVMs );
    }

    @Test
    public void test02 ()
    {
        final String ip = SystemUtil.getAValidLocalIP( "", true );
        logger.info( "ip:{}", ip );
    }

    @Test(expected = NullPointerException.class)
    public void testNull ()
    {
        final String ip = SystemUtil.getAValidLocalIP( null, true );
        logger.info( "ip:{}", ip );
    }

    @Test
    public void test03 ()
    {
        final String ip = SystemUtil.getAValidLocalIP( "localhost", true );
        logger.info( "ip:{}", ip );
    }

    @Test
    public void test04 ()
    {
        final String ip = SystemUtil.getAValidLocalIP( "127.0.0.1", true );
        logger.info( "ip:{}", ip );
    }

    @Test(expected = IllegalStateException.class)
    public void test05 ()
    {
        final String ip = SystemUtil.getAValidLocalIP( "127.0.0.56", true );
        logger.info( "ip:{}", ip );
    }

    @Test(expected = IllegalStateException.class)
    public void test06 ()
    {
        final String ip = SystemUtil.getAValidLocalIP( "127.0.78.56", true );
        logger.info( "ip:{}", ip );
    }

    @Test(expected = IllegalStateException.class)
    public void test07 ()
    {
        final String ip = SystemUtil.getAValidLocalIP( "127.0.78.56999", true );
        logger.info( "ip:{}", ip );
    }

    @Test(expected = IllegalStateException.class)
    public void test08 ()
    {
        final String ip = SystemUtil.getAValidLocalIP( "a.b.c.com", true );
        logger.info( "ip:{}", ip );
    }

    @Test
    public void test09 ()
    {
        final Locale locale = Locale.getDefault();
        logger.info( "locale:{}", locale );
        logger.info( "locale.getDisplayLanguage():{}", locale.getDisplayLanguage() );
        logger.info( "locale.getDisplayCountry():{}", locale.getDisplayCountry() );
        logger.info( "locale.getLanguage():{}", locale.getLanguage() );
        logger.info( "locale.getCountry():{}", locale.getCountry() );
        logger.info( "locale.getISO3Country():{}", locale.getISO3Country() );
        logger.info( "locale.getISO3Language():{}", locale.getISO3Language() );
    }
}