/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import org.junit.Test;
import org.apache.logging.log4j.*;

public class TestLog1
{
    @Test
    public void test00 ()
    {
        // IMPORTANT !!!! this has to happen BEFORE ANY call to LogManager.getLogger();
        LogConfigurator.singleton().init();

        LogConfigurator.singleton().setLevel( "TRACE" );
        logAtAllLevels ();

        LogConfigurator.singleton().setLevel( "DEBUG" );
        logAtAllLevels ();

        LogConfigurator.singleton().setLevel( "INFO" );
        logAtAllLevels ();

        LogConfigurator.singleton().setLevel( "WARN" );
        logAtAllLevels ();

        LogConfigurator.singleton().setLevel( "ERROR" );
        logAtAllLevels ();

        LogConfigurator.singleton().setLevel( "DEBUG" );
        logAtAllLevels ();
    }

    private void logAtAllLevels ()
    {
        final Logger logger = LogManager.getLogger();

        for ( int i=0; i<10; i++) {
            logger.trace("hello world! (1)");
            logger.debug("hello world! (2)");
            logger.info("hello world! (3)");
            logger.warn("hello world! (4)");
            logger.error("hello world! (5)");
            logger.error("");
        }
    }
}