/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import com.google.common.base.Preconditions;
import com.pera.TestCommon;
import com.pera.soundbridge.audio.util.AudioLineUtils;
import org.junit.*;
import org.apache.logging.log4j.*;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class TestByteBuffer extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Test
    public void test01 ()
    {
        iTestA(0);
        iTestA(10000000);
        iTestA(0x742349238749L);
        iTestA(System.currentTimeMillis());
    }

    @Test
    public void test02 ()
    {
        iTestB(0);
        iTestB(10000000);
        iTestB(0x742349238749L);
        iTestB(System.currentTimeMillis());
    }

    private void iTestA ( long in )
    {
        final ByteBuffer bbuf = ByteBuffer.allocate(8);
        bbuf.putLong(0,in);
        final long b = bbuf.getLong();
        logger.info( "bbuf:{}", Arrays.toString(bbuf.array()) );
        Preconditions.checkState( in == b );
    }

    private void iTestB ( long in )
    {
        final byte[] a = AudioLineUtils.getBytesFromLong( in );
        final long b = AudioLineUtils.getLongFromBytes( a, 0, 8 );
        Preconditions.checkState( b == in );
    }

    @Test
    public void test24Bits ()
    {
        Assert.assertEquals(          0, AudioLineUtils.convert3PositiveBytesIntoSignedNumber( 0, 0, 0 ) );
        Assert.assertEquals(          1, AudioLineUtils.convert3PositiveBytesIntoSignedNumber( 0, 0, 1 ) );
        Assert.assertEquals(      0x100, AudioLineUtils.convert3PositiveBytesIntoSignedNumber( 0, 1, 0 ) );
        Assert.assertEquals(    0x10000, AudioLineUtils.convert3PositiveBytesIntoSignedNumber( 1, 0, 0 ) );
        Assert.assertEquals(    0x10203, AudioLineUtils.convert3PositiveBytesIntoSignedNumber( 1, 2, 3 ) );
        Assert.assertEquals(         -1, AudioLineUtils.convert3PositiveBytesIntoSignedNumber( 0xff, 0xff, 0xff ) );
        Assert.assertEquals(         -2, AudioLineUtils.convert3PositiveBytesIntoSignedNumber( 0xff, 0xff, 0xfe ) );
    }
}