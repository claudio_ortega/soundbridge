/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import com.pera.TestCommon;
import org.junit.Test;
import org.apache.logging.log4j.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class TestQueue extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void test00 () throws Exception
    {
        final BlockingQueue<String>queue = new ArrayBlockingQueue<>(3);

        logger.info( String.format( "(1)queue.remaining:%d", queue.remainingCapacity() ) );
        logger.info( String.format( "(2)queue.size:%d", queue.size() ) );

        queue.put("a");
        logger.info( String.format( "(3)queue.remaining:%d", queue.remainingCapacity() ) );
        logger.info( String.format( "(4)queue.size:%d", queue.size() ) );

        queue.take();
        logger.info( String.format( "(5)queue.remaining:%d", queue.remainingCapacity() ) );
        logger.info( String.format( "(6)queue.size:%d", queue.size() ) );

        queue.put("b");
        logger.info( String.format( "(7)queue.remaining:%d", queue.remainingCapacity() ) );
        logger.info( String.format( "(8)queue.size:%d", queue.size() ) );

        queue.put("c");
        logger.info( String.format( "(9)queue.remaining:%d", queue.remainingCapacity() ) );
        logger.info( String.format( "(10)queue.size:%d", queue.size() ) );

        queue.put("d");
        logger.info( String.format( "(11)queue.remaining:%d", queue.remainingCapacity() ) );
        logger.info( String.format( "(12)queue.size:%d", queue.size() ) );

        queue.take();
        logger.info( String.format( "(13)queue.remaining:%d", queue.remainingCapacity() ) );
        logger.info( String.format( "(14)queue.size:%d", queue.size() ) );
    }
}