package com.pera.license;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.pera.TestCommon;
import com.pera.util.FileUtil;
import com.pera.util.GsonUtil;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import java.io.File;
import java.util.List;

public class TestLicenseInfo extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    protected Level getLoggingLevel()
    {
        return Level.INFO;
    }

    @Test
    public void testGenerateLicenseInfo() throws Exception {
        final File licenseRootDir = new File ( "src/test/java/com/pera/license" );
        final File licensesOutDir = new File( licenseRootDir, "generated" );

        FileUtil.deleteDirectoryRecursively( licensesOutDir, false );
        FileUtil.createDirIfDoesNotExist( licensesOutDir );

        final List<String> lines = FileUtil.getLinesFromFile( new File( licenseRootDir, "licenses.csv" ) );

        for (String line : lines) {
            if ( !line.trim().startsWith("#") ) {
                final String[] splitted = line.trim().split(",");
                if (splitted.length > 1) {

                    final LicenseInfo licenseInfo = LicenseInfo.create(
                        Integer.parseInt( splitted[0].trim() ),
                        splitted[1].trim(),
                        splitted[2].trim(),
                        splitted[3].trim(),
                        "perpetual".equals( splitted[4].trim() ),
                        splitted[4].trim()
                    );

                    Preconditions.checkState( licenseInfo.isLegitimate() );

                    logger.info( String.format (
                        "[%-8s | %-4d | %-20s | %-40s | %-12s | %-12s | %s]",
                        licenseInfo.getVersion(),
                        licenseInfo.getLicenseNumber(),
                        licenseInfo.getUserCompleteName(),
                        licenseInfo.getUserEmail(),
                        licenseInfo.getExpirationDate(),
                        licenseInfo.isExpired() ? "expired" : "not expired",
                        licenseInfo.getDigest() )
                    );

                    final File outputLicenseFile = new File (
                        licensesOutDir,
                        String.format(
                            "license-%s.json",
                            licenseInfo.getUserCompleteName().toLowerCase().replace(' ', '-' ) ) );

                    FileUtil.writeLinesIntoFile(
                        ImmutableList.of ( GsonUtil.serialize( licenseInfo ) ),
                        outputLicenseFile
                    );

                    final String licenseKey = new String(FileUtil.getFileAsBytes( outputLicenseFile ) );
                    final LicenseInfo licenseInfoRead = GsonUtil.deSerialize( licenseKey, LicenseInfo.class );
                    Preconditions.checkState( licenseInfoRead.isLegitimate() );
                }
            }
        }
    }
}