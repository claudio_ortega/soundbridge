/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.suites;

import com.pera.soundbridge.audio.*;
import com.pera.soundbridge.gpio.*;
import com.pera.soundbridge.main.*;
import com.pera.soundbridge.station.*;
import com.pera.util.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith (Suite.class)
@Suite.SuiteClasses(
    {
        TestAudioLineUtils.class,
        TestLineCodec.class,
        TestUDP.class,
        TestVessel.class,
        TestGPIO.class,
        TestStations.class,
        TestMainSoundBridgeArguments.class,
        TestSystemUtil.class,
        TestICableCommandLineParserUtil.class,
        TestLog1.class,
        TestLog2.class,
        TestRestApi.class,
    }
)

public class TestRegressionSuiteNoHw
{
}
