/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.suites;

import com.pera.soundbridge.audio.*;
import com.pera.soundbridge.main.TestMainSoundBridgeAPI;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith (Suite.class)
@Suite.SuiteClasses(
    {
        TestAudioSource.class,
        TestAudioSink.class,
        TestMainSoundBridgeAPI.class
    }
)

public class TestRegressionSuiteHw
{
}
