/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 */

package com.pera.icable;

import com.google.common.base.Preconditions;
import com.pera.TestCommon;
import com.pera.icable.javafx.ICableApplication;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

public class TestMainICable extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    protected Level getLoggingLevel()
    {
        return Level.INFO;
    }

    @Test
    public void testVersion()
    {
        final int status = ICableApplication.main( true, "--version" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }

    @Test
    public void testHelp()
    {
        final int status = ICableApplication.main( true, "--help" );
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }

    @Test
    public void testNormal ()
    {
        final int status = ICableApplication.main( true, "--log-level=info");
        logger.info( "status:{}", status );
        Preconditions.checkArgument( status == 0 );
    }
}