/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.registry;

import com.google.common.base.Preconditions;
import com.pera.TestCommon;
import com.pera.registry.restinfo.SubscriberRegistrationInfoMap;
import com.pera.registry.restinfo.SubscribersQueryInfo;
import com.pera.registry.restinfo.SubscriberRegistrationInfo;
import com.pera.util.GsonUtil;
import com.pera.util.RestUtil;
import com.pera.util.SystemUtil;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

public class TestRegistry extends TestCommon
{
    private final Logger logger = LogManager.getLogger();

    private final String registryHostName = "localhost";
    private final int registryPort = 7777;

    @Override
    protected Level getLoggingLevel() {
        return Level.INFO;
    }

    @Test
    public void testHelp()
    {
        final Registry registry = Registry.create();

        final int status = registry.start(
            false,
            true,
            false,
            "-help"
        );

        logger.info("status:{}", status);
        Preconditions.checkArgument(status == 0);
    }

    @Test
    public void test00() throws Exception
    {
        final Registry registry = Registry.create();

        final int status = registry.start(
            true,
            false,
            true,
            "--rest-host", registryHostName
        );

        logger.info("status:{}", status);
        Preconditions.checkArgument(status == 0);

        SystemUtil.sleepMsec( 1_000 );

        registry.terminate();
    }

    @Test
    public void test01() throws Exception
    {
        final Registry registry = Registry.create();

        {
            final int status = registry.start(
                true,
                false,
                true,
                "--rest-host", registryHostName,
                "--rest-port", Integer.toString(registryPort)
            );

            logger.info("status:{}", status);
            Preconditions.checkArgument(status == 0);
        }

        {
            final RestUtil.RestReturn ret1 = RestUtil.sendGet(
                "http",
                registryHostName,
                registryPort,
                "/test",
                1000);
            logger.info("(1) lRestReturn: [" + ret1 + "]");
            Preconditions.checkState(ret1.getStatus() == 200);
            Preconditions.checkState(!ret1.getContent().isEmpty());
        }

        {
            final RestUtil.RestReturn ret1 = RestUtil.sendGet(
                "http",
                registryHostName,
                registryPort,
                "/get-latest-status",
                1000);
            logger.info("(2) lRestReturn: [" + ret1 + "]");
            Preconditions.checkState(ret1.getStatus() == 200);
            Preconditions.checkState(!ret1.getContent().isEmpty());
        }

        {
            final SubscriberRegistrationInfo registrationInfo =  SubscriberRegistrationInfo
                .create()
                .setClientId("1")
                .setClientName("claudio")
                .setGroupName("12345678");

            final RestUtil.RestReturn ret2 = RestUtil.sendPut(
                "http",
                registryHostName,
                registryPort,
                "/register",
                1000,
                registrationInfo);

            logger.info("(3.1) lRestReturn: [" + ret2 + "]");
            Preconditions.checkState(ret2.getStatus() == 200);
            Preconditions.checkState(!ret2.getContent().isEmpty());
        }

        {
            final SubscriberRegistrationInfo registrationInfo =  SubscriberRegistrationInfo
                .create()
                .setClientId("2")
                .setClientName("claudio2")
                .setGroupName("secret1");

            final RestUtil.RestReturn ret2 = RestUtil.sendPut(
                "http",
                registryHostName,
                registryPort,
                "/register",
                1000,
                registrationInfo);

            logger.info("(3.2) lRestReturn: [" + ret2 + "]");
            Preconditions.checkState(ret2.getStatus() == 200);
            Preconditions.checkState(!ret2.getContent().isEmpty());
        }

        {
            final SubscribersQueryInfo queryInfo = SubscribersQueryInfo
                .create()
                .setGroupName("secret1");

            final RestUtil.RestReturn ret1 = RestUtil.sendPut(
                "http",
                registryHostName,
                registryPort,
                "/get-registrations",
                1000,
                queryInfo );

            logger.info("(4.1) lRestReturn: [" + ret1 + "]");

            Preconditions.checkState(ret1.getStatus() == 200);
            final SubscriberRegistrationInfoMap result = GsonUtil.deSerialize( ret1.getContent(), SubscriberRegistrationInfoMap.class);
            Preconditions.checkState(result.getMap().isEmpty());
        }

        {
            final SubscribersQueryInfo queryInfo = SubscribersQueryInfo
                .create()
                .setGroupName("u");

            final RestUtil.RestReturn ret1 = RestUtil.sendPut(
                "http",
                registryHostName,
                registryPort,
                "/get-registrations",
                1000,
                queryInfo );

            logger.info("(4.2) lRestReturn: [" + ret1 + "]");

            Preconditions.checkState(ret1.getStatus() == 200);
            final SubscriberRegistrationInfoMap result = GsonUtil.deSerialize( ret1.getContent(), SubscriberRegistrationInfoMap.class);
            Preconditions.checkState(result.getMap().isEmpty());
        }

        {
            final SubscribersQueryInfo queryInfo = SubscribersQueryInfo
                .create()
                .setGroupName("12345678");

            final RestUtil.RestReturn ret1 = RestUtil.sendPut(
                "http",
                registryHostName,
                registryPort,
                "/get-registrations",
                100000,
                queryInfo );

            logger.info("(6) lRestReturn: [" + ret1 + "]");

            final SubscriberRegistrationInfoMap result = GsonUtil.deSerialize( ret1.getContent(), SubscriberRegistrationInfoMap.class);
            Preconditions.checkState(!result.getMap().isEmpty());
        }

        {
            final SubscriberRegistrationInfo registrationInfo = SubscriberRegistrationInfo
                    .create()
                    .setClientId("1");

            final RestUtil.RestReturn ret2 = RestUtil.sendPut(
                "http",
                registryHostName,
                registryPort,
                "/unregister",
                1000,
                registrationInfo);

            logger.info("(5) lRestReturn: [" + ret2 + "]");
            Preconditions.checkState(ret2.getStatus() == 200);
            Preconditions.checkState(!ret2.getContent().isEmpty());
        }

        {
            final SubscribersQueryInfo queryInfo = SubscribersQueryInfo
                .create()
                .setGroupName("12345678");

            final RestUtil.RestReturn ret1 = RestUtil.sendPut(
                "http",
                registryHostName,
                registryPort,
                "/get-registrations",
                100000,
                queryInfo );

            logger.info("(6) lRestReturn: [" + ret1 + "]");

            final SubscriberRegistrationInfoMap result = GsonUtil.deSerialize( ret1.getContent(), SubscriberRegistrationInfoMap.class);
            Preconditions.checkState(result.getMap().isEmpty());
        }

        registry.terminate();
    }
}

