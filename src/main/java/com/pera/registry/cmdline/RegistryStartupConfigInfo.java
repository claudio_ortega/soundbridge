/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.registry.cmdline;

public class RegistryStartupConfigInfo
{
    private int restConfigPort;
    private String restConfigHostNameOrIPAddress;
    private boolean helpRequested;
    private boolean versionRequested;
    private String logLevel;

    public static RegistryStartupConfigInfo create()
    {
        return new RegistryStartupConfigInfo();
    }

    public static final RegistryStartupConfigInfo DEFAULT = create();

    private RegistryStartupConfigInfo()
    {
        helpRequested = false;
        versionRequested = false;
        restConfigPort = 7777;
        restConfigHostNameOrIPAddress = "";
        logLevel = "info";
    }

    public boolean isVersionRequested() {
        return versionRequested;
    }

    public RegistryStartupConfigInfo setVersionRequested(boolean value) {
        versionRequested = value;
        return this;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public RegistryStartupConfigInfo setLogLevel(String value) {
        logLevel = value;
        return this;
    }

    public RegistryStartupConfigInfo setRestConfigPort(int value)
    {
        restConfigPort = value;
        return this;
    }

    public RegistryStartupConfigInfo setHelpRequested(boolean value)
    {
        helpRequested = value;
        return this;
    }

    public RegistryStartupConfigInfo setRestConfigHostNameOrIPAddress(String value)
    {
        restConfigHostNameOrIPAddress = value;
        return this;
    }

    public int getRestConfigPort()
    {
        return restConfigPort;
    }

    public boolean getHelpRequested()
    {
        return helpRequested;
    }

    public String getRestConfigHostNameOrIPAddress()
    {
        return restConfigHostNameOrIPAddress;
    }
}

