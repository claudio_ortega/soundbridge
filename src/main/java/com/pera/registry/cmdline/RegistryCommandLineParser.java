/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.registry.cmdline;

import com.pera.util.CommandLineParserUtil;
import com.pera.util.SystemUtil;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import java.io.IOException;
import java.io.PrintStream;

public class RegistryCommandLineParser
{
    private final OptionParser parser;

    public static RegistryCommandLineParser create()
    {
        return new RegistryCommandLineParser();
    }

    private RegistryCommandLineParser()
    {
        parser = new OptionParser();
    }

    public RegistryStartupConfigInfo getConfigFromArgs(
        String[] args,
        RegistryStartupConfigInfo defaultValues )
    {
        final OptionSpec<Void> help = parser
            .accepts( "help", "prints this help and exits" )
            .forHelp();

        final OptionSpec<Void> version = parser
            .accepts( "version", "prints the app version and exits" );

        final OptionSpec<String> restHostNameOrIPAddress = parser
            .accepts( "rest-host", "sets the name/IP address for the restful API server, " +
                      "if empty we will use the first available v4 address" )
            .withRequiredArg()
            .ofType( String.class )
            .defaultsTo( defaultValues.getRestConfigHostNameOrIPAddress() );

        final OptionSpec<Integer> restPort = parser
            .accepts( "rest-port", "sets the port number used by the restful API server" )
            .withRequiredArg()
            .ofType( Integer.class )
            .defaultsTo( defaultValues.getRestConfigPort() );

        final OptionSpec<String> logLevel = parser
            .accepts( "log-level", "sets the log level, it should be any of these: {error, warn, info, debug, trace}" )
            .withRequiredArg()
            .ofType( String.class )
            .defaultsTo( defaultValues.getLogLevel() );

        final OptionSet options = parser.parse( args );

        final RegistryStartupConfigInfo startupConfig = RegistryStartupConfigInfo.create();

        startupConfig.setHelpRequested( options.has( help ) );
        startupConfig.setVersionRequested( options.has( version ) );
        startupConfig.setRestConfigHostNameOrIPAddress( options.valueOf( restHostNameOrIPAddress ) );
        startupConfig.setRestConfigPort( options.valueOf( restPort ) );
        startupConfig.setLogLevel( options.valueOf( logLevel ) );

        return startupConfig;
    }

    public void printHelp ( PrintStream ps ) throws IOException
    {
        ps.println();
        ps.println("Registry service for SoundBridge." );
        ps.println();
        ps.println("git version: " + SystemUtil.getGitVersion( true ) );
        ps.println();
        ps.println("Usage: ");
        ps.println();
        parser.formatHelpWith( new CommandLineParserUtil.HelpFormatter() );
        parser.printHelpOn( ps );
        ps.println();
        ps.println( "All command line options should start either with a single or double hyphen," );
        ps.println( "followed by the minimum number of letters that singles out an option." );
    }
}

