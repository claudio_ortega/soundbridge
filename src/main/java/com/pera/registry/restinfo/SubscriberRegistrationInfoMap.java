/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.registry.restinfo;

import com.google.gson.annotations.Expose;

import java.util.Map;

public class SubscriberRegistrationInfoMap
{
    @Expose private Map<String,SubscriberRegistrationInfo> map;

    public Map<String, SubscriberRegistrationInfo> getMap() {
        return map;
    }

    public SubscriberRegistrationInfoMap setMap(Map<String, SubscriberRegistrationInfo> map) {
        this.map = map;
        return this;
    }
}
