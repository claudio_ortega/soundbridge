/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.registry.restinfo;

import com.google.gson.annotations.Expose;

public class RegistryAppStatusInfo
{
    @Expose private volatile long appStartTime;
    @Expose private volatile long currentTime;
    @Expose private volatile String appVersion;
    @Expose private volatile String osName;
    @Expose private volatile String osVersion;
    @Expose private volatile String osArch;
    @Expose private volatile int numberOfProcessors;
    @Expose private volatile String cmdLineArgs;
    @Expose private volatile String processId;
    @Expose private volatile String currentDirectory;
    @Expose private volatile String javaArgs;
    @Expose private volatile String javaHome;
    @Expose private volatile String javaRuntimeVersion;
    @Expose private volatile String logLevel;

    public static RegistryAppStatusInfo create()
    {
        return new RegistryAppStatusInfo();
    }

    private RegistryAppStatusInfo()
    {
    }

    public RegistryAppStatusInfo setLogLevel(String value) {
        logLevel = value;
        return this;
    }

    public RegistryAppStatusInfo setAppStartTime(long value )
    {
        appStartTime = value;
        return this;
    }

    public RegistryAppStatusInfo setCurrentTime(long value )
    {
        currentTime = value;
        return this;
    }

    public RegistryAppStatusInfo setAppVersion(String appVersion) {
        this.appVersion = appVersion;
        return this;
    }

    public RegistryAppStatusInfo setOsName(String osName) {
        this.osName = osName;
        return this;
    }

    public RegistryAppStatusInfo setOsVersion(String osVersion) {
        this.osVersion = osVersion;
        return this;
    }

    public RegistryAppStatusInfo setOsArch(String osArch) {
        this.osArch = osArch;
        return this;
    }

    public RegistryAppStatusInfo setNumberOfProcessors(int numberOfProcessors) {
        this.numberOfProcessors = numberOfProcessors;
        return this;
    }

    public RegistryAppStatusInfo setCmdLineArgs(String cmdLineArgs) {
        this.cmdLineArgs = cmdLineArgs;
        return this;
    }

    public RegistryAppStatusInfo setProcessId(String processId) {
        this.processId = processId;
        return this;
    }

    public RegistryAppStatusInfo setCurrentDirectory(String currentDirectory) {
        this.currentDirectory = currentDirectory;
        return this;
    }

    public RegistryAppStatusInfo setJavaArgs(String javaArgs) {
        this.javaArgs = javaArgs;
        return this;
    }

    public RegistryAppStatusInfo setJavaHome(String javaHome) {
        this.javaHome = javaHome;
        return this;
    }

    public RegistryAppStatusInfo setJavaRuntimeVersion(String javaRuntimeVersion) {
        this.javaRuntimeVersion = javaRuntimeVersion;
        return this;
    }
}
