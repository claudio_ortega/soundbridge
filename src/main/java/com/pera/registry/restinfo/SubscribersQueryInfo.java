/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.registry.restinfo;

import com.google.gson.annotations.Expose;

public class SubscribersQueryInfo
{
    @Expose private String groupName;

    public static SubscribersQueryInfo create()
    {
        return new SubscribersQueryInfo();
    }

    private SubscribersQueryInfo() {
    }

    public String getGroupName() {
        return groupName;
    }

    public SubscribersQueryInfo setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }
}
