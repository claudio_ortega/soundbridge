/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.registry.restinfo;

import com.google.gson.annotations.Expose;
import com.pera.soundbridge.audio.common.FSEnum;

public class SubscriberRegistrationInfo
{
    @Expose private String clientId;
    @Expose private String clientName;
    @Expose private String groupName;
    @Expose private String clientWanIPAddress;
    @Expose private String clientLanIPAddress;
    @Expose private int bufferLengthInMSec;
    @Expose private FSEnum audioSamplingFrequency;
    @Expose private Long registrationTime;
    @Expose private Integer licenseNumber;

    public static SubscriberRegistrationInfo create()
    {
        return new SubscriberRegistrationInfo();
    }

    private SubscriberRegistrationInfo() {
        clientId = "";
        clientName = "";
        groupName = "";
        clientWanIPAddress = "";
        clientLanIPAddress = "";
        bufferLengthInMSec = 0;
        audioSamplingFrequency = FSEnum.value44100;
        registrationTime = null;
        licenseNumber = null;
    }

    public String getClientId() {
        return clientId;
    }

    public SubscriberRegistrationInfo setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public String getClientWanIPAddress() {
        return clientWanIPAddress;
    }

    public SubscriberRegistrationInfo setClientWanIPAddress(String clientWanIPAddress) {
        this.clientWanIPAddress = clientWanIPAddress;
        return this;
    }

    public String getClientName() {
        return clientName;
    }

    public SubscriberRegistrationInfo setClientName(String clientName) {
        this.clientName = clientName;
        return this;
    }

    public Long getRegistrationTime() {
        return registrationTime;
    }

    public SubscriberRegistrationInfo setRegistrationTime(Long registrationTime) {
        this.registrationTime = registrationTime;
        return this;
    }

    public String getClientLanIPAddress() {
        return clientLanIPAddress;
    }

    public SubscriberRegistrationInfo setClientLanIPAddress(String clientLanIPAddress) {
        this.clientLanIPAddress = clientLanIPAddress;
        return this;
    }

    public int getBufferLengthInMSec() {
        return bufferLengthInMSec;
    }

    public SubscriberRegistrationInfo setBufferLengthInMSec(int bufferLengthInMSec) {
        this.bufferLengthInMSec = bufferLengthInMSec;
        return this;
    }

    public FSEnum getAudioSamplingFrequency() {
        return audioSamplingFrequency;
    }

    public SubscriberRegistrationInfo setAudioSamplingFrequency(FSEnum audioSamplingFrequency) {
        this.audioSamplingFrequency = audioSamplingFrequency;
        return this;
    }

    public String getGroupName() {
        return groupName;
    }

    public SubscriberRegistrationInfo setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public Integer getLicenseNumber() {
        return licenseNumber;
    }

    public SubscriberRegistrationInfo setLicenseNumber(Integer licenseNumber) {
        this.licenseNumber = licenseNumber;
        return this;
    }
}
