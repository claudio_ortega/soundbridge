/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.registry.main;

import com.pera.registry.Registry;
import com.pera.util.LogUtil;

public class RegistryMain
{
    public static void main( String[] args )
    {
        LogUtil.initLog4j();

        System.exit(
            Registry.create().start(
                false,
                true,
                false,
                args )
        );
    }
}
