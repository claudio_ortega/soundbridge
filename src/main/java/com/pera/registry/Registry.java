/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.registry;

import com.pera.registry.cmdline.RegistryCommandLineParser;
import com.pera.registry.cmdline.RegistryStartupConfigInfo;
import com.pera.registry.restinfo.SubscriberRegistrationInfoMap;
import com.pera.registry.restinfo.SubscribersQueryInfo;
import com.pera.registry.restinfo.SubscriberRegistrationInfo;
import com.pera.registry.restinfo.RegistryAppStatusInfo;
import com.pera.restserver.RestServer;
import com.pera.restserver.RestServerBuilder;
import com.pera.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spark.Request;
import spark.Response;

import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.*;

public class Registry
{
    private static final Logger logger = LogManager.getLogger();

    private final int evictionTimeInMSecs;
    private final RegistryAppStatusInfo registryStatus;
    private final ThreadPoolExecutor executor;
    private final ScheduledThreadPoolExecutor scheduledExecutor;
    private volatile RestServer restServer;
    private final CountDownLatch terminationWaitingLatch;
    private volatile RegistryStartupConfigInfo startupConfig;
    private final ConcurrentHashMap<String, SubscriberRegistrationInfo> map;

    public static Registry create() {
        return new Registry();
    }

    private Registry()
    {
        final int cleanupPeriodInMSecs = 60_000;
        evictionTimeInMSecs = 600_000;

        registryStatus = RegistryAppStatusInfo.create();
        terminationWaitingLatch = new CountDownLatch(1);

        executor = new ThreadPoolExecutor(
            10,
            10,
            5,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>( ),
            TaggedThreadFactory.create( getClass ( ), "rest-api" ) );

        scheduledExecutor = new ScheduledThreadPoolExecutor(
            1,
            TaggedThreadFactory.create( getClass(), "1" ) );

        startupConfig = RegistryStartupConfigInfo.DEFAULT;

        map = new ConcurrentHashMap<>(100);

        scheduledExecutor.scheduleAtFixedRate (
            this::iCleanupOldEntries,
            0,
            cleanupPeriodInMSecs,
            TimeUnit.MILLISECONDS
        );
    }

    public int start (
        boolean executingFromTestCase,
        boolean addShutDownHook,
        boolean returnImmediately,
        String ... args  )
    {
        int exitCode = 0;

        try
        {
            final RegistryCommandLineParser commandLineParser = RegistryCommandLineParser.create();
            startupConfig = commandLineParser.getConfigFromArgs( args, RegistryStartupConfigInfo.DEFAULT );

            if ( startupConfig.getHelpRequested() ) {
                commandLineParser.printHelp( System.out );
                System.out.println( "\nversion " + SystemUtil.getGitVersion ( true ) + "\n" );
                return 0;
            }

            if ( startupConfig.isVersionRequested() ) {
                System.out.println( "\nversion " + SystemUtil.getGitVersion ( true ) + "\n" );
                return 0;
            }

            // our test code configures log4j before executing the mainAlt(),
            // so then, we should skip the log configuration whenever coming from test code
            if ( ! executingFromTestCase ) {
                LogConfigurator.singleton().setLevel( startupConfig.getLogLevel() ) ;
            }

            registryStatus.setLogLevel( LogConfigurator.singleton().getLogLevel() );

            LogUtil.logHeader ( logger, args );

            if ( ! SystemUtil.ensureWeHaveAnIPAddressForService( 60 ) ) {
                System.out.println( "there are no local IPs available" );
                return -1;
            }

            iInitAppStatus( args );

            final String restHost = SystemUtil.getAValidLocalIP(
                startupConfig.getRestConfigHostNameOrIPAddress(),
                true );

            final boolean isRESTSocketUsed = SystemUtil.canConnectOverTCP(
                restHost,
                startupConfig.getRestConfigPort(),
                1_000 );

            if ( isRESTSocketUsed ) {
                throw new IllegalStateException(
                    String.format(
                        "there is already a server socket opened on %s:%d REST server cannot be started",
                        restHost,
                        startupConfig.getRestConfigPort() ) );
            }

            iStartupRestServer(
                startupConfig.getRestConfigHostNameOrIPAddress(),
                startupConfig.getRestConfigPort()
            );

            iAttachRestAPIMappings();

            logger.info("rest server started on {}:{}, REST server started OK: {}",
                restServer.getEffectiveIP(),
                startupConfig.getRestConfigPort(),
                restServer.isStarted() );

            if ( addShutDownHook ) {
                iAddShutdownHook();
            }

            if ( !returnImmediately ) {
                logger.info("station started, to stop use control-c or SIGINT");
                terminationWaitingLatch.await();
                logger.info("stopping application");
            }
        }

        catch ( Throwable e )
        {
            logger.error ( e.getMessage (), e );
            exitCode = -1;
        }

        return exitCode;
    }

    private void iStartupRestServer( String hostNameOrIPAddress, int port )
    {
        restServer = RestServer
            .create (
                RestServerBuilder
                    .create ()
                    .setHostNameOrIpAddress( hostNameOrIPAddress )
                    .setPort ( port )
            )
            .startUp();
    }

    enum RestResult { ok, error }

    private void iAttachRestAPIMappings()
    {
        restServer.attachMapping (
            RestServer.RESTVerbEnum.get,
            "/test",
            ( Request aInRequest, Response aInResponse ) ->
            {
                try {
                    logger.info ( "aInRequest.pathInfo(): [{}]", aInRequest.pathInfo() );
                    aInResponse.status(200);
                    return RestResult.ok.toString();
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.get,
            "/get-latest-status",
            ( Request aInRequest, Response aInResponse ) ->
            {
                try {
                    logger.info ( "aInRequest.pathInfo(): [{}]", aInRequest.pathInfo() );

                    aInResponse.status(200);
                    return GsonUtil.serialize(
                        registryStatus.setCurrentTime(System.currentTimeMillis() )
                    );
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/register",
            ( final Request aInRequest, final Response aInResponse ) ->
            {
                try {
                    logger.info ( "aInRequest.pathInfo(): [{}]", aInRequest.pathInfo() );
                    logger.info ( "aInRequest.body(): [{}]", aInRequest.body () );
                    logger.info ( "aInRequest.ip(): [{}]", aInRequest.ip() );

                    executor.submit(
                        () -> {
                            final SubscriberRegistrationInfo subscriberRegistrationInfo = GsonUtil.deSerialize (
                                aInRequest.body (),
                                SubscriberRegistrationInfo.class );

                            subscriberRegistrationInfo.setClientWanIPAddress( aInRequest.ip() );
                            subscriberRegistrationInfo.setRegistrationTime( System.currentTimeMillis() );

                            logger.info ( "subscriberRegistrationInfo: {}", GsonUtil.serialize( subscriberRegistrationInfo ) );

                            map.put( subscriberRegistrationInfo.getClientId(), subscriberRegistrationInfo );

                            logger.info ( "map: {}", GsonUtil.serialize( map ) );
                        });

                    aInResponse.status( 200 );

                    return RestResult.ok.toString();
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/unregister",
            ( final Request aInRequest, final Response aInResponse ) ->
            {
                try {
                    logger.info ( "aInRequest.pathInfo(): [{}]", aInRequest.pathInfo() );
                    logger.info ( "aInRequest.body (): [{}]", aInRequest.body () );

                    executor.submit(
                        () -> {
                            final SubscriberRegistrationInfo subscriberRegistrationInfo = GsonUtil.deSerialize (
                                aInRequest.body (),
                                SubscriberRegistrationInfo.class );

                            logger.info ( "subscriberRegistrationInfo: {}", GsonUtil.serialize( subscriberRegistrationInfo ) );

                            if ( !map.containsKey( subscriberRegistrationInfo.getClientId() ) ) {
                                logger.warn ( "not present in map, subscriberRegistrationInfo.getClientId(): {}", subscriberRegistrationInfo.getClientId() );
                            }
                            else {
                                map.remove( subscriberRegistrationInfo.getClientId() );
                            }
                        });

                    aInResponse.status( 200 );

                    return RestResult.ok.toString();
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/get-registrations",
            ( final Request aInRequest, final Response aInResponse ) ->
            {
                try {
                    logger.info ( "aInRequest.pathInfo(): [{}]", aInRequest.pathInfo() );
                    logger.info ( "aInRequest.body (): [{}]", aInRequest.body () );

                    final SubscribersQueryInfo subscribersQueryInfo = GsonUtil.deSerialize (
                        aInRequest.body (),
                        SubscribersQueryInfo.class );
                    logger.info ( "subscribersQueryInfo: {}", GsonUtil.serialize( subscribersQueryInfo ) );

                    aInResponse.status( 200 );

                    final SubscriberRegistrationInfoMap subscriberRegistrationInfoMap = new SubscriberRegistrationInfoMap();
                    subscriberRegistrationInfoMap.setMap( iFilterMap( subscribersQueryInfo ) );

                    final String response = GsonUtil.serialize( subscriberRegistrationInfoMap );
                    logger.info ( "subscriberRegistrationInfoMap: {}", response );

                    return response;
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/set-log-level",
            ( Request aInRequest, Response aInResponse ) ->
            {
                try {
                    final String logLevel = aInRequest.body().trim().toLowerCase();
                    registryStatus.setLogLevel(logLevel);
                    LogConfigurator.singleton().setLevel(logLevel);
                    logger.info ( "new log level:{}", logLevel );

                    aInResponse.status(200);

                    return RestResult.ok.toString();
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );
    }

    private Map<String, SubscriberRegistrationInfo> iFilterMap ( SubscribersQueryInfo subscribersQueryInfo ) {
        final Map<String, SubscriberRegistrationInfo> filtered = new HashMap<>();
        for ( Map.Entry<String, SubscriberRegistrationInfo> next : map.entrySet() ) {
            final String regexp = subscribersQueryInfo.getGroupName();
            if ( regexp != null && ( regexp.length() >= 6 ) && regexp.equals( next.getValue().getGroupName() ) )
            {
                filtered.put(next.getKey(), next.getValue());
            }
        }
        return filtered;
    }

    private void iCleanupOldEntries() {
        for ( Map.Entry<String, SubscriberRegistrationInfo> entry : map.entrySet() ) {
            final long ageTimeInMSecs = System.currentTimeMillis() - entry.getValue().getRegistrationTime();
            logger.info( "entry:{}, ageTimeInMSecs:{}, evictionTimeInMSecs:{}", entry, ageTimeInMSecs, evictionTimeInMSecs );
            if ( ageTimeInMSecs > evictionTimeInMSecs ) {
                logger.info ( "removing:{}", entry );
                map.remove( entry.getKey() );
            }
        }
    }

    private void iStopApp() throws Exception
    {
        if (restServer != null)
        {
            restServer.shutdown();
            restServer = null;
        }

        executor.shutdownNow();
        scheduledExecutor.shutdownNow();
    }

    private void iAddShutdownHook()
    {
        logger.debug ( "iAddShutdownHook - BEGIN" );

        final Thread terminationThread = new Thread (
            () ->
            {
                try
                {
                    logger.debug ( "shutdown hook fired - BEGIN" );
                    terminate();
                    logger.debug ( "shutdown hook fired - END" );
                }

                catch ( Exception ex )
                {
                    logger.error ( ex.getMessage(), ex );
                }
            }
        );

        // we want this thread to -prevent- the JVM of exiting,
        // so this should not be a daemon thread, then
        terminationThread.setDaemon ( false );

        Runtime.getRuntime ().addShutdownHook ( terminationThread );

        logger.debug ( "iAddShutdownHook - END" );
    }

    public void terminate() throws Exception {
        iStopApp();
        terminationWaitingLatch.countDown();
    }

    private void iInitAppStatus( String[] args ) throws UnknownHostException {
        registryStatus
            .setAppStartTime( System.currentTimeMillis() )
            .setAppVersion( SystemUtil.getGitVersion ( true ) )
            .setOsName( System.getProperty ( "os.name" ) )
            .setOsVersion( System.getProperty ( "os.version" ) )
            .setOsArch( System.getProperty ( "os.arch" ) )
            .setNumberOfProcessors( Runtime.getRuntime().availableProcessors() )
            .setCmdLineArgs( String.join( ", ", Arrays.asList( args ) ) )
            .setProcessId( SystemUtil.getPIDForThisProcess("n/a") )
            .setCurrentDirectory( SystemUtil.getCurrentDirectory().getAbsolutePath() )
            .setJavaArgs( String.join( ", ", SystemUtil.getJavaExtraArguments ( ) ) )
            .setJavaHome( SystemUtil.getEnvironmentOrSysProperty ( "java.home" ) )
            .setJavaRuntimeVersion( SystemUtil.getEnvironmentOrSysProperty ( "java.runtime.version" ) );
    }
}
