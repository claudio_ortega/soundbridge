/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.restserver;

public class RestServerBuilder
{
    private String hostNameOrIpAddress;
    private int port;
    private int minConnections;
    private int maxConnections;

    private RestServerBuilder()
    {
        hostNameOrIpAddress = "";
        port = 8090;
        minConnections = 10;
        maxConnections = 10;
    }

    public static RestServerBuilder create()
    {
        return new RestServerBuilder ( );
    }

    public int getPort ( )
    {
        return port;
    }

    public RestServerBuilder setPort ( int value )
    {
        port = value;
        return this;
    }

    public int getMinConnections ( )
    {
        return minConnections;
    }

    public RestServerBuilder setMinConnections ( int value )
    {
        minConnections = value;
        return this;
    }

    public int getMaxConnections ( )
    {
        return maxConnections;
    }

    public RestServerBuilder setMaxConnections ( int value )
    {
        maxConnections = value;
        return this;
    }

    public String getHostNameOrIpAddress( )
    {
        return hostNameOrIpAddress;
    }

    public RestServerBuilder setHostNameOrIpAddress ( String value )
    {
        hostNameOrIpAddress = value;
        return this;
    }

    @Override
    public String toString() {
        return "RestServerBuilder{" +
            "hostNameOrIpAddress='" + hostNameOrIpAddress + '\'' +
            ", port=" + port +
            ", minConnections=" + minConnections +
            ", maxConnections=" + maxConnections +
            '}';
    }
}
