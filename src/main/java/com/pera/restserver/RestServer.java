/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.restserver;

import com.google.common.base.Preconditions;
import com.pera.util.SystemUtil;
import org.apache.logging.log4j.*;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class RestServer
{
    private final Logger logger = LogManager.getLogger();

    private final CountDownLatch triggerShutDownLatch;
    private final CountDownLatch doneShutDownLatch;
    private final AtomicBoolean started;
    private final RestServerBuilder builder;
    private String effectiveIP;

    public static RestServer create ( RestServerBuilder builder )
    {
        return new RestServer( builder );
    }

    private RestServer(RestServerBuilder aInBuilder )
    {
        builder = aInBuilder;

        triggerShutDownLatch = new CountDownLatch ( 1 );
        doneShutDownLatch = new CountDownLatch ( 1 );

        started = new AtomicBoolean ( false );
    }

    public String getEffectiveIP() {
        return effectiveIP;
    }

    public AtomicBoolean isStarted() {
        return started;
    }

    public synchronized RestServer startUp ( ) {
        logger.info ( "startUp -- BEGIN" );

        Preconditions.checkState ( !started.get () );

        Spark.port ( builder.getPort () );

        effectiveIP = SystemUtil.getAValidLocalIP( builder.getHostNameOrIpAddress(), true );

        Spark.ipAddress ( effectiveIP );

        Spark.threadPool ( builder.getMaxConnections (), builder.getMinConnections (), 5_000 );

        new Thread (
            () ->
            {
                try
                {
                    triggerShutDownLatch.await ( );

                    logger.info ( "http server will now shut down..." );
                    Spark.stop ( );
                    logger.info ( "http server is shutting down, waiting for tcp port {} to go down ...", builder.getPort() );
                }

                catch ( Exception ex )
                {
                    logger.error ( ex.getMessage (), ex );
                }

                finally
                {
                    doneShutDownLatch.countDown ();
                }
            } ).start ();

        logger.info ( "waiting for server initialization" );

        started.set ( true );

        // it should exist at least one mapping before calling Spark.awaitInitialization(), this seems to me like a bug in Spark
        Spark.get (
            "/do-not-use-or-find-me",
            ( Request aInRequest, Response aInResponse ) -> new Date ().toString () );

        Spark.awaitInitialization ();

        logger.info ( "startUp -- END" );

        return this;
    }

    public RestServer shutdown()
    {
        logger.info ( "receiving server shutdown request -- BEGIN" );

        if ( triggerShutDownLatch.getCount () == 0 )
        {
            logger.info ( "shutdown request has already been received, ignoring this one" );
        }

        else
        {
            triggerShutDownLatch.countDown ();
        }

        logger.info ( "receiving server shutdown request -- END" );

        return this;
    }

    public RestServer waitForTermination(long timeout, TimeUnit unit ) throws Exception
    {
        logger.info ( "waitForTermination() -- BEGIN" );

        if ( ! started.get() )
        {
            logger.info ( "server is either already shut down or it has never started" );
        }

        else
        {
            if ( timeout == 0 )
            {
                logger.info ( "waiting forever on the latch.." );
                doneShutDownLatch.await ();
            }
            else
            {
                logger.info ( "waiting for {} {} on the latch..", timeout, unit.toString().toLowerCase() );
                final boolean elapsed = ! doneShutDownLatch.await ( timeout, unit );
                Preconditions.checkState( ! elapsed );
            }
        }

        logger.info ( "waitForTermination() -- END" );

        return this;
    }

    public enum RESTVerbEnum { post, put, get }

    public RestServer attachMapping (
        RESTVerbEnum aInRESTVerbEnum,
        String aInRequest,
        Route aInRoute ) {
        if ( aInRESTVerbEnum == RESTVerbEnum.put )
        {
            Spark.put ( aInRequest, aInRoute );
        }

        else if ( aInRESTVerbEnum == RESTVerbEnum.post )
        {
            Spark.post ( aInRequest, aInRoute );
        }

        else if ( aInRESTVerbEnum == RESTVerbEnum.get )
        {
            Spark.get ( aInRequest, aInRoute );
        }

        else
        {
            Preconditions.checkArgument ( false,  "add Verb: " + aInRESTVerbEnum );
        }

        return this;
    }
}
