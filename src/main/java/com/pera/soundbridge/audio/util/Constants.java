/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.util;

public class Constants
{
    private Constants() {}
    public static final int UDP_MAX_PACKET_ACCEPTED_LENGTH = 65535;
}
