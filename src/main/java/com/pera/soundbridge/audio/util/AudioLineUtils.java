/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.util;

import com.google.common.base.Preconditions;
import com.pera.util.CollectionsUtil;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class AudioLineUtils
{
    public static int iGet8BitsIntoPositiveModule256 ( int x )
    {
        return ( x < 0 ) ? x + 256 : x;
    }

    public static int convert16BitsIntoSignedNumber(int x )
    {
        Preconditions.checkArgument( x >= 0 );
        return x > (0x8000 - 1) ? x - 0x10000 : x;
    }

    public static int getIntFromBytes (byte msb, byte lsb )
    {
        final int lLsb = iGet8BitsIntoPositiveModule256 ( ( int ) lsb );
        final int lMsb = iGet8BitsIntoPositiveModule256 ( ( int ) msb );
        return convert16BitsIntoSignedNumber( 256 * lMsb + lLsb );
    }

    public static long getLongFromBytes (byte[] buf, int offset, int length )
    {
        Preconditions.checkArgument( length <= 8 );
        final ByteBuffer bb = ByteBuffer.allocate(length);
        bb.put( buf, offset, length );
        bb.flip();
        return bb.getLong();
    }

    public static long convert3PositiveBytesIntoSignedNumber (int msb, int middle, int lsb)
    {
        Preconditions.checkArgument( msb >= 0 && msb < 0x1_00 );
        Preconditions.checkArgument( middle >= 0 && middle < 0x1_00 );
        Preconditions.checkArgument( lsb >= 0 && lsb < 0x1_00 );

        final int x = 0x1_00_00 * msb + 0x1_00 * middle + lsb;
        final int r = x > 0x7f_ff_ff ? x - 0x1_00_00_00 : x;

        Preconditions.checkArgument( r <=  0xff_ff_ff );
        Preconditions.checkArgument( r >= -0x1_00_00_00 );

        return r;
    }

    public static byte[] getBytesFromLong (long x)
    {
        final ByteBuffer bb = ByteBuffer.allocate(8);
        bb.putLong(0, x);
        return bb.array();
    }

    public static byte getLsbFromInt (int in)
    {
        return (byte) ( in & 0xff );
    }

    public static byte getMsbFromInt (int in)
    {
        return (byte) ( ( in >> 8 ) & 0xff );
    }

    public static void printCounters(
         Map<String,Number> counters,
         Consumer<Map.Entry<String,Number>> action)
    {
        final List<java.util.Map.Entry<String,Number>> keyList = CollectionsUtil.createList( counters.entrySet() );
        keyList.sort( (a, b) -> a.getKey().compareTo( b.getKey() ) );
        keyList.forEach( action );
    }
}
