/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.util;

public class SocketEndPoint
{
    private String ipAddress;
    private int port;

    public String getIpAddress() {
        return ipAddress;
    }

    public int getPort() {
        return port;
    }

    public SocketEndPoint(String inIpAddress, int inPort)
    {
        ipAddress = inIpAddress;
        port = inPort;
    }
}
