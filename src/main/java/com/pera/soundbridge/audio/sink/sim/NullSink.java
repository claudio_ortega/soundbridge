/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.sink.sim;

import com.pera.soundbridge.audio.sink.ISink;
import com.pera.soundbridge.vessel.Vessel;

import java.util.Map;
import java.util.concurrent.*;

public class NullSink implements ISink
{
    public static NullSink create()
    {
        return new NullSink();
    }

    private NullSink()
    {
    }

    @Override
    public void consume( Vessel aInOutMCVessel )
    {
    }

    @Override
    public void init ( )
    {
    }

    @Override
    public void close ( )
    {
    }

    @Override
    public void start() {
    }

    @Override
    public void stop() {
    }

    @Override
    public boolean waitForStop(long timeout, TimeUnit unit)
    {
        return true;
    }

    @Override
    public long getConsumedSinkSamplesCount()
    {
        return 0;
    }

    @Override
    public long getDroppedSinkSamplesCount( )
    {
        return 0;
    }

    @Override
    public void getCounters ( Map<String, Number> x )
    {
    }

    @Override
    public String getId()
    {
        return "null";
    }

    @Override
    public boolean isOperational()
    {
        return true;
    }
}
