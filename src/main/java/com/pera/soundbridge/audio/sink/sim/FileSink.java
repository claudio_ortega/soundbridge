/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.sink.sim;

import com.pera.soundbridge.audio.sink.ISink;
import com.pera.soundbridge.vessel.Vessel;
import com.pera.util.FileUtil;
import org.apache.logging.log4j.*;
import java.io.File;
import java.io.PrintStream;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class FileSink implements ISink
{
    private final Logger logger = LogManager.getLogger();

    private PrintStream datafilePS;
    private final AtomicLong currentSamplesCount;
    private final String subDir;

    public static FileSink create(String inSubDir )
    {
        return new FileSink( inSubDir );
    }

    private FileSink(String inSubDir )
    {
        subDir = inSubDir;
        currentSamplesCount = new AtomicLong( 0 );
    }

    @Override
    public void consume( Vessel aInOutMCVessel )
    {
        final int numChannels = aInOutMCVessel.getNumberOfChannels ();

        for (int sampleIndex = 0; sampleIndex < aInOutMCVessel.getNumberOfSamples(); sampleIndex++ )
        {
            currentSamplesCount.incrementAndGet ();

            for ( int channelIndex = 0; channelIndex < numChannels; channelIndex++ )
            {
                datafilePS.print ( String.format ( "%d", aInOutMCVessel.getSample (channelIndex, sampleIndex ) ) );

                if ( channelIndex < (numChannels-1) )
                {
                    datafilePS.print("; ");
                }
            }

            datafilePS.println ();
        }
    }

    @Override
    public String getId()
    {
        return subDir;
    }

    @Override
    public void init ( )  throws Exception
    {
        final File dataFile = new File ( String.format ( "tmp/%s/datafile-%s.csv", subDir, System.identityHashCode ( this ) ) );
        FileUtil.deletePlainFileIfExists ( dataFile, true );
        FileUtil.createDirIfDoesNotExist ( dataFile.getParentFile() );
        datafilePS = FileUtil.createPrintStreamFromFile( dataFile, false );
        currentSamplesCount.set(0);
        logger.info ( "successfully opened file:{}", dataFile.getAbsolutePath () );
    }

    @Override
    public void close ( )
    {
        datafilePS.close ();
        logger.info ( "successfully closed data file" );
    }

    @Override
    public void start() {
    }

    @Override
    public void stop() {
    }

    @Override
    public boolean waitForStop(long timeout, TimeUnit unit)
    {
        return true;
    }

    @Override
    public long getConsumedSinkSamplesCount()
    {
        return currentSamplesCount.get();
    }

    @Override
    public long getDroppedSinkSamplesCount( )
    {
        return 0;
    }

    @Override
    public void getCounters ( Map<String, Number> x )
    {
        x.put( String.format( "%s.%s.currentSamplesCount", getClass().getSimpleName(), getId() ), currentSamplesCount.get() );
    }

    @Override
    public boolean isOperational()
    {
        return true;
    }
}

