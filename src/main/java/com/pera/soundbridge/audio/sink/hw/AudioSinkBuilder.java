/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.sink.hw;

import com.google.gson.annotations.Expose;
import com.pera.soundbridge.audio.common.AudioCommon;
import com.pera.soundbridge.audio.common.DirectionEnum;
import com.pera.soundbridge.audio.common.IAudioBuilder;
import com.pera.soundbridge.audio.common.FSEnum;

public class AudioSinkBuilder implements IAudioBuilder
{
    @Expose private final DirectionEnum direction;
    @Expose private FSEnum samplingFrequencyEnum;
    @Expose private int numberOfChannels;
    @Expose private String audioMixerDescriptionRegexp;
    @Expose private final int sampleSizeInBytes;
    @Expose private String id;
    @Expose private int maximumEnqueuedBuffers;
    @Expose private int sinkDropPacketsModulus;
    @Expose private int sinkDropPacketsSilenceThreshold;
    @Expose private int bufferLengthInMSec;

    public AudioSinkBuilder create ()
    {
        return this;
    }

    private AudioSinkBuilder()
    {
        id = "n/a";
        numberOfChannels = 1;
        sampleSizeInBytes = 2;
        samplingFrequencyEnum = FSEnum.value8000;
        audioMixerDescriptionRegexp = "";
        maximumEnqueuedBuffers = -1;
        sinkDropPacketsModulus = -1;
        sinkDropPacketsSilenceThreshold = -1;
        bufferLengthInMSec = 20;
        direction = DirectionEnum.out;
    }

    public static AudioSinkBuilder init()
    {
        return new AudioSinkBuilder();
    }

    @Override
    public int getBufferLengthInMSec() {
        return bufferLengthInMSec;
    }

    public AudioSinkBuilder setBufferLengthInMSec(int value) {
        bufferLengthInMSec = value;
        return this;
    }

    @Override
    public int getMaximumEnqueuedBuffers() {
        return maximumEnqueuedBuffers;
    }

    public AudioSinkBuilder setMaximumEnqueuedBuffers(int value) {
        maximumEnqueuedBuffers = value;
        return this;
    }

    @Override
    public String getAudioMixerDescriptionRegexp()
    {
        return audioMixerDescriptionRegexp;
    }

    public AudioSinkBuilder setAudioMixerDescriptionRegexp(String value)
    {
        audioMixerDescriptionRegexp = value;
        return this;
    }

    @Override
    public FSEnum getSamplingFrequencyEnum ( )
    {
        return samplingFrequencyEnum;
    }

    public AudioSinkBuilder setSamplingFrequencyEnum (FSEnum value )
    {
        samplingFrequencyEnum = value;
        return this;
    }

    @Override
    public int getNumberOfChannels ( )
    {
        return numberOfChannels;
    }

    public AudioSinkBuilder setNumberOfChannels (int value)
    {
        numberOfChannels = value;
        return this;
    }

    @Override
    public int getSampleSizeInBytes()
    {
        return sampleSizeInBytes;
    }

    @Override
    public String getId()
    {
        return id;
    }

    public AudioSinkBuilder setId(String value)
    {
        id = value;
        return this;
    }

    @Override
    public DirectionEnum getDirection()
    {
        return direction;
    }

    public int getSinkDropPacketsModulus() {
        return sinkDropPacketsModulus;
    }

    public AudioSinkBuilder setSinkDropPacketsModulus(int sinkDropPacketsModulus) {
        this.sinkDropPacketsModulus = sinkDropPacketsModulus;
        return this;
    }

    public int getSinkDropPacketsSilenceThreshold() {
        return sinkDropPacketsSilenceThreshold;
    }

    public AudioSinkBuilder setSinkDropPacketsSilenceThreshold(int sinkDropPacketsSilenceThreshold) {
        this.sinkDropPacketsSilenceThreshold = sinkDropPacketsSilenceThreshold;
        return this;
    }

    @Override
    public int getConfiguredBufferSizeInBytes() {
        return AudioCommon.getBufferSizeInBytesModulusFrame(
            numberOfChannels * sampleSizeInBytes,
            samplingFrequencyEnum,
            bufferLengthInMSec );
    }
}
