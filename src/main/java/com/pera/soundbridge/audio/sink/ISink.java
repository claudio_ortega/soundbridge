/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.sink;

import com.pera.soundbridge.audio.*;
import com.pera.soundbridge.vessel.Vessel;

public interface ISink extends IInitiable, IIdentifiable, IObservable, IStartable
{
    long getConsumedSinkSamplesCount();
    long getDroppedSinkSamplesCount( );
    void consume( Vessel aInOutMCVessel )  throws Exception;
}
