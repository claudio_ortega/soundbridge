/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.sink.sim;

import com.pera.soundbridge.audio.sink.ISink;
import com.pera.soundbridge.vessel.*;
import org.apache.logging.log4j.*;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class DebugSink implements ISink
{
    private final Logger logger = LogManager.getLogger();

    private final AtomicLong currentSamplesCount;
    private final String id;
    private final VesselSequenceChecker checker;

    public static DebugSink create ( String inId, long inMaxWindow )
    {
        return new DebugSink( inId, inMaxWindow );
    }

    private DebugSink ( String inId, long inMaxWindow )
    {
        id = inId;
        currentSamplesCount = new AtomicLong ( 0 );
        checker = VesselSequenceChecker.create( inId , VesselType.Signal, inMaxWindow );
    }

    @Override
    public void consume( Vessel vessel )
    {
        checker.accept( vessel );

        currentSamplesCount.addAndGet ( vessel.getNumberOfSamples() );

        logger.info( "vessel serial/samples/total/oos: {},{},{},{}",
            () -> vessel.getSerialNumber(),
            () -> vessel.getNumberOfSamples(),
            () -> currentSamplesCount.get(),
            () -> checker.getVesselOutOfSequenceCount() );

        for (int ii = 0; ii < vessel.getNumberOfChannels(); ii++) {
            final int i = ii;
            logger.debug("{}", () -> String.format("channel(%d): mean:  %.2f", i, vessel.getChannel(i).computeMean()));
            logger.debug("{}", () -> String.format("channel(%d): power: %.2f", i, vessel.getChannel(i).computePower()));
        }
    }

    @Override
    public void init ( )
    {
        currentSamplesCount.set(0);
    }

    @Override
    public void close ( )
    {
        logger.info ( "currentSamplesCount:{}", currentSamplesCount.get() );
    }

    @Override
    public void start() {
    }

    @Override
    public void stop() {
    }

    @Override
    public boolean waitForStop(long timeout, TimeUnit unit)
    {
        return true;
    }

    @Override
    public long getConsumedSinkSamplesCount()
    {
        return currentSamplesCount.get();
    }

    @Override
    public long getDroppedSinkSamplesCount( )
    {
        return 0;
    }

    @Override
    public void getCounters ( Map<String, Number> m )
    {
        m.put( String.format( "%s.%s.currentSamplesCount", getClass().getSimpleName(), getId() ), currentSamplesCount.get() );
        checker.getCounters( m );
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public boolean isOperational()
    {
        return true;
    }
}

