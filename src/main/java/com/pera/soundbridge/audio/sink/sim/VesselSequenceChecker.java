/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.sink.sim;

import com.pera.soundbridge.audio.IObservable;
import com.pera.soundbridge.vessel.*;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

public class VesselSequenceChecker implements IObservable, Consumer<Vessel>
{
    private final AtomicLong vesselCount;
    private final AtomicLong lastVesselSerial;
    private final AtomicLong vesselOutOfSequenceCount;
    private final Map<Long,Long> vesselDeltaSerialDistribution;
    private final String id;
    private final long maxWindow;
    private final VesselType vesselType;

    public static VesselSequenceChecker create (
        String inId,
        VesselType inVesselType,
        long inMaxWindow )
    {
        return new VesselSequenceChecker ( inId, inVesselType, inMaxWindow );
    }

    private VesselSequenceChecker (
        String inId,
        VesselType inVesselType,
        long inMaxWindow  )
    {
        id = inId;
        vesselType = inVesselType;
        vesselCount = new AtomicLong ( 0 );
        lastVesselSerial = new AtomicLong ( 0 );
        vesselOutOfSequenceCount = new AtomicLong ( 0 );
        vesselDeltaSerialDistribution = new ConcurrentHashMap<>();
        maxWindow = inMaxWindow;
    }

    @Override
    public void accept ( Vessel vessel )
    {
        if ( vessel.getType() == vesselType )
        {
            final long thisSerial = vessel.getSerialNumber();

            if ( vesselCount.get () > 0  ) {

                final long shift = thisSerial - lastVesselSerial.get();

                if ( shift != 1 && ( maxWindow > 0 && Math.abs( shift ) < maxWindow ) )
                {
                    vesselOutOfSequenceCount.incrementAndGet();
                    vesselDeltaSerialDistribution.compute(
                        shift,
                        (key, value) -> (value == null) ? 1 : value + 1 );
                }
            }

            lastVesselSerial.set ( thisSerial );
            vesselCount.incrementAndGet ();
        }
    }

    public long getVesselOutOfSequenceCount( )
    {
        return vesselOutOfSequenceCount.get();
    }

    public long getTotalCount ( )
    {
        return vesselCount.get();
    }

    @Override
    public void getCounters(Map<String, Number> m)
    {
        m.put( String.format( "%s.%s.vesselCount", getClass().getSimpleName(), id ), vesselCount.get() );
        m.put( String.format( "%s.%s.lastVesselSerial", getClass().getSimpleName(), id ), lastVesselSerial.get() );
        m.put( String.format( "%s.%s.vesselOutOfSequenceCount", getClass().getSimpleName(), id ), vesselOutOfSequenceCount.get() );

        vesselDeltaSerialDistribution.forEach(
            (k,v) -> m.put(
                  String.format( "%s.%s.vesselDeltaSerialDistribution.%d", getClass().getSimpleName(), id, k ),
                  v )
        );
    }

    @Override
    public boolean isOperational()
    {
        return true;
    }

    @Override
    public String toString() {
        return "VesselSequenceChecker{" +
            "vesselCount=" + vesselCount +
            ", lastVesselSerial=" + lastVesselSerial +
            ", vesselOutOfSequenceCount=" + vesselOutOfSequenceCount +
            ", vesselDeltaSerialDistribution=" + vesselDeltaSerialDistribution +
            ", id='" + id + '\'' +
            '}';
    }
}
