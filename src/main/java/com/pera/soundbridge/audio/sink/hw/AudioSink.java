/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.sink.hw;

import com.google.common.base.Preconditions;
import com.pera.soundbridge.audio.common.AudioCommon;
import com.pera.soundbridge.audio.sink.ISink;
import com.pera.soundbridge.vessel.*;
import com.pera.util.GsonUtil;
import com.pera.util.TaggedThreadFactory;
import org.apache.logging.log4j.*;
import javax.sound.sampled.*;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class AudioSink implements ISink
{
    private final static Logger logger = LogManager.getLogger();

    private final AudioSinkBuilder builder;
    private final BlockingQueue<Vessel> queue;
    private final ThreadPoolExecutor executor;
    private final Object lineGuard;
    private final AtomicBoolean cancel;
    private final AtomicLong droppedSamplesCount;
    private final AtomicLong receivedSamplesCount;
    private final AtomicLong playedSamplesCount;
    private volatile Mixer.Info mixerInfo;
    private volatile SourceDataLine line;

    public static AudioSink create(AudioSinkBuilder inBuilder )
    {
        return new AudioSink( inBuilder );
    }

    private AudioSink(AudioSinkBuilder inBuilder )
    {
        builder = inBuilder;
        queue = new ArrayBlockingQueue<>(inBuilder.getMaximumEnqueuedBuffers());
        receivedSamplesCount = new AtomicLong(0);
        droppedSamplesCount = new AtomicLong(0);
        playedSamplesCount = new AtomicLong(0);
        lineGuard = new Object();
        cancel = new AtomicBoolean( false );
        mixerInfo = null;
        line = null;
        executor = new ThreadPoolExecutor (
            1,
            1,
            0,
            TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>( ),
            TaggedThreadFactory.create( getClass ( ), inBuilder.getId() ) );
    }

    @Override
    public void init ( )
    {
        synchronized ( lineGuard )
        {
            if ( line != null )
            {
                logger.warn("the line was initialized before");
            }

            else
            {
                final List<Mixer.Info> lCompatibleMixers = AudioCommon.getMixers(
                    false,
                    true,
                    builder,
                    false );

                if (lCompatibleMixers.size() == 0) {
                    mixerInfo = null;
                    line = null;
                    logger.error( "builder:{}", GsonUtil.serialize( builder ) );
                    throw new IllegalStateException("the application will not work under this configuration, as there is no compatible sink mixers in the system");
                }
                else if (lCompatibleMixers.size() > 1 ) {
                    mixerInfo = null;
                    line = null;
                    logger.error( "builder:{}", GsonUtil.serialize( builder ) );
                    lCompatibleMixers.forEach( x -> logger.error( "mixer.info:{}", x ) );
                    throw new IllegalStateException("the application will not work under this configuration, there are more then one compatible sink mixers matching");
                }
                else {
                    mixerInfo = lCompatibleMixers.get(0);

                    logger.info(
                        "the system will use this compatible sink mixer: [{}]",
                        () -> AudioCommon.getMixerInfo( mixerInfo ).description );

                    final AudioCommon.CreationResult lCr = AudioCommon.attemptLineCreationFromMixerInfo(
                        builder,
                        mixerInfo,
                        true );

                    if ( ! lCr.success ) {
                        line = null;
                        logger.error("the attempt to open the line was unsuccessful, error msg:{}", lCr.errorMsg);
                        throw new IllegalStateException("the application will not work under this configuration");
                    }

                    else {
                        logger.info("the line opening attempt was successful");
                        line = (SourceDataLine) lCr.line;
                        Preconditions.checkNotNull( line );

                        logger.debug(
                            "the attempt to open the line was successful, mixer info:{}",
                            () -> AudioCommon.getMixerInfo( mixerInfo ).description );

                        executor.submit (
                            ( ) ->
                            {
                                logger.debug ( "executor.run() -- BEGIN" );

                                try
                                {
                                    synchronized ( lineGuard )
                                    {
                                        queue.clear();

                                        line.addLineListener(event -> logger.info("line event:{}", event));
                                        AudioCommon.setMaximumVolume( line );

                                        final int configuredBufferSizeInBytes = builder.getConfiguredBufferSizeInBytes();

                                        logger.info(
                                                "expected delay in audio sink: {} mSec",
                                                1000.0 * configuredBufferSizeInBytes /
                                                        ( (float) builder.getSampleSizeInBytes() *
                                                                builder.getNumberOfChannels() *
                                                                builder.getSamplingFrequencyEnum().getValue() )
                                        );

                                        line.open( AudioCommon.createAudioFormat( builder ), configuredBufferSizeInBytes );

                                        line.start();
                                        logger.info("line.getBufferSize():{}", line.getBufferSize() );

                                        while ( ! cancel.get() )
                                        {
                                            try
                                            {
                                                final Vessel vessel = queue.poll(1, TimeUnit.SECONDS);

                                                if ( vessel == null ) {
                                                    logger.debug("vessel queue is still empty after timing out");
                                                }

                                                else if ( vessel.getType() == VesselType.Null )  {
                                                    logger.debug("skipping null vessel: type:{}, serial:{}, size:{}",
                                                        vessel::getType,
                                                        vessel::getSerialNumber,
                                                        vessel::getNumberOfSamples);
                                                }

                                                else if ( vessel.getType() == VesselType.Probe ) {
                                                    logger.debug("skipping probe vessel: type:{}, serial:{}, size:{}",
                                                        vessel::getType,
                                                        vessel::getSerialNumber,
                                                        vessel::getNumberOfSamples);
                                                }

                                                else if ( builder.getSinkDropPacketsModulus() > 0 &&
                                                          builder.getSinkDropPacketsSilenceThreshold() > 0 &&
                                                        ( vessel.getSerialNumber() % builder.getSinkDropPacketsModulus() ) == 0 &&
                                                          isVesselLowPower( vessel, builder.getSinkDropPacketsSilenceThreshold() )  ) {
                                                    droppedSamplesCount.addAndGet( vessel.getNumberOfSamples() );
                                                    logger.trace("skipping low power signal vessel: type:{}, serial:{}, size:{}",
                                                        vessel::getType,
                                                        vessel::getSerialNumber,
                                                        vessel::getNumberOfSamples);
                                                }

                                                else {
                                                    playedSamplesCount.addAndGet( vessel.getNumberOfSamples() );

                                                    final byte[] buffer = vessel.produceAudioBuffer();
                                                    final int bytesWritten = line.write(buffer, 0, buffer.length);

                                                    if ( buffer.length != bytesWritten ) {
                                                        logger.warn("signal vessel: type:{}, serial:{}, size:{}, bytes:{}, written:{}",
                                                            vessel::getType,
                                                            vessel::getSerialNumber,
                                                            vessel::getNumberOfSamples,
                                                            () -> buffer.length,
                                                            () -> bytesWritten );
                                                    }
                                                    else {
                                                        if ( vessel.getSerialNumber() % 100 == 0 ) {
                                                            logger.debug ("signal vessel: type:{}, serial:{}, size:{}, bytes:{}, written:{}",
                                                                vessel::getType,
                                                                vessel::getSerialNumber,
                                                                vessel::getNumberOfSamples,
                                                                () -> buffer.length,
                                                                () -> bytesWritten );
                                                        }
                                                    }
                                                }
                                            }

                                            catch (InterruptedException ex)
                                            {
                                                logger.info("interrupted while waiting on the vessel queue, msg:{}", ex.getMessage());
                                            }
                                        }

                                        line.stop();
                                        line.flush();
                                        line.close();

                                        logger.debug("executor.run() -- END");
                                    }
                                }

                                catch ( Throwable ex )
                                {
                                    logger.warn ( "submission problem: {}", ex.getMessage ( ) );
                                }
                            }
                        );
                    }
                }
            }
        }
    }

    private static boolean isVesselLowPower( Vessel in, int silenceThreshold ) {

        float power = 0;

        for ( int i=0; i<in.getNumberOfChannels(); i++ ) {
            power += in.getChannel(0).computePower();
        }

        return power < silenceThreshold*silenceThreshold;
    }

    @Override
    public void consume( Vessel vessel ) throws InterruptedException
    {
        Preconditions.checkArgument( vessel != null, "vessel should not be null" );

        receivedSamplesCount.addAndGet( vessel.getNumberOfSamples() );

        if ( queue.size() > builder.getMaximumEnqueuedBuffers() / 10 )
        {
            droppedSamplesCount.addAndGet( vessel.getNumberOfSamples() );
            logger.debug( "dropping vessel: {}", vessel.debugSingleLiner() );
        }

        else
        {
            queue.put( vessel );
        }
    }

    @Override
    public void start() {
    }

    @Override
    public void stop() {
        cancel.set ( true );
    }

    @Override
    public boolean waitForStop(long timeout, TimeUnit unit)
    {
        return true;
    }

    @Override
    public void close() throws InterruptedException
    {
        cancel.set ( true );
        executor.shutdownNow ( );
    }

    @Override
    public void getCounters ( Map<String, Number> x )
    {
        x.put( String.format( "%s.%s.queue.size", getClass().getSimpleName(), getId() ), (long) queue.size() );
        x.put( String.format( "%s.%s.receivedSamplesCount", getClass().getSimpleName(), getId() ), receivedSamplesCount.get() );
        x.put( String.format( "%s.%s.droppedSamplesCount", getClass().getSimpleName(), getId() ), droppedSamplesCount.get() );
        x.put( String.format( "%s.%s.playedSamplesCount", getClass().getSimpleName(), getId() ), playedSamplesCount.get() );
    }

    @Override
    public boolean isOperational()
    {
        return line != null && line.isActive();
    }

    @Override
    public String getId()
    {
        return builder.getId();
    }

    @Override
    public long getConsumedSinkSamplesCount() {
        return receivedSamplesCount.get();
    }

    @Override
    public long getDroppedSinkSamplesCount( ) {
        return droppedSamplesCount.get();
    }
}


