/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio;

import java.util.concurrent.*;

public interface IStartable
{
    void start() throws Exception;
    void stop() throws InterruptedException;
    boolean waitForStop(long timeout, TimeUnit unit) throws InterruptedException;
}
