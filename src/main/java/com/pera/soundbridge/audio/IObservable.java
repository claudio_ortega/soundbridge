/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio;

import java.util.Map;

public interface IObservable
{
    void getCounters ( Map<String,Number> m );
    boolean isOperational();
}
