/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio;

public interface IInitiable
{
    void init () throws Exception;
    void close() throws InterruptedException;
}
