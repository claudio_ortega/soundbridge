/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.filter;

import com.pera.soundbridge.vessel.Vessel;

public class ZeroInPlaceFilter implements ISignalInPlaceFilter
{
    private final int channel;

    public static ZeroInPlaceFilter create( int inChannel )
    {
        return new ZeroInPlaceFilter( inChannel );
    }

    private ZeroInPlaceFilter( int inChannel )
    {
        channel = inChannel;
    }

    public void filter ( Vessel in )
    {
        for ( int i=0; i<in.getNumberOfSamples (); i++)
        {
            in.setSample ( channel, i, 0 );
        }
    }
}
