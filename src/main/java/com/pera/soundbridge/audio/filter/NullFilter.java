/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.filter;

import com.pera.soundbridge.vessel.Vessel;

public class NullFilter implements ISignalFilter
{
    public static NullFilter create()
    {
        return new NullFilter ();
    }

    private NullFilter ()
    {
    }

    @Override
    public Vessel filter (Vessel in )
    {
        return in;
    }
}
