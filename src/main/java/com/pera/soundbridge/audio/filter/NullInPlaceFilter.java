/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.filter;

import com.pera.soundbridge.vessel.Vessel;

public class NullInPlaceFilter implements ISignalInPlaceFilter
{
    public static NullInPlaceFilter create()
    {
        return new NullInPlaceFilter ();
    }

    private NullInPlaceFilter ()
    {
    }

    @Override
    public void filter ( Vessel in )
    {
    }
}
