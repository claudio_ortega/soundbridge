/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.filter;

import com.pera.soundbridge.vessel.Vessel;

public class SquareWaveInPlaceFilter implements ISignalInPlaceFilter
{
    private final int channel;
    private final int high;
    private final int period;
    private long lastIndex;

    /**
     *    inPeriod is in samples
     *    inHigh is the number of samples to get the square in the positive side,
     *    the rest of the period would be the equal negative, so if you need symmetry,
     *    make inHigh = inPeriod/2
     */
    public static SquareWaveInPlaceFilter create(
        int inChannel,
        int inHigh,
        int inPeriod )
    {
        return new SquareWaveInPlaceFilter( inChannel, inHigh, inPeriod );
    }

    private SquareWaveInPlaceFilter(
        int inChannel,
        int inHigh,
        int inPeriod )
    {
        channel = inChannel;
        high = inHigh;
        period = inPeriod;
        lastIndex = 0;
    }

    public void filter ( Vessel in )
    {
        final int amplitude = in.getBytesPerSample () == 2 ? 0x7fff : 0x7f;

        for ( int i=0; i<in.getNumberOfSamples (); i++)
        {
            in.setSample (
                channel,
                i,
                ( (lastIndex++ % period) < high ) ? amplitude : -amplitude );
        }
    }
}
