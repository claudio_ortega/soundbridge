/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.filter;

import com.pera.soundbridge.vessel.Vessel;

public interface ISignalInPlaceFilter
{
    void filter ( Vessel in );
}
