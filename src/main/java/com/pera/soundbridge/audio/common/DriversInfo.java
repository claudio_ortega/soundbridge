/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.common;

import com.google.gson.annotations.Expose;
import javax.sound.sampled.Mixer;
import java.util.LinkedList;
import java.util.List;

public class DriversInfo
{
    private List<Mixer.Info> allDrivers;
    private List<Mixer.Info> compatibleSourceMixers;
    private List<Mixer.Info> compatibleSinkMixers;

    @Expose private List<AudioCommon.MixerInfo> allDriversDescription;
    @Expose private List<AudioCommon.MixerInfo> compatibleSourceDescription;
    @Expose private List<AudioCommon.MixerInfo> compatibleSinkDescription;
    @Expose private boolean valid;

    public static DriversInfo create()
    {
        return new DriversInfo();
    }

    private DriversInfo()
    {
        valid = false;
        allDrivers = new LinkedList<>();
        compatibleSourceMixers = new LinkedList<>();
        compatibleSinkMixers = new LinkedList<>();
    }

    public void setAllDriversDescription(List<AudioCommon.MixerInfo> in) {
        allDriversDescription = in;
    }

    public void setCompatibleSourceDescription(List<AudioCommon.MixerInfo> in) {
        compatibleSourceDescription = in;
    }

    public List<AudioCommon.MixerInfo> getCompatibleSourceDescription() {
        return compatibleSourceDescription;
    }

    public List<AudioCommon.MixerInfo> getCompatibleSinkDescription() {
        return compatibleSinkDescription;
    }

    public void setCompatibleSinkDescription(List<AudioCommon.MixerInfo> in) {
        compatibleSinkDescription = in;
    }

    public List<Mixer.Info> getAllDrivers() {
        return allDrivers;
    }

    public void setAllDrivers(List<Mixer.Info> in) {
        allDrivers = in;
    }

    public List<Mixer.Info> getCompatibleSourceMixers() {
        return compatibleSourceMixers;
    }

    public void setCompatibleSourceMixers(List<Mixer.Info> in ) {
        compatibleSourceMixers = in;
    }

    public List<Mixer.Info> getCompatibleSinkMixers() {
        return compatibleSinkMixers;
    }

    public void setCompatibleSinkMixers( List<Mixer.Info> in ) {
        compatibleSinkMixers = in;
    }

    public void setValid(boolean in) {
        valid = in;
    }
}
