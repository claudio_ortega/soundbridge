/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.common;

public enum FSEnum
{
    value8000 ( 8000 ),
    value16000 ( 16000 ),
    value32000 ( 32000 ),
    value44100 ( 44100 ),
    value48000 ( 48000 ),
    value88200 ( 88200 ),
    value96000 ( 96000 );

    private final int value;

    FSEnum(int aInNumber )
    {
        value = aInNumber;
    }

    @Override
    public String toString ( )
    {
        return value + " Hz";
    }

    public int getValue ( )
    {
        return value;
    }
}
