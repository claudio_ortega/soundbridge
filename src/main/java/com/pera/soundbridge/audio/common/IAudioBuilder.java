/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.common;


public interface IAudioBuilder extends IBuilder
{
    String getAudioMixerDescriptionRegexp ( );
    DirectionEnum getDirection();
}
