/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.common;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.gson.annotations.Expose;
import com.pera.soundbridge.audio.sink.hw.AudioSinkBuilder;
import com.pera.soundbridge.audio.source.hw.AudioSourceBuilder;
import com.pera.soundbridge.restinfo.SoundBridgeConfig;
import com.pera.util.CollectionsUtil;
import com.pera.util.GsonUtil;
import com.pera.util.StringUtil;
import javax.sound.sampled.*;
import java.io.*;
import com.pera.util.SystemUtil;
import org.apache.logging.log4j.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class AudioCommon
{
    private final static Logger logger = LogManager.getLogger();

    private AudioCommon() {}

    public static List<String> getSuggestedHashes( DriversInfo driversInfo, String requestedHash )
    {
        final List<String> sourceHashes = AudioCommon.getHashes( driversInfo.getCompatibleSourceMixers() );
        final List<String> sinkHashes = AudioCommon.getHashes( driversInfo.getCompatibleSinkMixers() );
        final List<String> commonBothSidesHashes = iGetIntersection( sourceHashes, sinkHashes );
        return iComputeEffectiveConfigHashes( commonBothSidesHashes, requestedHash );
    }

    private static List<String> iGetIntersection( List<String> a, List<String> b )
    {
        final Set<String> as = ImmutableSet.copyOf( a );
        final Set<String> bs = ImmutableSet.copyOf( b );

        final Set<String> intersection = CollectionsUtil.createSet(as);
        intersection.retainAll( bs );

        return ImmutableList.copyOf( intersection );
    }

    /**
     *
     * @param compatibleBothSides
     * @param requestedHash
     * @return  null if it could not find a best unique compatible driver
     */
    private static List<String> iComputeEffectiveConfigHashes(
            List<String> compatibleBothSides,
            String requestedHash )
    {
        if ( ! requestedHash.isEmpty() ) {
            return ImmutableList.of( requestedHash );
        }

        else {
            return ImmutableList.copyOf( compatibleBothSides );
        }
    }

    public static List<String> getAllLines( DriversInfo driversInfo )
    {
        return CollectionsUtil.transformList (
            driversInfo.getAllDrivers(),
            AudioCommon::getMixerShowLine
        );
    }

    public static void listAllDrivers(
        PrintStream inPs,
        DriversInfo outDriversInfo )
    {
        inPs.println("all drivers -- BEGIN");
        outDriversInfo.getAllDrivers().forEach(
            mixerInfo -> inPs.println("    " + AudioCommon.getMixerShowLine( mixerInfo ) ) );
        inPs.println("all drivers -- END");
    }

    public interface Printer {
        void println(String x);
    }

    public static void listCompatibleDrivers(
        boolean includeSource,
        boolean includeSink,
        final Printer inPs,
        final DriversInfo driversInfo )
    {
        if ( includeSource ) {
            inPs.println("compatible source drivers -- BEGIN");
            driversInfo.getCompatibleSourceMixers().forEach(
                mixerInfo -> inPs.println("    " + AudioCommon.getMixerShowLine( mixerInfo ) ) );
            inPs.println("compatible source drivers -- END");
        }

        if ( includeSink ) {
            inPs.println("compatible sink drivers -- BEGIN");
            driversInfo.getCompatibleSinkMixers().forEach(
                mixerInfo -> inPs.println("    " + AudioCommon.getMixerShowLine( mixerInfo ) ) );
            inPs.println("compatible sink drivers -- END");
        }
    }

    public static List<Mixer.Info> filterRegexpCompatibleMixers(
        List<Mixer.Info> aInMixerList,
        String aInDriverNameRegexp ) {
        return CollectionsUtil.filterList (
            aInMixerList, aInTUnderTest -> iCheckMatchOnRegexp (
                aInDriverNameRegexp,
                getMixerInfo( aInTUnderTest ).hash ) );
    }

    public static boolean iCheckMatchOnRegexp(String aInRegexp, String aInTarget)
    {
        String lTmpRegexp = aInRegexp.trim ( );
        final boolean aInNegateRegexp = lTmpRegexp.startsWith ( "!" );

        if ( aInNegateRegexp )
        {
            lTmpRegexp = lTmpRegexp.substring ( 1 );
        }

        boolean lRet;

        if ( aInRegexp .contains ( ".*" ) )
        {
            lRet = aInNegateRegexp != aInTarget.matches ( lTmpRegexp );
        }
        else
        {
            lRet = aInRegexp.equals ( aInTarget );
        }

        return lRet;
    }

    private static String iGetMixerLoggingLine (Mixer.Info aInInfo )
    {
        return aInInfo.getDescription ( ) +
            ", " + aInInfo.getName ( ) +
            ", " + aInInfo.getVendor ( ) +
            ", " + aInInfo.getVersion ( ) +
            ", " + aInInfo.toString ( );
    }

    public static class MixerInfo {
        @Expose public final String hash;
        @Expose public final String description;

        public MixerInfo(String hash, String description) {
            this.hash = hash;
            this.description = description;
        }

        @Override
        public String toString() {
            return "MixerInfo{" +
                    "hash='" + hash + '\'' +
                    ", description='" + description + '\'' +
                    '}';
        }
    }

    public static String getMixerShowLine( final Mixer.Info aInInfo ) {
        final MixerInfo mixerInfo = getMixerInfo( aInInfo );
        return mixerInfo.hash + " -- " + mixerInfo.description;
    }

    public static MixerInfo getMixerInfo( final Mixer.Info aInInfo )
    {
        try
        {
            final String lMixerDescription = ( aInInfo == null ) ? "undetermined" : iGetMixerLoggingLine( aInInfo );

            final MessageDigest digestAlgorithm = MessageDigest.getInstance ( "SHA1" );
            digestAlgorithm.reset();
            final byte[] lNextAsBytes = lMixerDescription.getBytes( StandardCharsets.US_ASCII );
            digestAlgorithm.update( lNextAsBytes, 0, lNextAsBytes.length );
            final String lStringDigest = StringUtil.byteArrayToString ( digestAlgorithm.digest ( ), "", true );

            final String hash = String.format (
                    "%s:%s",
                    lStringDigest.substring ( 0, 4 ),
                    lStringDigest.substring ( 4, 8 ) );
            
            return new MixerInfo( hash, lMixerDescription );
        }

        catch ( Exception ex )
        {
            logger.error ( ex.getMessage (), ex );
            return new MixerInfo( "0000:0000", "" );
        }
    }
            
    public static DriversInfo fetchDriversInfoFromHardware(
        boolean fetchCompatibleSource,
        boolean fetchCompatibleSink,
        SoundBridgeConfig soundBridgeConfig )
    {
        final DriversInfo info = DriversInfo.create();

        {
            final List<Mixer.Info> allDrivers = AudioCommon.getMixers( false, false, null, false );
            info.setAllDrivers(allDrivers);
            info.setAllDriversDescription( getMixerInfos( allDrivers ) );
        }

        if ( fetchCompatibleSource ) {
            final List<Mixer.Info> sources = AudioCommon.getMixers(
                true,
                !soundBridgeConfig.getAudioSourceHash().isEmpty(),
                AudioSourceBuilder
                    .init()
                    .setAudioMixerDescriptionRegexp(soundBridgeConfig.getAudioSourceHash())
                    .setNumberOfChannels(1)
                    .setSamplingFrequencyEnum(soundBridgeConfig.getAudioSamplingFrequency())
                    .create(),
                soundBridgeConfig.isFastAudioDeviceTest()
            );

            info.setCompatibleSourceMixers(sources);
            info.setCompatibleSourceDescription( getMixerInfos( sources ) );
        }

        if ( fetchCompatibleSink ) {

            final List<Mixer.Info> sinks = AudioCommon.getMixers(
                true,
                !soundBridgeConfig.getAudioSinkHash().isEmpty(),
                AudioSinkBuilder
                    .init()
                    .setAudioMixerDescriptionRegexp(soundBridgeConfig.getAudioSinkHash())
                    .setNumberOfChannels(1)
                    .setSamplingFrequencyEnum(soundBridgeConfig.getAudioSamplingFrequency())
                    .create(),
                soundBridgeConfig.isFastAudioDeviceTest()
            );

            info.setCompatibleSinkMixers(sinks);
            info.setCompatibleSinkDescription( getMixerInfos( sinks ) );
        }

        info.setValid( true );

        return info;
    }

    public static List<String> getDescriptions( List<Mixer.Info> mixers )
    {
        return CollectionsUtil.transformList (
            mixers,
            mixer -> getMixerInfo( mixer ).description
        );
    }

    public static List<String> getHashes( List<Mixer.Info> mixers )
    {
        return CollectionsUtil.transformList (
            mixers,
            mixer -> getMixerInfo( mixer ).hash
        );
    }


    public static List<MixerInfo> getMixerInfos( List<Mixer.Info> mixers )
    {
        return CollectionsUtil.transformList (
            mixers,
            AudioCommon::getMixerInfo
        );
    }

    public static List<Mixer.Info> getMixers(
        final boolean filterOnCompatibility,
        final boolean filterOnRegexp,
        final IAudioBuilder builder,
        final boolean fastAudioDeviceTest )
    {
        List<Mixer.Info> lResult = CollectionsUtil.createList ( AudioSystem.getMixerInfo ( ) );

        logger.debug ( "includeCompatibleOnly: {}", filterOnCompatibility );
        logger.debug ( "inFilterOnRegexp: {}", filterOnRegexp );
        logger.debug ( "builder: {}", GsonUtil.serialize( builder ) );

        if ( filterOnRegexp )
        {
            logger.debug ( "aInBuilder.regexp: [{}]", builder::getAudioMixerDescriptionRegexp);

            lResult = filterRegexpCompatibleMixers(
                lResult,
                builder.getAudioMixerDescriptionRegexp ( ) );
        }
        else
        {
            Preconditions.checkArgument( builder == null || builder.getAudioMixerDescriptionRegexp().isEmpty());
        }

        if ( filterOnCompatibility )
        {
            lResult = iFilterLineCompatibleMixers (
                lResult,
                builder,
                fastAudioDeviceTest );
        }

        logger.debug( "result:{}", GsonUtil.serialize( getMixerInfos( lResult ) ) );
        logger.debug( "result:{}", getHashes( lResult ) );

        return lResult;
    }

    private static List<Mixer.Info> iFilterLineCompatibleMixers (
        List<Mixer.Info> aInMixerList,
        IAudioBuilder aInBuilder,
        boolean fastAudioDeviceTest )
    {
        // all these are indicative of bad options ** keep them all lowercase !!!
        final List<String> BLACK_LIST = ImmutableList.of (
            "bwa01a",
            "mixer",
            "bcm2835",
            "display",
            "default",
            "controlador",
            "controller" );

        return CollectionsUtil.filterList (
            aInMixerList,
            ( mixerUnderTest ) ->
            {
                final String desc = getMixerInfo( mixerUnderTest ).description;

                if ( isContainingAnyOf( desc.toLowerCase(), BLACK_LIST ) )
                {
                    return false;
                }

                final CreationResult lCr = attemptLineCreationFromMixerInfo(
                    aInBuilder,
                    mixerUnderTest,
                    fastAudioDeviceTest );

                if ( lCr.line != null )
                {
                    lCr.line.close();
                }

                return lCr.success;
            }
        );
    }

    private static boolean isContainingAnyOf( String container, List<String> particles )
    {
        for ( String next : particles )
        {
            if ( container.contains( next ) )
            {
                return true;
            }
        }

        return false;
    }

    public static class CreationResult
    {
        public final boolean success;
        public final String errorMsg;
        public final DataLine line;
        public final AudioFormat audioFormat;

        public CreationResult (
            boolean inSuccess,
            DataLine inLine,
            String inErrorMessage,
            AudioFormat inAudioFormat )
        {
            success = inSuccess;
            line = inLine;
            errorMsg = inErrorMessage;
            audioFormat = inAudioFormat;
        }
    }

    public static AudioFormat createAudioFormat ( IAudioBuilder aInBuilder ) {
        return new AudioFormat (
            AudioFormat.Encoding.PCM_SIGNED,
            aInBuilder.getSamplingFrequencyEnum ( ).getValue (),
            aInBuilder.getSampleSizeInBytes() * 8,
            aInBuilder.getNumberOfChannels ( ),
            aInBuilder.getSampleSizeInBytes() * aInBuilder.getNumberOfChannels ( ),
            aInBuilder.getSamplingFrequencyEnum ( ).getValue (),
            true );
    }

    public static CreationResult attemptLineCreationFromMixerInfo(
        IAudioBuilder aInBuilder,
        Mixer.Info aInMixerInfo,
        boolean fastAudioDeviceTest )
    {
        logger.debug ( "attemptLineCreationFromMixerInfo -- BEGIN");

        AudioFormat lAudioFormat = null;
        String errMsg = "";

        try
        {
            lAudioFormat = createAudioFormat( aInBuilder );

            final DataLine.Info lLineInfo;

            if ( aInBuilder.getDirection() == DirectionEnum.in )
            {
                lLineInfo = new DataLine.Info ( TargetDataLine.class, lAudioFormat );
            }
            else if ( aInBuilder.getDirection() == DirectionEnum.out )
            {
                lLineInfo = new DataLine.Info ( SourceDataLine.class, lAudioFormat );
            }
            else
            {
                throw new UnsupportedOperationException("wrong value for direction enum" );
            }

            if ( ! AudioSystem.isLineSupported ( lLineInfo ) )
            {
                return new CreationResult ( false, null, String.format( "format is not supported: [%s]", lAudioFormat ), lAudioFormat );
            }

            final Mixer lMixer = AudioSystem.getMixer ( aInMixerInfo );

            logger.debug( "about to invoke getLine() with aInMixerInfo: [{}]", () -> getMixerInfo( aInMixerInfo ).description );
            logger.debug( "about to invoke getLine() with lLineInfo: [{}]", lLineInfo );
            logger.debug( "existing as opened lines:{}", () -> Arrays.asList ( lMixer.getTargetLines () ) );

            final DataLine lLine = (DataLine) lMixer.getLine ( lLineInfo );

            if ( aInBuilder.getDirection() == DirectionEnum.in )
            {
                ((TargetDataLine)lLine).open(lAudioFormat, lLine.getBufferSize());
            }
            else if ( aInBuilder.getDirection() == DirectionEnum.out )
            {
                ((SourceDataLine)lLine).open(lAudioFormat, lLine.getBufferSize());
            }
            else
            {
                throw new UnsupportedOperationException( "unexpected value for direction enum" + aInBuilder.getDirection() );
            }

            lLine.start();

            final boolean lineReady;
            if ( fastAudioDeviceTest ) {
                lineReady = true;
            }
            else {
                // unfortunately, we need to wait enough time for the driver to start up
                SystemUtil.sleepMsec( 1_000 );
                lineReady = lLine.available ( ) > 0;
            }

            lLine.stop();
            lLine.flush();
            lLine.close();

            return new CreationResult ( lineReady, lLine, errMsg, lAudioFormat );
        }

        catch ( Exception ex )
        {
            logger.debug ( "exception received while initializing driver: {}", ex.getMessage () );
        }

        finally {
            logger.debug("attemptLineCreationFromMixerInfo -- END");
        }

        return new CreationResult ( false, null, "", lAudioFormat );
    }

    public static void setMaximumVolume( Line line )
    {
        if( line.isControlSupported(FloatControl.Type.VOLUME) ) {
            iSetMaximumForControl( (FloatControl) line.getControl(FloatControl.Type.VOLUME) );
        }
        else if( line.isControlSupported(FloatControl.Type.MASTER_GAIN) ) {
            iSetMaximumForControl( (FloatControl) line.getControl(FloatControl.Type.MASTER_GAIN) );
        }
        else {
            logger.debug( "this line does not support volume or master gain control" );
        }
    }

    private static void iSetMaximumForControl( FloatControl floatControl )
    {
        logger.debug("volume adjustment, before: {}", floatControl );
        floatControl.setValue( floatControl.getMaximum() );
        logger.debug("volume adjustment, after:  {}", floatControl );
    }

    public static int getBufferSizeInBytesModulusFrame(
        int bytesPerFrame,
        FSEnum samplingFrequencyEnum,
        int bufferLengthInMSec ) {

        // this should be a multiple of numberOfChannels*sampleSizeInBytes
        final int bufferSize = (int) (bytesPerFrame *
                samplingFrequencyEnum.getValue() *
                bufferLengthInMSec
                * 0.001);

        if ( bufferSize % bytesPerFrame == 0 )  {
            return bufferSize;
        }
        else {
            return ( ( bufferSize / bytesPerFrame ) + 1 ) * bytesPerFrame;
        }
    }
}
