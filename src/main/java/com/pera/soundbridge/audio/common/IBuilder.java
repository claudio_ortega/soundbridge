/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.common;

public interface IBuilder
{
    String getId();
    FSEnum getSamplingFrequencyEnum ( );
    int getNumberOfChannels ( );
    int getSampleSizeInBytes ( );
    int getMaximumEnqueuedBuffers();
    int getBufferLengthInMSec();
    int getConfiguredBufferSizeInBytes();
}
