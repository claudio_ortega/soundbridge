/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.source.sim;

import org.apache.logging.log4j.*;

public class SinusoidalCompute implements ICompute
{
    private final Logger logger = LogManager.getLogger();

    private final int periodInSamples;
    private final float[] signal;
    private final long modulationPeriod;
    private final long modulationInHigh;

    public static SinusoidalCompute create(SimulatedSourceBuilder builder)
    {
        return new SinusoidalCompute(builder);
    }

    private SinusoidalCompute(SimulatedSourceBuilder builder )
    {
        final double frequency = builder.getSignalFrequency();
        final double dc = builder.getDc ();
        final double amplitude = builder.getGain () * ( builder.getSampleSizeInBytes () == 2 ? 0x7fff : 0x7f );
        final double samplingFreqHz = builder.getSamplingFrequencyEnum ().getValue ();

        periodInSamples = (int) ( samplingFreqHz / frequency );
        signal = new float[periodInSamples];

        logger.debug( "{}", () -> String.format("requested freq: %.2f Hz", frequency));
        logger.debug( "{}", () -> String.format("generated freq: %.2f Hz", samplingFreqHz/periodInSamples));

        final double omega = 2.0 * Math.PI / ( (double) periodInSamples );
        for ( int i=0; i<periodInSamples; i++ )
        {
            signal[i] = (float) (dc + amplitude * Math.sin ( omega * i ));
        }

        if ( builder.getSquareModulationFrequency() < 1e-6 )
        {
            // no modulation
            modulationPeriod = -1;
            modulationInHigh = -1;
        }
        else
        {
            modulationPeriod = (long) (samplingFreqHz / builder.getSquareModulationFrequency());
            modulationInHigh = (long) ( modulationPeriod * builder.getSquareModulationDutyCycle() );
        }
    }

    @Override
    public float compute(
        int aInChannel,
        long aInSampleCount)
    {
        final float sine = signal[ (int) ( aInSampleCount % periodInSamples ) ];
        final float square;

        if ( modulationPeriod == -1 )
        {
            square = 1.0f;
        }
        else
        {
            square = ( (aInSampleCount % modulationPeriod) < modulationInHigh) ? 1.0f : 0 ;
        }

        return sine * square;
    }
}

