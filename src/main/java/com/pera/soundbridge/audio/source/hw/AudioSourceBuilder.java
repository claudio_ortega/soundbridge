/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.source.hw;

import com.google.gson.annotations.Expose;
import com.pera.soundbridge.audio.common.AudioCommon;
import com.pera.soundbridge.audio.common.DirectionEnum;
import com.pera.soundbridge.audio.common.IAudioBuilder;
import com.pera.soundbridge.audio.common.FSEnum;

public class AudioSourceBuilder implements IAudioBuilder {
    @Expose private final DirectionEnum direction;
    @Expose private final int sampleSizeInBytes;
    @Expose private FSEnum samplingFrequencyEnum;
    @Expose private int numberOfChannels;
    @Expose private String audioMixerDescriptionRegexp;
    @Expose private String id;
    @Expose private int maximumEnqueuedBuffers;
    @Expose private int bufferLengthInMSec;

    public AudioSourceBuilder create() {
        return this;
    }

    private AudioSourceBuilder() {
        id = "n/a";
        numberOfChannels = 1;
        sampleSizeInBytes = 2;
        samplingFrequencyEnum = FSEnum.value8000;
        audioMixerDescriptionRegexp = "";
        maximumEnqueuedBuffers = -1;
        bufferLengthInMSec = 20;
        direction = DirectionEnum.in;
    }

    public static AudioSourceBuilder init() {
        return new AudioSourceBuilder();
    }

    @Override
    public int getBufferLengthInMSec() {
        return bufferLengthInMSec;
    }

    public AudioSourceBuilder setBufferLengthInMSec(int value) {
        bufferLengthInMSec = value;
        return this;
    }

    @Override
    public int getMaximumEnqueuedBuffers() {
        return maximumEnqueuedBuffers;
    }

    public AudioSourceBuilder setMaximumEnqueuedBuffers(int value) {
        maximumEnqueuedBuffers = value;
        return this;
    }

    @Override
    public String getAudioMixerDescriptionRegexp() {
        return audioMixerDescriptionRegexp;
    }

    public AudioSourceBuilder setAudioMixerDescriptionRegexp(String aInAudioMixerDescriptionRegexp) {
        audioMixerDescriptionRegexp = aInAudioMixerDescriptionRegexp;
        return this;
    }

    @Override
    public FSEnum getSamplingFrequencyEnum() {
        return samplingFrequencyEnum;
    }

    public AudioSourceBuilder setSamplingFrequencyEnum(FSEnum aInSamplingFrequency) {
        samplingFrequencyEnum = aInSamplingFrequency;
        return this;
    }

    @Override
    public int getNumberOfChannels() {
        return numberOfChannels;
    }

    public AudioSourceBuilder setNumberOfChannels(int aInnChannels) {
        numberOfChannels = aInnChannels;
        return this;
    }

    @Override
    public int getSampleSizeInBytes() {
        return sampleSizeInBytes;
    }

    public AudioSourceBuilder setId(String value) {
        id = value;
        return this;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public DirectionEnum getDirection() {
        return direction;
    }

    @Override
    public int getConfiguredBufferSizeInBytes() {
        return AudioCommon.getBufferSizeInBytesModulusFrame(
            numberOfChannels * sampleSizeInBytes,
            samplingFrequencyEnum,
            bufferLengthInMSec );
    }
}