/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.source.hw;

import com.pera.soundbridge.audio.common.AudioCommon;
import com.pera.util.TaggedThreadFactory;
import com.pera.util.SystemUtil;
import com.pera.soundbridge.vessel.Vessel;
import com.pera.soundbridge.audio.source.ISource;
import org.apache.logging.log4j.*;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.TargetDataLine;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class AudioSource implements ISource
{
    private final static Logger logger = LogManager.getLogger();

    private final AtomicBoolean started;
    private final BlockingQueue<Vessel> queue;
    private final ThreadPoolExecutor executor;
    private final AtomicBoolean cancel;
    private final AtomicLong droppedSamplesCount;
    private final AtomicLong currentSamplesCount;
    private final AtomicLong serialNumber;
    private final Object lineGuard;
    private final AudioSourceBuilder builder;
    private Mixer.Info mixerInfo;
    private volatile TargetDataLine line;
    private final AtomicBoolean initPending;

    public static AudioSource create(AudioSourceBuilder aInSignalSourceBuilder )
    {
        return new AudioSource( aInSignalSourceBuilder );
    }

    private AudioSource( AudioSourceBuilder inBuilder )
    {
        builder = inBuilder;
        started = new AtomicBoolean( false );
        queue = new ArrayBlockingQueue<>(inBuilder.getMaximumEnqueuedBuffers());
        droppedSamplesCount = new AtomicLong( 0 );
        currentSamplesCount = new AtomicLong( 0 );
        serialNumber = new AtomicLong( 0 );
        lineGuard = new Object();
        cancel = new AtomicBoolean ( false );
        mixerInfo = null;
        line = null;
        executor = new ThreadPoolExecutor (
            1,
            1,
            0,
            TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<> ( ),
            TaggedThreadFactory.create( getClass ( ), inBuilder.getId() ) );
        initPending = new AtomicBoolean( false );
    }

    @Override
    public String getId()
    {
        return builder.getId();
    }

    @Override
    public void init ( )
    {
        synchronized ( lineGuard )
        {
            final boolean shouldInit = initPending.get() || line == null;

            if ( ! shouldInit )
            {
                logger.info( "skipping initialization, line:{}, needsReset:{}", line, initPending);
            }
            else
            {
                final List<Mixer.Info> lCompatibleMixers = AudioCommon.getMixers(
                    false,
                    true,
                    builder,
                    false );

                if (lCompatibleMixers.size() == 0) {
                    mixerInfo = null;
                    line = null;
                    throw new IllegalStateException("the application will not work under this configuration, as there is no compatible source mixers in the system");
                } else if (lCompatibleMixers.size() > 1 ) {
                    mixerInfo = null;
                    line = null;
                    lCompatibleMixers.forEach( x -> logger.error( "compatible mixers:{}", x ) );
                    throw new IllegalStateException("the application will not work under this configuration, there are more then one compatible source mixers matching");
                } else {

                    mixerInfo = lCompatibleMixers.get(0);

                    logger.info(
                        "the system will use this compatible source mixer: [{}]",
                        () -> AudioCommon.getMixerInfo( mixerInfo ).description );

                    final AudioCommon.CreationResult lCr = AudioCommon.attemptLineCreationFromMixerInfo(
                        builder,
                        mixerInfo,
                        true );

                    if (!lCr.success) {
                        line = null;
                        logger.error("the attempt to open the line was unsuccessful, error msg:{}", lCr.errorMsg);
                    } else {
                        logger.info("the line opening attempt was successful");
                        line = (TargetDataLine) lCr.line;
                    }
                }

                if ( line == null )
                {
                    initPending.set ( true );
                }

                else
                {
                    logger.debug("the attempt to open the line was successful, mixer info:{}",
                        () -> AudioCommon.getMixerInfo( mixerInfo ).description );

                    executor.submit(
                        () ->
                        {
                            try {

                                logger.debug("executor.run() -- BEGIN");

                                line.addLineListener(event -> logger.info("line event:{}", event));
                                AudioCommon.setMaximumVolume( line );

                                final int configuredBufferSizeInBytes = builder.getConfiguredBufferSizeInBytes();

                                logger.info(
                                    "expected delay in audio source: {} mSec",
                                    1000.0 * configuredBufferSizeInBytes /
                                        ( (float) builder.getSampleSizeInBytes() *
                                            builder.getNumberOfChannels() *
                                            builder.getSamplingFrequencyEnum().getValue() )
                                );

                                line.open( AudioCommon.createAudioFormat( builder ), configuredBufferSizeInBytes );
                                final byte[] buffer = new byte[configuredBufferSizeInBytes];

                                while ( ! cancel.get() ) {
                                    if ( !started.get() ) {
                                        logger.debug("source not yet started, waiting...");
                                        SystemUtil.sleepMsec( 200 );;
                                    } else {

                                        // it is ok if we block on line.read() in the case data is not available
                                        final int lNumberOfReadBytes = line.read(buffer, 0, buffer.length);

                                        if (lNumberOfReadBytes == 0) {
                                            logger.info("receiving no output when reading from audio line, it means that it was probably stopped");
                                            initPending.set(true);
                                            cancel.set(true);
                                        }

                                        else {

                                            final Vessel vessel = Vessel.createFromAudioBuffer(
                                                "",
                                                System.currentTimeMillis(),
                                                serialNumber.incrementAndGet(),
                                                builder.getNumberOfChannels(),
                                                builder.getSampleSizeInBytes(),
                                                buffer,
                                                0,
                                                buffer.length);

                                            if ( queue.size() > builder.getMaximumEnqueuedBuffers() / 10 ) {
                                                droppedSamplesCount.addAndGet( vessel.getNumberOfSamples() );
                                                logger.debug( "dropping vessel: {}", vessel.debugSingleLiner() );
                                            }

                                            else {
                                                queue.put(vessel);
                                                currentSamplesCount.addAndGet(vessel.getNumberOfSamples());
                                            }
                                        }
                                    }
                                }
                            }
                            catch ( Throwable ex  ) {
                                logger.error( ex.getMessage(), ex );
                            }

                            line.stop();
                            line.flush();
                            line.close();

                            queue.clear();

                            logger.debug("executor.run() -- END");
                        }
                    );

                    initPending.set( false );
                }
            }
        }
    }

    @Override
    public Vessel produce(long timeout, TimeUnit unit) throws Exception
    {
        final Vessel next = queue.poll(timeout, unit );
        return next == null ? Vessel.NULL : next;
    }

    @Override
    public void close() throws InterruptedException
    {
        cancel.set ( true );
        executor.shutdownNow ( );
    }

    @Override
    public void start ( )
    {
        synchronized ( lineGuard )
        {
            if ( line != null && ! line.isActive () && ! line.isRunning () )
            {
                started.set(true);
                line.start ( );
            }
        }
    }

    @Override
    public void stop ( )
    {
        synchronized ( lineGuard )
        {
            if ( line != null && line.isActive () && line.isRunning () )
            {
                line.stop();
                line.flush();
                started.set(false);
            }
        }
    }

    @Override
    public boolean waitForStop(long timeout, TimeUnit unit)
    {
        return true;
    }

    @Override
    public long getDroppedSourceSamplesCount()
    {
        return droppedSamplesCount.get();
    }

    @Override
    public long getProducedSourceSamplesCount()
    {
        return currentSamplesCount.get();
    }

    @Override
    public void getCounters ( Map<String, Number> x )
    {
        x.put( String.format( "%s.%s.queue.size", getClass().getSimpleName(), getId() ), (long) queue.size() );
        x.put( String.format( "%s.%s.droppedSamplesCount", getClass().getSimpleName(), getId() ), droppedSamplesCount.get() );
        x.put( String.format( "%s.%s.currentSamplesCount", getClass().getSimpleName(), getId() ), currentSamplesCount.get() );
        x.put( String.format( "%s.%s.serialNumber", getClass().getSimpleName(), getId() ), serialNumber.get() );
    }

    @Override
    public boolean isOperational()
    {
        return line != null && line.isActive();
    }
}
