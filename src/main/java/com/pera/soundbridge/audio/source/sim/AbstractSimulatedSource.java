/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.source.sim;

import com.google.common.base.Preconditions;
import com.pera.soundbridge.audio.source.ISource;
import com.pera.util.TaggedThreadFactory;
import com.pera.soundbridge.vessel.Vessel;
import org.apache.logging.log4j.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;


public abstract class AbstractSimulatedSource implements ISource
{
    private final Logger logger = LogManager.getLogger();

    private final SimulatedSourceBuilder builder;
    private final ScheduledThreadPoolExecutor executor;
    private final BlockingQueue<Vessel> queue;
    private final ICompute compute;
    private final AtomicBoolean started;
    private final AtomicLong startTimeInMSec;
    private final AtomicLong startSample;
    private final AtomicLong latestSampleInPreviousBatch;
    private final AtomicLong droppedSamplesCount;
    private final AtomicLong currentSamplesCount;
    private final AtomicLong batchCount;
    private final AtomicLong serialNumber;
    private final int queueResetLevel;
    private final List<Vessel> drainingList;

    AbstractSimulatedSource(
        SimulatedSourceBuilder inBuilder,
        ICompute aInCompute )
    {
        compute = aInCompute;
        builder = inBuilder;
        started = new AtomicBoolean( false );
        queue = new ArrayBlockingQueue<>(inBuilder.getMaximumEnqueuedBuffers());
        queueResetLevel = inBuilder.getMaximumEnqueuedBuffers()/10;
        drainingList = new LinkedList<>();
        startTimeInMSec = new AtomicLong( 0 );
        startSample = new AtomicLong( 0 );
        latestSampleInPreviousBatch = new AtomicLong( 0 );
        droppedSamplesCount = new AtomicLong( 0 );
        currentSamplesCount = new AtomicLong( 0 );
        batchCount = new AtomicLong( 0 );
        serialNumber = new AtomicLong( 0 );
        executor = new ScheduledThreadPoolExecutor (
            1,
            TaggedThreadFactory.create( getClass(), inBuilder.getId() ) );
    }

    @Override
    public void init ( )
    {
        Preconditions.checkState( builder.getBufferLengthInMSec() > 0 );
        executor.scheduleAtFixedRate (
            ( ) ->
            {
                try
                {
                    if ( started.get() )
                    {
                        if ( batchCount.get() == 0 )
                        {
                            startTimeInMSec.set( System.currentTimeMillis() );
                            startSample.set( 0 );
                            latestSampleInPreviousBatch.set(0);
                        }
                        else
                        {
                            final long lTimeSinceLastStartInMSec = System.currentTimeMillis() - startTimeInMSec.get();

                            // base 0
                            final long lSampleCountAtNow = (long) ( ( lTimeSinceLastStartInMSec / 1000.0 ) * builder.getSamplingFrequencyEnum ( ).getValue () );

                            // we should start producing samples from the last times's 'Now' to this 'Now'
                            final long lSamplesToProduceThisTime = lSampleCountAtNow - latestSampleInPreviousBatch.get();

                            Preconditions.checkState( lSamplesToProduceThisTime < Integer.MAX_VALUE );

                            if ( lSamplesToProduceThisTime > 0 )
                            {
                                final long nextVesselSerial = serialNumber.incrementAndGet();

                                final Vessel vessel;

                                final boolean shouldProduceThisVessel;

                                if ( builder.getVesselCountProductionLimit() >= 0 &&
                                     nextVesselSerial > builder.getVesselCountProductionLimit() )
                                {
                                    vessel = Vessel.NULL;
                                    shouldProduceThisVessel = true;
                                }
                                else
                                {
                                    vessel = Vessel.createZeroedBuffer(
                                        "",
                                        System.currentTimeMillis(),
                                        nextVesselSerial,
                                        builder.getNumberOfChannels(),
                                        builder.getSampleSizeInBytes(),
                                        (int) lSamplesToProduceThisTime);

                                    iComputeNextSamples(
                                        latestSampleInPreviousBatch.get() + 1,
                                        lSamplesToProduceThisTime,
                                        vessel);

                                    if ( builder.getSquelchChannel() == -1)
                                    {
                                        shouldProduceThisVessel = true;
                                    }
                                    else
                                    {
                                        shouldProduceThisVessel = vessel.getChannel(0).computePower() > builder.getSquelchPowerLevel();
                                    }
                                }

                                if ( shouldProduceThisVessel )
                                {
                                    // is the queue full()?
                                    if (queue.remainingCapacity() == 0)
                                    {
                                        // we have already blocked
                                        // on the consumption side, see comment above on line.read(),
                                        // so then do not block, but drop the data if the queue is full
                                        droppedSamplesCount.addAndGet(vessel.getNumberOfSamples());
                                    }
                                    else
                                    {
                                        queue.put(vessel);
                                        currentSamplesCount.addAndGet(vessel.getNumberOfSamples());
                                    }
                                }
                            }

                            latestSampleInPreviousBatch.set( lSampleCountAtNow );
                        }

                        batchCount.incrementAndGet ();
                    }
                }

                catch ( Throwable ex )
                {
                    logger.error( ex.getMessage(), ex );
                }
            },
            0,
            builder.getBufferLengthInMSec(),
            TimeUnit.MILLISECONDS
        );
    }

    private void iComputeNextSamples (
        final long aInCurrentSampleCount,
        final long aInSamplesToBeProduced,
        Vessel aInSignalVessel)
    {
        Preconditions.checkArgument( aInSamplesToBeProduced == aInSignalVessel.getNumberOfSamples() );

        for ( int lChannelIndex = 0; lChannelIndex <aInSignalVessel.getNumberOfChannels (); lChannelIndex++ )
        {
            for (int i = 0; i < aInSignalVessel.getNumberOfSamples(); i++ )
            {
                aInSignalVessel.setSample (
                    lChannelIndex,
                    i,
                    (int) compute.compute( lChannelIndex, aInCurrentSampleCount + i ) );
            }
        }
    }

    @Override
    public void start ()
    {
        batchCount.set(0);
        started.set(true);
    }

    @Override
    public void close()
    {
        queue.clear();
        executor.shutdownNow ();
    }

    @Override
    public long getDroppedSourceSamplesCount()
    {
        return droppedSamplesCount.get();
    }

    @Override
    public long getProducedSourceSamplesCount()
    {
        return currentSamplesCount.get();
    }

    @Override
    public Vessel produce(long timeout, TimeUnit unit) throws Exception
    {
        if ( queue.size() > queueResetLevel )
        {
            logger.info ( "clearing vessel queue, size:{}", () -> queue.size() );
            drainingList.clear();
            queue.drainTo( drainingList );
            drainingList.forEach( x -> droppedSamplesCount.addAndGet(x.getNumberOfSamples()) );
            return Vessel.NULL;
        }
        else
        {
            final Vessel next = queue.poll(timeout, unit );

            if ( next == null )
            {
                return Vessel.NULL;
            }
            else
            {
                return next;
            }
        }
    }

    @Override
    public void stop ()
    {
        started.set(false);
    }

    @Override
    public void getCounters ( Map<String, Number> m )
    {
        m.put( String.format( "%s.%s.started", getClass().getSimpleName(), getId() ), started.get() ? 1L : 0 );
        m.put( String.format( "%s.%s.queue", getClass().getSimpleName(), getId() ), (long) queue.size() );
        m.put( String.format( "%s.%s.droppedSamplesCount", getClass().getSimpleName(), getId() ), droppedSamplesCount.get() );
        m.put( String.format( "%s.%s.currentSamplesCount", getClass().getSimpleName(), getId() ), currentSamplesCount.get() );
        m.put( String.format( "%s.%s.serialNumber", getClass().getSimpleName(), getId() ), serialNumber.get() );
    }
}
