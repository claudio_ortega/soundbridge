/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.source.sim;

import com.pera.soundbridge.audio.source.ISource;

import java.util.concurrent.*;

public class SimulatedSource extends AbstractSimulatedSource implements ISource
{
    private final SimulatedSourceBuilder builder;

    public static SimulatedSource create(SimulatedSourceBuilder aInBuilder )
    {
        return new SimulatedSource( aInBuilder );
    }

    private SimulatedSource(SimulatedSourceBuilder aInBuilder )
    {
        super( aInBuilder, SinusoidalCompute.create ( aInBuilder ) );
        builder = aInBuilder;
    }

    @Override
    public void init ()
    {
        super.init();
    }

    @Override
    public boolean waitForStop(long timeout, TimeUnit unit)
    {
        return true;
    }

    @Override
    public String getId()
    {
        return builder.getId();
    }

    @Override
    public boolean isOperational()
    {
        return true;
    }
}