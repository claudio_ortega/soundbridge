/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.source;

import com.pera.soundbridge.audio.IInitiable;
import com.pera.soundbridge.audio.IIdentifiable;
import com.pera.soundbridge.audio.IObservable;
import com.pera.soundbridge.audio.IStartable;
import com.pera.soundbridge.vessel.Vessel;

import java.util.concurrent.TimeUnit;

public interface ISource extends IInitiable, IIdentifiable, IObservable, IStartable
{
    long getProducedSourceSamplesCount();
    long getDroppedSourceSamplesCount( );
    Vessel produce(long timeout, TimeUnit unit) throws Exception;
}
