/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.source.sim;

import com.pera.soundbridge.audio.common.FSEnum;
import com.pera.soundbridge.audio.common.IBuilder;

public class SimulatedSourceBuilder implements IBuilder
{
    private FSEnum samplingFrequencyEnum;
    private int nChannels;
    private double signalFrequency;
    private double gain;
    private double dc;
    private int sampleSizeInBytes;
    private double squareModulationFrequency;
    private double squareModulationDutyCycle;
    private String id;
    private int maximumEnqueuedBuffers;
    private int bufferLengthInMSec;
    private long squelchPowerLevel;
    private final long squelchChannel;
    private final long vesselCountProductionLimit;

    public SimulatedSourceBuilder create ()
    {
        return this;
    }

    private SimulatedSourceBuilder()
    {
        id = "n/a";
        nChannels = 0;
        samplingFrequencyEnum = FSEnum.value8000;
        signalFrequency = 1000.0;
        gain = 1.0;
        dc = 0.0;
        bufferLengthInMSec = -1;
        squareModulationFrequency = .5;
        maximumEnqueuedBuffers = -1;
        squelchPowerLevel = -1;
        squelchChannel = -1;
        squareModulationDutyCycle = 0.5;
        vesselCountProductionLimit = -1;
    }

    public static SimulatedSourceBuilder init()
    {
        return new SimulatedSourceBuilder();
    }

    public long getVesselCountProductionLimit() {
        return vesselCountProductionLimit;
    }

    public double getSquareModulationDutyCycle() {
        return squareModulationDutyCycle;
    }

    public SimulatedSourceBuilder setSquareModulationDutyCycle(double value) {
        squareModulationDutyCycle = value;
        return this;
    }

    public long getSquelchPowerLevel() {
        return squelchPowerLevel;
    }

    public SimulatedSourceBuilder setSquelchPowerLevel(long value) {
        squelchPowerLevel = value;
        return this;
    }

    public long getSquelchChannel() {
        return squelchChannel;
    }

    @Override
    public int getMaximumEnqueuedBuffers() {
        return maximumEnqueuedBuffers;
    }

    public SimulatedSourceBuilder setMaximumEnqueuedBuffers(int value) {
        maximumEnqueuedBuffers = value;
        return this;
    }

    public int getBufferLengthInMSec()
    {
        return bufferLengthInMSec;
    }

    public SimulatedSourceBuilder setBufferLengthInMSec(int value)
    {
        bufferLengthInMSec = value;
        return this;
    }

    @Override
    public int getSampleSizeInBytes ( )
    {
        return sampleSizeInBytes;
    }

    @Override
    public FSEnum getSamplingFrequencyEnum ( )
    {
        return samplingFrequencyEnum;
    }

    public SimulatedSourceBuilder setSamplingFrequencyEnum (FSEnum value )
    {
        samplingFrequencyEnum = value;
        return this;
    }

    @Override
    public int getNumberOfChannels ( )
    {
        return nChannels;
    }

    public SimulatedSourceBuilder setNumberOfChannels (int value)
    {
        nChannels = value;
        return this;
    }

    public double getSignalFrequency( )
    {
        return signalFrequency;
    }

    public SimulatedSourceBuilder setSignalFrequency(double value )
    {
        signalFrequency = value;
        return this;
    }

    public double getGain ( )
    {
        return gain;
    }

    public SimulatedSourceBuilder setGain (double value )
    {
        gain = value;
        return this;
    }

    public double getDc ( )
    {
        return dc;
    }

    public SimulatedSourceBuilder setDc (double value )
    {
        dc = value;
        return this;
    }

    public SimulatedSourceBuilder setSampleSizeInBytes(int value)
    {
        sampleSizeInBytes = value;
        return this;
    }

    public double getSquareModulationFrequency()
    {
        return squareModulationFrequency;
    }

    public SimulatedSourceBuilder setSquareModulationFrequency(double value)
    {
        squareModulationFrequency = value;
        return this;
    }

    public SimulatedSourceBuilder setId(String value)
    {
        id = value;
        return this;
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public int getConfiguredBufferSizeInBytes()
    {
        throw new UnsupportedOperationException();
    }
}
