/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.source.sim;

public interface ICompute
{
    float compute ( int aInChannel, long aInSampleCount );
}

