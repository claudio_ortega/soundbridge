/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.audio.source.sim;

import com.pera.soundbridge.audio.source.*;
import com.pera.soundbridge.vessel.*;
import java.util.*;
import java.util.concurrent.*;

public class NullSource implements ISource
{
    public static NullSource create()
    {
        return new NullSource();
    }

    private NullSource()
    {
    }

    @Override
    public void init ()
    {
    }

    @Override
    public long getProducedSourceSamplesCount() {
        return 0;
    }

    @Override
    public long getDroppedSourceSamplesCount() {
        return 0;
    }

    @Override
    public Vessel produce(long timeout, TimeUnit unit) {
        return Vessel.NULL;
    }

    @Override
    public void close() {

    }

    @Override
    public void getCounters(Map<String, Number> m) {

    }

    @Override
    public void start()  {
    }

    @Override
    public void stop() {

    }

    @Override
    public boolean waitForStop(long timeout, TimeUnit unit) {
        return true;
    }

    @Override
    public String getId() {
        return "null source";
    }

    @Override
    public boolean isOperational()
    {
        return true;
    }
}