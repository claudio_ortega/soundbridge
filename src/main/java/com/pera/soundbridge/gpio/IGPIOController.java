package com.pera.soundbridge.gpio;

public interface IGPIOController extends AutoCloseable {

    enum PinOpResult {noop, ok, fail}

    class PinConfig {
        enum Direction {in, out}
        int bcmIndex;
        Direction direction;

        PinConfig(int iBcmIndex, Direction iDirection)
        {
            bcmIndex = iBcmIndex;
            direction = iDirection;
        }
    }

    IGPIOController init(boolean inInvertTTLLevels);
    PinOpResult setPinState(int bcmIndex, boolean newState);
    PinOpResult changePinState(int bcmIndex, boolean newState);
    boolean validateBcmIndex(int bcmIndex);
    void close();
}
