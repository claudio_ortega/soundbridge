/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.gpio;

import com.google.gson.annotations.Expose;

public class GPIOPinCommandInfo
{
    @Expose private final int bcmIndex;
    @Expose private final boolean commandedState;

    public static GPIOPinCommandInfo create()
    {
        return new GPIOPinCommandInfo();
    }

    private GPIOPinCommandInfo()
    {
        bcmIndex = -1;
        commandedState = false;
    }

    public int getBcmIndex() {
        return bcmIndex;
    }

    public boolean isCommandedState() {
        return commandedState;
    }
}
