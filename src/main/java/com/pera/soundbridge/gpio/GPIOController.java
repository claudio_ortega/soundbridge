/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.gpio;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.apache.logging.log4j.*;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.*;

public class GPIOController implements IGPIOController {

    private static final Logger logger = LogManager.getLogger();

    private Map<Integer,PinConfig> pinConfig;
    private Map<Integer,Boolean> pinState;
    private boolean enableGPIO;
    private boolean invertTTLLevels;

    private GPIOController()
    {
    }

    public static GPIOController create()
    {
        return new GPIOController();
    }

    @Override
    public IGPIOController init( boolean inInvertTTLLevels )
    {
        invertTTLLevels = inInvertTTLLevels;

        final int ret = iExec( "gpio -v" );

        if ( ret == 0 ) {
            enableGPIO = true;
        }
        else {
            enableGPIO = false;
            logger.warn( "unable to find wiringPI functionality, will log gpio commands instead" );
        }

        // these numbers are in BCM notation, see https://pinout.xyz/pinout/wiringpi
        // ie: pin 17 in bcm base corresponds to position pin 11 in the physical connector
        // bcm go from [0-27]
        final Map<Integer,PinConfig> tmpMap = new HashMap<>();
        for ( int bcmIndex=0; bcmIndex<28; bcmIndex++) {
            tmpMap.put( bcmIndex, new PinConfig( bcmIndex, PinConfig.Direction.out) );
        }

        pinConfig = ImmutableMap.copyOf( tmpMap );
        pinState = new ConcurrentHashMap<>(pinConfig.size());

        iProgramPinDirections();
        iSetAllPins( false );

        return this;
    }

    @Override
    public void close()
    {
        iSetAllPins( false );
    }

    @Override
    public PinOpResult setPinState(final int bcmIndex, final boolean newState)
    {
        pinState.put( bcmIndex, newState );

        return iSetPin( bcmIndex, newState );
    }

    @Override
    public PinOpResult changePinState(final int bcmIndex, final boolean newState)
    {
        if ( ! pinState.containsKey( bcmIndex ) )
        {
            return PinOpResult.fail;
        }

        if ( pinState.get( bcmIndex ).equals( newState ) ) {
            return PinOpResult.noop;
        }

        pinState.put( bcmIndex, newState );

        return iSetPin( bcmIndex, newState );
    }

    /**
     *
     * @param bcmIndex   bcmIndex index for the pin, see https://pinout.xyz/pinout/wiringpi
     *                   for actual pin placement inside the GPIO connector
     * @param setTTLToHigh
     *                   true:   turn the pin level into high ttl
     *                   false:  low ttl
     *
     *                   uses command
     *                       gpio -g write bcm-index 1/0
     *
     * @return
     */
    private PinOpResult iSetPin ( final int bcmIndex, final boolean setTTLToHigh )
    {
        if ( ! pinConfig.containsKey( bcmIndex ) )
        {
            return PinOpResult.fail;
        }

        final PinConfig pin = pinConfig.get( bcmIndex );

        final PinOpResult result;

        // wiringPI command:
        //   gpio -g write bcm-index [0-1]
        final String command = String.format(
            "gpio -g write %d %d",
            pin.bcmIndex,
            invertTTLLevels ^ setTTLToHigh ? 1 : 0
        );

        if ( enableGPIO ) {
            result = iExec(command) == 0 ? PinOpResult.ok : PinOpResult.fail;
        } else {
            logger.debug( "gpio is disabled, bypassing execution of gpio command: {}", command );
            result = PinOpResult.ok;
        }

        return result;
    }

    private void iSetAllPins(boolean setToTTLHigh )
    {
        pinConfig.forEach(
            (bcmIndex, pin) -> {
                Preconditions.checkState( bcmIndex == pin.bcmIndex );
                setPinState( bcmIndex, setToTTLHigh );
            } );
    }

    @Override
    public boolean validateBcmIndex(final int bcmIndex)
    {
        return pinConfig.containsKey( bcmIndex );
    }

    // wiringPI command:
    // gpio mode bcm-index out
    private void iProgramPinDirections()
    {
        pinConfig.forEach(
            (bcmIndex, pin) -> {

                Preconditions.checkState( bcmIndex == pin.bcmIndex );

                // for the bcm-index for each pin, see https://pinout.xyz/pinout/wiringpi
                // uses command
                //    gpio -g mode bcm-index in/out
                final String command = String.format(
                    "gpio -g mode %d %s",
                    pin.bcmIndex,
                    pin.direction == PinConfig.Direction.out ? "out" : "in");

                if ( enableGPIO ) {
                    iExec(command);
                } else {
                    logger.debug( "bypassing command: {}", command );
                }
            }
        );
    }

    private static int iExec ( String execCmd )
    {
        int ret;
        try {
            logger.debug( "attempting to execute command: {}", execCmd);

            final Process process = Runtime.getRuntime().exec(execCmd);

            ret = process.waitFor();
            logger.debug( "process returned {}", ret );

            process.destroy();
        }
        catch ( Exception e ) {
            logger.warn( e.getMessage() );
            ret = -1;
        }
        return ret;
    }
}
