/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.gpio;

public class NullGPIOController implements IGPIOController {

    private NullGPIOController()
    {
    }

    public static NullGPIOController create()
    {
        return new NullGPIOController();
    }

    @Override
    public IGPIOController init(boolean inInvertTTLLevels)
    {
        return this;
    }

    @Override
    public void close()
    {
    }

    @Override
    public PinOpResult setPinState(final int bcmIndex, final boolean newState)
    {
        return PinOpResult.fail;
    }

    @Override
    public PinOpResult changePinState(final int bcmIndex, final boolean newState)
    {
        return PinOpResult.fail;
    }

    @Override
    public boolean validateBcmIndex(final int bcmIndex)
    {
        return false;
    }
}
