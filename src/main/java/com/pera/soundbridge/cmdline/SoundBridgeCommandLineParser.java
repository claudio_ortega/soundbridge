/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.cmdline;

import com.pera.util.CommandLineParserUtil;
import com.pera.util.*;
import joptsimple.*;

import java.io.*;

public class SoundBridgeCommandLineParser
{
    private final OptionParser parser;

    public static SoundBridgeCommandLineParser create()
    {
        return new SoundBridgeCommandLineParser();
    }

    private SoundBridgeCommandLineParser()
    {
        parser = new OptionParser();
    }

    public SoundBridgeStartupConfigInfo getConfigFromArgs(
        String[] args,
        SoundBridgeStartupConfigInfo defaultValues )
    {
        final OptionSpec<Void> help = parser
            .accepts( "help", "prints this help and exits" )
            .forHelp();

        final OptionSpec<Void> version = parser
            .accepts( "version", "prints the app version and exits" );

        final OptionSpec<Boolean> useRest = parser
            .accepts( "use-rest", "starts a restful API server for configuration and monitoring" )
            .withRequiredArg()
            .ofType( Boolean.class )
            .defaultsTo( defaultValues.isUseRest() );

        final OptionSpec<Void> checkSiblings = parser
            .accepts( "check-for-siblings", "lists instances of this app and exits" );

        final OptionSpec<Void> listAllAudioDrivers = parser
            .accepts( "list-all-audio", "lists all the audio drivers, either compatible or not with this app" );

        final OptionSpec<Void> listCompatibleAudioDrivers = parser
            .accepts( "list-compat-audio", "lists only the audio drivers compatible with this app" );

        final OptionSpec<String> restHostNameOrIPAddress = parser
            .accepts( "rest-host", "sets the name/IP address for the restful API server, " +
                      "if empty we will use the first available v4 address" )
            .withRequiredArg()
            .ofType( String.class )
            .defaultsTo( defaultValues.getRestConfigHostNameOrIPAddress() );

        final OptionSpec<Integer> restPort = parser
            .accepts( "rest-port", "sets the port number used by the restful API server" )
            .withRequiredArg()
            .ofType( Integer.class )
            .defaultsTo( defaultValues.getRestConfigPort() );

        final OptionSpec<String> configPath = parser
            .accepts( "config-file", "sets the path for the startup configuration file" )
            .withRequiredArg()
            .ofType( String.class )
            .defaultsTo( defaultValues.getConfigPath() );

        final OptionSpec<String> logLevel = parser
            .accepts( "log-level", "sets the log level, it should be any of these: {error, warn, info, debug, trace}" )
            .withRequiredArg()
            .ofType( String.class )
            .defaultsTo( defaultValues.getLogLevel() );

        final OptionSet options = parser.parse( args );

        final SoundBridgeStartupConfigInfo startupConfig = SoundBridgeStartupConfigInfo.create();

        startupConfig.setHelpRequested( options.has( help ) );
        startupConfig.setVersionRequested( options.has( version ) );
        startupConfig.setUseRest( options.valueOf ( useRest ) );
        startupConfig.setCheckSiblings( options.has( checkSiblings ) );
        startupConfig.setListCompatibleAudioDrivers( options.has( listCompatibleAudioDrivers ) );
        startupConfig.setListAllAudioDrivers( options.has( listAllAudioDrivers ) );
        startupConfig.setRestConfigHostNameOrIPAddress( options.valueOf( restHostNameOrIPAddress ) );
        startupConfig.setRestConfigPort( options.valueOf( restPort ) );
        startupConfig.setLogLevel( options.valueOf( logLevel ) );
        startupConfig.setConfigPath( options.valueOf( configPath ) );

        return startupConfig;
    }

    public void printHelp ( PrintStream ps ) throws IOException
    {
        ps.println();
        ps.println("SoundBridge - carries sound across the internet." );
        ps.println();
        ps.println("git version: " + SystemUtil.getGitVersion( true ) );
        ps.println();
        ps.println("Usage: ");
        ps.println();
        parser.formatHelpWith( new CommandLineParserUtil.HelpFormatter() );
        parser.printHelpOn( ps );
        ps.println();
        ps.println( "All command line options should start either with a single or double hyphen," );
        ps.println( "followed by the minimum number of letters that singles out an option." );
    }
}

