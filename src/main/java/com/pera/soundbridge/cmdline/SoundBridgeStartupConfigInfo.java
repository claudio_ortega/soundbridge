/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.cmdline;

public class SoundBridgeStartupConfigInfo
{
    private int restConfigPort;
    private String restConfigHostNameOrIPAddress;
    private boolean helpRequested;
    private boolean versionRequested;
    private boolean useRest;
    private boolean listAllAudioDrivers;
    private boolean listCompatibleAudioDrivers;
    private String configPath;
    private String logLevel;
    private boolean checkSiblings;

    public static SoundBridgeStartupConfigInfo create()
    {
        return new SoundBridgeStartupConfigInfo();
    }

    public static final SoundBridgeStartupConfigInfo DEFAULT = create();

    private SoundBridgeStartupConfigInfo()
    {
        helpRequested = false;
        versionRequested = false;
        useRest = true;
        restConfigPort = 9090;
        restConfigHostNameOrIPAddress = "";
        listAllAudioDrivers = false;
        listCompatibleAudioDrivers = false;
        configPath = "";
        logLevel = "info";
        checkSiblings = false;
    }

    public boolean isVersionRequested() {
        return versionRequested;
    }

    public SoundBridgeStartupConfigInfo setVersionRequested(boolean value) {
        versionRequested = value;
        return this;
    }

    public boolean isCheckSiblings() {
        return checkSiblings;
    }

    public SoundBridgeStartupConfigInfo setCheckSiblings(boolean value) {
        checkSiblings = value;
        return this;
    }

    public boolean isUseRest() {
        return useRest;
    }

    public SoundBridgeStartupConfigInfo setUseRest(boolean value) {
        useRest = value;
        return this;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public SoundBridgeStartupConfigInfo setLogLevel(String value) {
        logLevel = value;
        return this;
    }

    public String getConfigPath()
    {
        return configPath;
    }

    public SoundBridgeStartupConfigInfo setConfigPath(String value)
    {
        configPath = value;
        return this;
    }

    public boolean isListAllAudioDrivers()
    {
        return listAllAudioDrivers;
    }

    public SoundBridgeStartupConfigInfo setListAllAudioDrivers(boolean value)
    {
        listAllAudioDrivers = value;
        return this;
    }

    public boolean isListCompatibleAudioDrivers()
    {
        return listCompatibleAudioDrivers;
    }

    public SoundBridgeStartupConfigInfo setListCompatibleAudioDrivers(boolean value)
    {
        listCompatibleAudioDrivers = value;
        return this;
    }

    public SoundBridgeStartupConfigInfo setRestConfigPort(int value)
    {
        restConfigPort = value;
        return this;
    }

    public SoundBridgeStartupConfigInfo setHelpRequested(boolean value)
    {
        helpRequested = value;
        return this;
    }

    public SoundBridgeStartupConfigInfo setRestConfigHostNameOrIPAddress(String value)
    {
        restConfigHostNameOrIPAddress = value;
        return this;
    }

    public int getRestConfigPort()
    {
        return restConfigPort;
    }

    public boolean getHelpRequested()
    {
        return helpRequested;
    }

    public String getRestConfigHostNameOrIPAddress()
    {
        return restConfigHostNameOrIPAddress;
    }
}

