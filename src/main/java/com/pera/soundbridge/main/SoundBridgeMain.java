/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.main;

import com.pera.soundbridge.SoundBridge;
import com.pera.util.LogUtil;

public class SoundBridgeMain
{
    // used from shells
    public static void main( String[] args )
    {
        LogUtil.initLog4j();

        System.exit(
            SoundBridge.create().start(
                false,
                true,
                true,
                true,
                true,
                false,
                args )
        );
    }
}
