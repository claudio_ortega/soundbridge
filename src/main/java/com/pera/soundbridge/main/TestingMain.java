/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.main;

import com.pera.soundbridge.SoundBridge;

public class TestingMain
{
    // used only from tests
    public static int main( String ... args )
    {
        return
            SoundBridge.create().start(
                true,
                true,
                true,
                true,
                true,
                false,
                args );
    }
}
