/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge;

import com.pera.soundbridge.audio.common.DriversInfo;
import com.pera.soundbridge.audio.common.FSEnum;
import com.pera.soundbridge.restinfo.SoundBridgeConfig;
import com.pera.soundbridge.station.DelayRecorder;
import java.util.List;

public interface ISoundBridge 
{
    SoundBridgeConfig getSoundBridgeConfig();
    DriversInfo getDriversInfo();
    List<FSEnum> getValidFS();
    int start (
        boolean executingFromTestCase,
        boolean loadConfigOnStartup,
        boolean addShutDownHook,
        boolean startStationOnStartup,
        boolean displayEnvironmentAtStartup,
        boolean returnImmediately,
        String ... args  );
    void startStation() throws Exception;
    void stopStation() throws Exception;
    void stop() throws Exception;
    DelayRecorder.Result computeDelayResults();
}
