/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.vessel;

import com.google.common.base.*;

public enum VesselType {

    Null( (byte)0 ),
    Probe( (byte)1 ),
    Signal( (byte)2 );

    byte value;

    VesselType(byte v )
    {
        value = v;
    }

    static VesselType fromByte(byte b ) {
        if (b == 0) {
            return Null;
        }
        if (b == 1) {
            return Probe;
        }
        if (b == 2) {
            return Signal;
        }
        Preconditions.checkState(false);
        return null;
    }
}
