/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.vessel;

import com.google.common.base.Preconditions;
import com.pera.soundbridge.audio.util.AudioLineUtils;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Vessel
{
    private final VesselType type;
    private final String originatorId;
    private final long serialNumber;
    private final long timestamp;
    private final int numberOfChannels;
    private final int bytesPerSample;
    private final int numberOfSamples;
    private final SingleChannelVessel[] channelVessels;

    public static final Vessel NULL = new Vessel(VesselType.Null, "", 0,0,0,0,0);

    public static Vessel createProbe( String originatorId, long serialnumber )
    {
        return new Vessel(
            VesselType.Probe,
            originatorId,
            0, // probes do not have a timestamp, they only have serialNumber
            serialnumber,
            1,
            2,
            0 );
    }

    public static Vessel createZeroedBuffer(
        String originatorId,
        long timestamp,
        long serialnumber,
        int aInChannels,
        int bytesPerSample,
        int aInSize )
    {
        return new Vessel(
            VesselType.Signal,
            originatorId,
            timestamp,
            serialnumber,
            aInChannels,
            bytesPerSample,
            aInSize );
    }

    public static Vessel createFromAudioBuffer(
        String originatorId,
        long timestamp,
        long serialnumber,
        int nChannels,
        int aInBytesPerSample,
        byte[] samplesBuffer,
        int beginOfBufferOffset,
        int bufferLenInBytes )
    {
        return iCreateFromSamples(
            VesselType.Signal,
            originatorId,
            timestamp,
            serialnumber,
            nChannels,
            aInBytesPerSample,
            samplesBuffer,
            beginOfBufferOffset,
            bufferLenInBytes );
    }

    private static Vessel iCreateFromSamples(
        VesselType inVesselType,
        String originatorId,
        long timestamp,
        long serialnumber,
        int nChannels,
        int aInBytesPerSample,
        byte[] samplesBuffer,
        int beginOfBufferOffset,
        int bufferLenInBytes )
    {
        Preconditions.checkArgument ( ( bufferLenInBytes % ( nChannels * aInBytesPerSample ) ) == 0 );

        final int nSamples = bufferLenInBytes / ( nChannels * aInBytesPerSample );

        final Vessel vessel = new Vessel(
            inVesselType,
            originatorId,
            timestamp,
            serialnumber,
            nChannels,
            aInBytesPerSample,
            nSamples );

        vessel.fillSamplesIntoBuffer(
            nChannels,
            aInBytesPerSample,
            nSamples,
            beginOfBufferOffset,
            samplesBuffer);

        return vessel;
    }

    public boolean isNull()
    {
        return type == VesselType.Null;
    }

    public boolean isProbe()
    {
        return type == VesselType.Probe;
    }

    public VesselType getType()
    {
        return type;
    }

    public String getOriginatorId() {
        return originatorId;
    }

    /**
     *
     * @param buffer: contains the whole vessel, as produced by produceUDPBuffer()
     * @param length buffer size in bytes
     * @return null if error, otherwise a vessel same as the one that would have produced
     *         this buffer with method produceUDPBuffer()
     */
    public static Vessel createFromUDPBuffer(
        byte[] buffer,
        int offset,
        int length )
    {
        final int checksumA = iComputeChecksum ( buffer, offset+PACKET_SIZE_INDEX, length - PACKET_SIZE_INDEX);
        final int checksumB = AudioLineUtils.getIntFromBytes( buffer[offset+CHECKSUM_INDEX], buffer[offset+CHECKSUM_INDEX+1] );

        if ( checksumA != checksumB )
        {
            return Vessel.NULL;
        }

        final int contentLength = AudioLineUtils.getIntFromBytes( buffer[offset+PACKET_SIZE_INDEX], buffer[offset+PACKET_SIZE_INDEX+1] );

        if ( length != contentLength )
        {
            return Vessel.NULL;
        }

        final byte typeAsByte = buffer[offset+TYPE_INDEX];
        final byte[] originatorIdAsBytes = new byte[8];
        System.arraycopy( buffer, offset+ORIGINATOR_INDEX, originatorIdAsBytes, 0, originatorIdAsBytes.length );
        final String originatorId = new String( originatorIdAsBytes, StandardCharsets.US_ASCII );

        final long serialnumber = AudioLineUtils.getLongFromBytes(buffer, offset+SERIAL_INDEX, 8);
        final long timestamp =  AudioLineUtils.getLongFromBytes(buffer, offset+TIMESTAMP_INDEX, 8);
        final int nChannels = buffer[offset+CHANNELS_INDEX];
        final int aInBytesPerSample = buffer[offset+BYTES_PER_SAMPLE_INDEX];
        final int nSamples = AudioLineUtils.getIntFromBytes( buffer[offset+NUMBER_SAMPLES_INDEX], buffer[offset+NUMBER_SAMPLES_INDEX+1] );

        final int samplesSizeInBytes = nChannels * aInBytesPerSample * nSamples;

        return iCreateFromSamples(
            VesselType.fromByte( typeAsByte ),
            originatorId,
            timestamp,
            serialnumber,
            nChannels,
            aInBytesPerSample,
            buffer,
            offset+FIRST_SAMPLE_INDEX,
            samplesSizeInBytes );
    }

    private void fillSamplesIntoBuffer(
        int nChannels,
        int aInBytesPerSample,
        int nSamples,
        int beginOfBufferOffset,
        byte[] buffer )
    {
        for ( int iChannelIndex = 0; iChannelIndex < nChannels; iChannelIndex++)
        {
            final int channelOffset = iChannelIndex * aInBytesPerSample;

            for ( int iSampleIndex = 0; iSampleIndex < nSamples; iSampleIndex++ )
            {
                final int lFistByteInFrameIndex =
                    beginOfBufferOffset + iSampleIndex * aInBytesPerSample * nChannels;

                channelVessels[iChannelIndex].values[iSampleIndex] =
                    AudioLineUtils.getIntFromBytes (
                        buffer[lFistByteInFrameIndex + channelOffset],        // big indian
                        buffer[lFistByteInFrameIndex + channelOffset + 1] );
            }
        }
    }

    private Vessel(
        VesselType inVesselType,
        String inOriginatorId,
        long aInTimestamp,
        long aInSerialnumber,
        int aInChannels,
        int aInBytesPerSample,
        int aInSize )
    {
        if ( inOriginatorId.length() == 0 ) {
            originatorId = EMPTY_ORIGINATOR;
        }
        else if ( inOriginatorId.length() == 8 ) {
            originatorId = inOriginatorId;
        }
        else {
            throw new RuntimeException( String.format( "wrong originatorId's length:%d", inOriginatorId.length() ) );
        }

        type = inVesselType;
        timestamp = aInTimestamp;
        serialNumber = aInSerialnumber;
        numberOfChannels = aInChannels;
        numberOfSamples = aInSize;
        bytesPerSample = aInBytesPerSample;
        channelVessels = new SingleChannelVessel[aInChannels];

        for ( int i=0; i<aInChannels; i++ )
        {
            channelVessels[i] = new SingleChannelVessel ( aInSize );
        }
    }

    public byte[] produceAudioBuffer()
    {
        Preconditions.checkState( ! isNull() );

        final int bufferSize = numberOfSamples * numberOfChannels * bytesPerSample;

        final byte[] buffer = new byte[bufferSize];

        produceSamplesBuffer ( buffer, 0 );

        return buffer;
    }

    private void produceSamplesBuffer( byte[] buffer, int offset )
    {
        for ( int channelIndex = 0; channelIndex < numberOfChannels; channelIndex++)
        {
            final int channelOffset = channelIndex * bytesPerSample;
            for (int sampleIndex = 0; sampleIndex < numberOfSamples; sampleIndex++ )
            {
                final int sample = channelVessels[channelIndex].values[sampleIndex];

                final int bufferOffset =
                    offset +
                    sampleIndex * numberOfChannels * bytesPerSample +
                    channelOffset;

                final byte msb = AudioLineUtils.getMsbFromInt ( sample );
                final byte lsb = AudioLineUtils.getLsbFromInt ( sample );
                buffer[bufferOffset] = msb;               // big indian
                buffer[bufferOffset + 1] = lsb;
            }
        }
    }

    private static final int CHECKSUM_INDEX = 0;
    private static final int PACKET_SIZE_INDEX = CHECKSUM_INDEX + 2;
    private static final int TYPE_INDEX = PACKET_SIZE_INDEX + 2;
    private static final int ORIGINATOR_INDEX = TYPE_INDEX + 1;
    private static final int SERIAL_INDEX = ORIGINATOR_INDEX + 8;
    private static final int TIMESTAMP_INDEX = SERIAL_INDEX + 8;
    private static final int CHANNELS_INDEX = TIMESTAMP_INDEX + 8;
    private static final int BYTES_PER_SAMPLE_INDEX = CHANNELS_INDEX + 1;
    private static final int NUMBER_SAMPLES_INDEX = BYTES_PER_SAMPLE_INDEX + 1;
    private static final int FIRST_SAMPLE_INDEX = NUMBER_SAMPLES_INDEX + 2;
    private static final String EMPTY_ORIGINATOR = "00000000";
    /**
     * @return  buffer contains
     *    -- checksum on the rest of the buffer (2 byte, big endian)    0-1
     *    -- number of bytes, including checksum (2 byte, big endian)   2-3
     *    -- type                                                       4
     *    -- originator (8 bytes, ascii)                                5-12
     *    -- serial number (8 bytes, big endian)                        13-20
     *    -- timestamp (8 bytes, big endian)                            21-28
     *    -- number of channels (1 byte)                                29
     *    -- number of bytes per sample (1 byte)                        30
     *    -- number of samples (2 byte, big endian)                     31-32
     *    -- samples, channels are placed in sequence 0,1,2..           33-
     */
    public byte[] produceUDPBuffer ()
    {
        // we do not want Null Vessels out in the network, should have been filtered before reaching here
        Preconditions.checkState( ! isNull() );

        final int samplesBufferSizeInBytes = numberOfSamples * numberOfChannels * bytesPerSample;

        final int totalLengthInBytes = FIRST_SAMPLE_INDEX + samplesBufferSizeInBytes;
        final byte[] buffer = new byte[totalLengthInBytes];

        produceSamplesBuffer ( buffer, FIRST_SAMPLE_INDEX );

        buffer[PACKET_SIZE_INDEX] = AudioLineUtils.getMsbFromInt( totalLengthInBytes );
        buffer[PACKET_SIZE_INDEX+1] = AudioLineUtils.getLsbFromInt( totalLengthInBytes );
        buffer[TYPE_INDEX] = type.value;

        System.arraycopy(
            originatorId.getBytes(StandardCharsets.US_ASCII),
            0,
            buffer,
            ORIGINATOR_INDEX,
            8 );

        System.arraycopy(
            AudioLineUtils.getBytesFromLong(serialNumber),
            0,
            buffer,
            SERIAL_INDEX,
            8 );

        System.arraycopy(
            AudioLineUtils.getBytesFromLong( timestamp ),
            0,
            buffer,
            TIMESTAMP_INDEX,
            8 );

        buffer[CHANNELS_INDEX] = (byte) numberOfChannels;
        buffer[BYTES_PER_SAMPLE_INDEX] = (byte) bytesPerSample;

        buffer[NUMBER_SAMPLES_INDEX] = AudioLineUtils.getMsbFromInt( numberOfSamples );
        buffer[NUMBER_SAMPLES_INDEX+1] = AudioLineUtils.getLsbFromInt( numberOfSamples );

        final int checksum = iComputeChecksum ( buffer, 2, buffer.length - 2);

        buffer[CHECKSUM_INDEX] = AudioLineUtils.getMsbFromInt( checksum );
        buffer[CHECKSUM_INDEX+1] = AudioLineUtils.getLsbFromInt( checksum );

        return buffer;
    }

    public int getSample( int channelIndex, int sampleIndex )
    {
        return channelVessels[channelIndex].values[sampleIndex];
    }

    public void setSample( int channelIndex, int sampleIndex, int value )
    {
        channelVessels[channelIndex].values[sampleIndex] = value;
    }

    public int getNumberOfSamples()
    {
        return numberOfSamples;
    }

    public int getNumberOfChannels ( )
    {
        return numberOfChannels;
    }

    public SingleChannelVessel getChannel( int chhanelIndex )
    {
        return channelVessels[chhanelIndex];
    }

    public int getBytesPerSample ( )
    {
        return bytesPerSample;
    }

    public long getSerialNumber()
    {
        return serialNumber;
    }

    @Override
    public String toString ( )
    {
        return "Vessel{"
            + "type="
            + type
            + ", originatorId="
            + originatorId
            + ", serialNumber="
            + serialNumber
            + ", timestamp="
            + timestamp
            + ", numberOfChannels="
            + numberOfChannels
            + ", bytesPerSample="
            + bytesPerSample
            + ", numberOfSamples="
            + numberOfSamples
            + ", channelVessels="
            + Arrays.toString ( channelVessels )
            + '}';
    }

    public String debugSingleLiner( )
    {
        return String.format( "type:%s, serial:%d, samples:%d, stamp:%d", type, serialNumber, numberOfSamples, timestamp );
    }

    private static int iComputeChecksum (byte[] buff, int offset, int len )
    {
        int sum = 0;

        for ( int i=0; i<len; i++ )
        {
            sum = 31 * sum + AudioLineUtils.iGet8BitsIntoPositiveModule256( buff[offset+i] );
            sum = sum % 0xffff;
        }

        return AudioLineUtils.convert16BitsIntoSignedNumber(sum);
    }

    public static class SingleChannelVessel
    {
        private final int capacity;
        private final int[] values;

        private SingleChannelVessel ( int inCapacity )
        {
            capacity = inCapacity;
            values = new int[inCapacity];
        }

        public float computeMean ()
        {
            long sum = 0;

            for (int i = 0; i < capacity; i++ )
            {
                sum = sum + values[i];
            }

            return ( (float) sum ) / capacity;
        }

        public float computePower()
        {
            final float mean = computeMean ();

            float sum = 0.0f;

            for (int i = 0; i < capacity; i++ )
            {
                final float ac = ( (float) values[i] ) - mean;
                sum = sum + ac * ac;
            }

            return sum / capacity;
        }

        @Override
        public String toString ( )
        {
            return "SingleChannelVessel{"
                + ", capacity="
                + capacity
                + ", values="
                + Arrays.toString ( values )
                + '}';
        }
    }
}
