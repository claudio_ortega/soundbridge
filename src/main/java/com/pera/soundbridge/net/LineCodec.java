/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.net;

import com.pera.soundbridge.audio.IObservable;
import com.pera.soundbridge.audio.util.AudioLineUtils;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *  Line Codec format:
 *   - start of frame header
 *   - payload bytes
 *   - IMPORTANT: payload bytes with indices 2,3 (0 based) in the payload should
 *                hold its length in bytes as we have only two bytes to hold the
 *                length then the max buffer size is 2^16-1
 */
public class LineCodec implements IObservable
{
    private final static byte[] HEADER_MARK = new byte[] {0x70,0x70, 0x70,0x70, 0x70,0x70, 0x70,0x70, 0x70,0x70};

    // we have only two bytes to hold the length
    private final static int MAX_BUFFER_LENGTH = Integer.rotateLeft( 1, 16 ) - 1;  // 2 bytes unsigned
    private final int lengthMsbZeroBasedIndex;
    private final int lengthLsbZeroBasedIndex;
    private final String id;
    private final byte[] buffer;
    private int waterMark;
    private long malformedPayloadCount;
    private long bufferTooBigCount;
    private long badFramingCount;

    private LineCodec ( String inId, int startIndex )
    {
        buffer = new byte[MAX_BUFFER_LENGTH];
        waterMark = 0;
        malformedPayloadCount = 0;
        bufferTooBigCount = 0;
        badFramingCount = 0;
        id = inId;
        lengthMsbZeroBasedIndex = startIndex;
        lengthLsbZeroBasedIndex = startIndex+1;
    }

    public static LineCodec create( String inId, int startIndex )
    {
        return new LineCodec ( inId, startIndex );
    }

    public <T> byte[] constructBufferFromPayload ( T in, SerializerDeserializer<T> serializerDeserializer )
    {
        final byte[] inner = serializerDeserializer.serialize( in );
        final byte[] outer = new byte[inner.length + HEADER_MARK.length];

        System.arraycopy (HEADER_MARK, 0, outer, 0, HEADER_MARK.length );
        System.arraycopy ( inner, 0, outer, HEADER_MARK.length, inner.length );

        return outer;
    }

    public interface SerializerDeserializer<T> {
        T deserialize ( byte[] buffer, int startIndex, int length );
        byte[] serialize ( T in );
        boolean isNull ( T in );
    }

    public <T> void extractPayloadsFromBuffer (
        byte[] in,
        int buffLen,
        List<T> result,
        SerializerDeserializer<T> serializerDeserializer )
    {
        if ( waterMark + buffLen > buffer.length )
        {
            bufferTooBigCount++;
            waterMark = 0;
        }

        else
        {
            System.arraycopy ( in, 0, buffer, waterMark, buffLen );
            waterMark += buffLen;

            final List<FrameBoundaries> frames = findFrameBoundaries ();

            if ( ! frames.isEmpty () )
            {
                for ( FrameBoundaries frame : frames )
                {
                    if ( frame.endIndex >= waterMark )
                    {
                        badFramingCount++;
                    }
                    else
                    {
                        final T payload = serializerDeserializer.deserialize( buffer, frame.startIndex, frame.length );

                        if ( serializerDeserializer.isNull( payload ) )
                        {
                            malformedPayloadCount++;
                        }
                        else
                        {
                            result.add( payload );
                        }
                    }
                }

                // any leftover after last complete frame?
                final int lastIndex = frames.get( frames.size () - 1 ).endIndex;

                if ( (lastIndex+1) < waterMark )
                {
                    System.arraycopy(
                        buffer,
                        lastIndex+1,
                        buffer,
                        0,
                        waterMark - (lastIndex+1)
                    );
                }

                waterMark = waterMark - (lastIndex+1);
            }
        }
    }

    /**
     *  both endIndex,endIndex are inside the frame
     *  so length = end-start-1
     */
    private static class FrameBoundaries
    {
        int startIndex;
        int endIndex;
        int length;

        private FrameBoundaries ( int startIndex, int endIndex, int length )
        {
            this.startIndex = startIndex;
            this.endIndex = endIndex;
            this.length = length;
        }
    }

    /**
     *    returns the boundaries -excluding- the start of frame marker
     *    and only if the payload is complete in the current
     *    buffer -> buffer+watermark
     */
    private List<FrameBoundaries> findFrameBoundaries ( )
    {
        final List<FrameBoundaries> boundaries = new LinkedList<>();

        int consecutiveMarks = 0;

        for ( int i=0; i<waterMark; i++ )
        {
            final byte a = buffer[i];
            final byte b = HEADER_MARK[consecutiveMarks];

            if ( a == b )
            {
                consecutiveMarks++;
            }
            else
            {
                consecutiveMarks = 0;
            }

            if ( consecutiveMarks == HEADER_MARK.length )
            {
                consecutiveMarks = 0;

                // check if the space left in the buffer up to watermark
                // can possibly contain a complete payload
                // the payload should have two bytes for length at indices 2,3 zero based
                final int leftSpace = waterMark - i - 1;
                if ( leftSpace >= lengthLsbZeroBasedIndex + 1 )
                {
                    final int length = AudioLineUtils.getIntFromBytes(
                        buffer[ i + (lengthMsbZeroBasedIndex + 1) ],
                        buffer[ i + (lengthLsbZeroBasedIndex + 1) ] );

                    if ( leftSpace >= length )
                    {
                        boundaries.add( new FrameBoundaries ( i+1, i+length, length) );
                    }

                    i = i + length;
                }
            }
        }

        return boundaries;
    }

    @Override
    public void getCounters(Map<String, Number> m)
    {
        m.put( String.format( "%s.%s.malformedPayloadCount", getClass().getSimpleName(), id ), malformedPayloadCount);
        m.put( String.format( "%s.%s.bufferTooBigCount", getClass().getSimpleName(), id ), bufferTooBigCount );
        m.put( String.format( "%s.%s.badFramingCount", getClass().getSimpleName(), id ), badFramingCount );
    }

    @Override
    public boolean isOperational()
    {
        return true;
    }
}
