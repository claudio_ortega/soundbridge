/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.net;

import com.google.common.base.*;
import com.pera.soundbridge.audio.sink.ISink;
import com.pera.soundbridge.audio.source.ISource;
import com.pera.soundbridge.audio.util.Constants;
import com.pera.soundbridge.vessel.Vessel;
import org.apache.logging.log4j.*;
import java.net.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class UDPAdaptor implements ISource, ISink
{
    private final Logger logger = LogManager.getLogger();

    private final byte[] sourceBuffer;
    private final ScheduledThreadPoolExecutor executor;
    private final BlockingQueue<Vessel> queueSource;
    private final AtomicLong droppedSourceSamplesCount;
    private final AtomicLong currentSourceSamplesCount;
    private final AtomicLong droppedSinkSamplesCount;
    private final AtomicLong currentSinkSamplesCount;
    private final AtomicBoolean cancel;
    private final LineCodec codec;
    private final EndpointBuilder builderSource;
    private final EndpointBuilder builderSink;
    private final DatagramSocket socket;
    private volatile String remoteIPAddress;
    private volatile InetAddress remoteInetAddress;
    private volatile int remotePort;
    private final AtomicLong packetsReceived;
    private final AtomicLong packetsSent;

    public static UDPAdaptor create(
        EndpointBuilder inBuilderSource,
        EndpointBuilder inBuilderSink )
            throws UnknownHostException, SocketException
    {
        return new UDPAdaptor( inBuilderSource, inBuilderSink );
    }

    private UDPAdaptor(
        EndpointBuilder aInBuilderSource,
        EndpointBuilder aInBuilderSink )
            throws UnknownHostException, SocketException
    {
        builderSource = aInBuilderSource;
        builderSink = aInBuilderSink;
        remoteIPAddress = aInBuilderSink.getIpAddress();
        remoteInetAddress = InetAddress.getByName(remoteIPAddress);
        remotePort = aInBuilderSink.getPort();
        sourceBuffer = new byte[Constants.UDP_MAX_PACKET_ACCEPTED_LENGTH];
        queueSource = new ArrayBlockingQueue<> ( aInBuilderSource.getMaximumEnqueuedBuffers());
        executor = new ScheduledThreadPoolExecutor ( 1 );
        droppedSourceSamplesCount = new AtomicLong( 0 );
        currentSourceSamplesCount = new AtomicLong( 0 );
        droppedSinkSamplesCount = new AtomicLong( 0 );
        currentSinkSamplesCount = new AtomicLong( 0 );
        cancel = new AtomicBoolean ( false );
        packetsReceived = new AtomicLong( 0 );
        packetsSent = new AtomicLong( 0 );
        codec = LineCodec.create ( aInBuilderSource.getId(), 2 );
        socket = new DatagramSocket(
            builderSource.getPort (),
            InetAddress.getByName ( builderSource.getIpAddress () ) );
        logger.info(
            "opening socket on {}:{}",
            InetAddress.getByName ( builderSource.getIpAddress () ),
            builderSource.getPort ());
    }

    @Override
    public String getId()
    {
        return builderSource.getId() + "---" + builderSink.getId();
    }

    private static LineCodec.SerializerDeserializer<Vessel> iGetSerializerDeserializer() {
        return new LineCodec.SerializerDeserializer<Vessel>() {
            @Override
            public Vessel deserialize(byte[] buffer, int startIndex, int length) {
                return Vessel.createFromUDPBuffer( buffer, startIndex, length );
            }

            @Override
            public boolean isNull(Vessel in) {
                return in.isNull();
            }

            @Override
            public byte[] serialize(Vessel in) {
                return in.produceUDPBuffer();
            }
        };
    }

    @Override
    public void init ()
    {
        executor.submit (
            ( ) ->
            {
                logger.debug( "executor.run() -- BEGIN" );

                final List<Vessel> vesselList = new LinkedList<>();

                while ( !cancel.get ( ) )
                {
                    try
                    {
                        final DatagramPacket dp = new DatagramPacket(sourceBuffer, sourceBuffer.length);

                        socket.receive(dp);

                        if ( remotePort < 0 ) {
                            remotePort = dp.getPort();
                            logger.info( "learned remotePort:{}", remotePort );
                        }

                        if ( remoteIPAddress.isEmpty() ) {
                            remoteIPAddress = dp.getAddress().getHostAddress();
                            remoteInetAddress = InetAddress.getByName(remoteIPAddress);
                            logger.info( "learned remoteIPAddress:{}", remoteIPAddress );
                        }

                        logger.log(
                            (packetsReceived.get() == 0) ? Level.INFO : Level.DEBUG,
                            "receiving packet with sequence:{}, bytes:{}, from: {}:{}",
                            packetsReceived::get,
                            dp::getLength,
                            () -> dp.getAddress().getHostAddress(),
                            dp::getPort);

                        packetsReceived.incrementAndGet();

                        vesselList.clear();

                        codec.extractPayloadsFromBuffer(
                            dp.getData(),
                            dp.getLength(),
                            vesselList,
                            iGetSerializerDeserializer()
                        );

                        for ( final Vessel vessel : vesselList )
                        {
                            // is the queueSource full()?
                            // we do this because we have already blocked on socket.receive();
                            // so then here we do not block, but drop the data if the queueSource is full
                            if ( queueSource.size() > builderSource.getMaximumEnqueuedBuffers()/10 )
                            {
                                droppedSourceSamplesCount.addAndGet(vessel.getNumberOfSamples());
                                logger.debug( "dropping vessel: {}", vessel.debugSingleLiner() );
                            }
                            else
                            {
                                queueSource.put(vessel);
                                currentSourceSamplesCount.addAndGet(vessel.getNumberOfSamples());
                            }
                        }
                    }

                    catch ( Throwable ex )
                    {
                        if ( cancel.get() )
                        {
                            logger.debug( String.format( "executor out from the receive: %s", ex.getMessage() ) );
                        }
                        else
                        {
                            logger.warn(ex.getMessage(), ex);
                        }
                    }
                }

                logger.debug( "executor.run() -- END" );            }
        );
    }

    @Override
    public void consume(Vessel vessel) throws Exception
    {
        Preconditions.checkArgument( ! vessel.isNull(), "vessel should not be null" );

        final byte[] buffer = codec.constructBufferFromPayload(
            vessel,
            iGetSerializerDeserializer()
        );

        if ( remoteIPAddress.isEmpty() || remotePort < 0 )
        {
            droppedSinkSamplesCount.addAndGet(vessel.getNumberOfSamples());
        }
        else
        {
            final DatagramPacket dp = new DatagramPacket(
                buffer,
                buffer.length,
                remoteInetAddress,
                remotePort);

            socket.send(dp);

            logger.log(
                (packetsSent.get() == 0) ? Level.INFO : Level.DEBUG,
                "sending packet with sequence:{}, bytes:{}, to: {}:{}",
                packetsSent::get,
                dp::getLength,
                () -> dp.getAddress().getHostAddress(),
                dp::getPort);

            packetsSent.incrementAndGet();
            currentSinkSamplesCount.addAndGet(vessel.getNumberOfSamples());
        }
    }

    @Override
    public Vessel produce(long timeout, TimeUnit unit) throws Exception
    {
        final Vessel vessel = queueSource.poll(timeout, unit);

        if ( vessel == null )
        {
            return Vessel.NULL;
        }
        else
        {
            return vessel;
        }
    }

    @Override
    public void start ()
    {
        logger.debug("no-op");
    }

    @Override
    public void stop ()
    {
        logger.debug("no-op");
    }

    @Override
    public boolean waitForStop(long timeout, TimeUnit unit)
    {
        return true;
    }

    @Override
    public void close() throws InterruptedException
    {
        logger.debug ( "close() -- BEGIN" );

        cancel.set ( true );

        executor.shutdown ();

        queueSource.clear();
        socket.close();

        logger.debug ( "close() -- END" );
    }

    @Override
    public long getDroppedSourceSamplesCount() {
        return droppedSourceSamplesCount.get();
    }

    @Override
    public long getProducedSourceSamplesCount() {
        return currentSourceSamplesCount.get();
    }

    @Override
    public long getConsumedSinkSamplesCount() {
        return currentSinkSamplesCount.get();
    }

    @Override
    public long getDroppedSinkSamplesCount() {
        return droppedSinkSamplesCount.get();
    }

    @Override
    public void getCounters ( Map<String, Number> m )
    {
        m.put( String.format( "%s.%s.queueSource.size", getClass().getSimpleName(), getId() ), (long) queueSource.size() );
        m.put( String.format( "%s.%s.droppedSourceSamplesCount", getClass().getSimpleName(), getId() ), droppedSourceSamplesCount.get() );
        m.put( String.format( "%s.%s.currentSourceSamplesCount", getClass().getSimpleName(), getId() ), currentSourceSamplesCount.get() );
        m.put( String.format( "%s.%s.droppedSinkSamplesCount", getClass().getSimpleName(), getId() ), droppedSinkSamplesCount.get() );
        m.put( String.format( "%s.%s.currentSinkSamplesCount", getClass().getSimpleName(), getId() ), currentSinkSamplesCount.get() );
        codec.getCounters( m );
    }

    @Override
    public boolean isOperational()
    {
        return true;
    }
}
