/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.net;

import com.pera.soundbridge.audio.IIdentifiable;

public class EndpointBuilder implements IIdentifiable
{
    private int port;
    private String ipAddress;
    private String id;
    private int maximumEnqueuedBuffers;

    private EndpointBuilder()
    {
        ipAddress = "";
        port = -1;
        maximumEnqueuedBuffers = -1;
    }

    public static EndpointBuilder init()
    {
        return new EndpointBuilder();
    }

    public EndpointBuilder create ()
    {
        return this;
    }

    public int getMaximumEnqueuedBuffers() {
        return maximumEnqueuedBuffers;
    }

    public EndpointBuilder setMaximumEnqueuedBuffers(int value) {
        maximumEnqueuedBuffers = value;
        return this;
    }

    public int getPort ( )
    {
        return port;
    }

    public EndpointBuilder setPort (int value )
    {
        port = value;
        return this;
    }

    public String getIpAddress ( )
    {
        return ipAddress;
    }

    public EndpointBuilder setIpAddress (String value )
    {
        ipAddress = value;
        return this;
    }

    @Override
    public String getId() {
        return id;
    }

    public EndpointBuilder setId(String value) {
        id = value;
        return this;
    }
}
