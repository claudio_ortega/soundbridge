/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.restinfo;

import com.google.gson.annotations.Expose;
import java.util.*;
import java.util.concurrent.*;

public class SoundBridgeStatus
{
    @Expose private final List<String> counters;
    @Expose private volatile long appStartTime;
    @Expose private volatile long currentTime;
    @Expose private volatile long lastStationStartTime;
    @Expose private volatile long lastStationStopTime;
    @Expose private volatile boolean stationOperational;
    @Expose private volatile String appVersion;
    @Expose private volatile String osName;
    @Expose private volatile String osVersion;
    @Expose private volatile String osArch;
    @Expose private volatile int numberOfProcessors;
    @Expose private volatile String cmdLineArgs;
    @Expose private volatile String processId;
    @Expose private volatile String currentDirectory;
    @Expose private volatile String javaArgs;
    @Expose private volatile String javaHome;
    @Expose private volatile String javaRuntimeVersion;
    @Expose private volatile boolean pttIsOn;
    @Expose private volatile Map<Number,Boolean> vhfBcmIndexMap;
    @Expose private volatile String logLevel;
    @Expose private volatile SoundBridgeConfig soundBridgeConfig;

    public static SoundBridgeStatus create()
    {
        return new SoundBridgeStatus();
    }

    private SoundBridgeStatus()
    {
        vhfBcmIndexMap = new ConcurrentHashMap<>();
        counters = new LinkedList<>();
    }

    public SoundBridgeStatus setSoundBridgeConfig(SoundBridgeConfig soundBridgeConfig) {
        this.soundBridgeConfig = soundBridgeConfig;
        return this;
    }

    public SoundBridgeStatus setLogLevel(String value) {
        logLevel = value;
        return this;
    }

    public Map<Number,Boolean> getVhfBcmIndexMap() {
        return vhfBcmIndexMap;
    }

    public SoundBridgeStatus putInVhfBcmIndexMap(Number n, boolean b) {
        vhfBcmIndexMap.put( n, b );
        return this;
    }

    public SoundBridgeStatus setAppStartTime(long value )
    {
        appStartTime = value;
        return this;
    }

    public SoundBridgeStatus setCurrentTime(long value )
    {
        currentTime = value;
        return this;
    }

    public SoundBridgeStatus setCounters(Map<String,Number> m )
    {
        counters.clear();
        for ( Map.Entry<String,Number> entry: m.entrySet() ) {
            counters.add( String.format( "%s:%d", entry.getKey(), entry.getValue().longValue() ) );
        }
        counters.sort( String::compareTo );
        return this;
    }

    public SoundBridgeStatus setLastStationStartTime(long value) {
        lastStationStartTime = value;
        return this;
    }

    public SoundBridgeStatus setLastStationStopTime(long value) {
        lastStationStopTime = value;
        return this;
    }

    public SoundBridgeStatus setAppVersion(String appVersion) {
        this.appVersion = appVersion;
        return this;
    }

    public SoundBridgeStatus setOsName(String osName) {
        this.osName = osName;
        return this;
    }

    public SoundBridgeStatus setOsVersion(String osVersion) {
        this.osVersion = osVersion;
        return this;
    }

    public SoundBridgeStatus setOsArch(String osArch) {
        this.osArch = osArch;
        return this;
    }

    public SoundBridgeStatus setNumberOfProcessors(int numberOfProcessors) {
        this.numberOfProcessors = numberOfProcessors;
        return this;
    }

    public SoundBridgeStatus setCmdLineArgs(String cmdLineArgs) {
        this.cmdLineArgs = cmdLineArgs;
        return this;
    }

    public SoundBridgeStatus setProcessId(String processId) {
        this.processId = processId;
        return this;
    }

    public SoundBridgeStatus setCurrentDirectory(String currentDirectory) {
        this.currentDirectory = currentDirectory;
        return this;
    }

    public SoundBridgeStatus setJavaArgs(String javaArgs) {
        this.javaArgs = javaArgs;
        return this;
    }

    public SoundBridgeStatus setJavaHome(String javaHome) {
        this.javaHome = javaHome;
        return this;
    }

    public SoundBridgeStatus setJavaRuntimeVersion(String javaRuntimeVersion) {
        this.javaRuntimeVersion = javaRuntimeVersion;
        return this;
    }

    public boolean getPttIsOn() {
        return pttIsOn;
    }

    public SoundBridgeStatus setPttIsOn(boolean pttIsOn) {
        this.pttIsOn = pttIsOn;
        return this;
    }

    public SoundBridgeStatus setStationIsOperational(boolean value) {
        this.stationOperational = value;
        return this;
    }
}
