/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.restinfo;

import com.google.gson.annotations.Expose;
import com.pera.soundbridge.audio.common.*;
import com.pera.soundbridge.station.*;
import com.pera.util.*;
import java.io.*;
import java.util.*;

public class SoundBridgeConfig
{
    @Expose private StationBuilder.StationTypeEnum stationType;
    @Expose private StationBuilder.AudioSinkOrSourceTypeEnum audioSinkTypeEnum;
    @Expose private StationBuilder.AudioSinkOrSourceTypeEnum audioSourceTypeEnum;
    @Expose private String audioSinkHash;
    @Expose private String audioSourceHash;
    @Expose private boolean fastAudioDeviceTest;
    @Expose private int bufferLengthInMSec;
    @Expose private int bufferQueueLength;
    @Expose private String thisSideIP;
    @Expose private int thisSidePort;
    @Expose private String otherSideIP;
    @Expose private int otherSidePort;
    @Expose private int probePeriodInSec;
    @Expose private int pttHoldInMSec;
    @Expose private FSEnum audioSamplingFrequency;
    @Expose private int sinkDropPacketsModulus;
    @Expose private int sinkDropPacketsSilenceThreshold;
    @Expose private boolean invertGPIOTTLLevelsOnVs;

    public static SoundBridgeConfig create()
    {
        return new SoundBridgeConfig();
    }

    private SoundBridgeConfig() {
        stationType = StationBuilder.StationTypeEnum.nil;
        audioSinkTypeEnum = StationBuilder.AudioSinkOrSourceTypeEnum.nil;
        audioSourceTypeEnum = StationBuilder.AudioSinkOrSourceTypeEnum.nil;
        audioSinkHash = "";
        audioSourceHash = "";
        bufferLengthInMSec = 50;
        bufferQueueLength = 100;
        thisSideIP = "";
        thisSidePort = 5555;
        otherSideIP = "";
        otherSidePort = 5555;
        probePeriodInSec = -1;
        pttHoldInMSec = 200;
        audioSamplingFrequency = FSEnum.value8000;
        sinkDropPacketsModulus = -1;
        sinkDropPacketsSilenceThreshold = -1;
        invertGPIOTTLLevelsOnVs = false;
        fastAudioDeviceTest = false;
    }

    public void loadFromPropertiesFile( File configFile ) throws IOException {

        final Map<String,String> map = OrderedProperties.create().fromFile( configFile ).getMap();

        stationType = StationBuilder.StationTypeEnum.valueOf( CollectionsUtil.getTrimmedString(map, "station_type", stationType.toString() ) );
        bufferLengthInMSec = CollectionsUtil.getInteger(map, "buffer_length_ms", bufferLengthInMSec);
        bufferQueueLength = CollectionsUtil.getInteger(map, "buffer_queue_length", bufferQueueLength);

        audioSourceTypeEnum = StationBuilder.AudioSinkOrSourceTypeEnum.valueOf( CollectionsUtil.getTrimmedString(map, "audio_source_type", audioSourceTypeEnum.toString() ) );
        audioSinkTypeEnum = StationBuilder.AudioSinkOrSourceTypeEnum.valueOf( CollectionsUtil.getTrimmedString(map, "audio_sink_type", audioSinkTypeEnum.toString() ) );

        audioSourceHash = CollectionsUtil.getTrimmedString(map, "audio_source_hash", audioSourceHash);
        audioSinkHash = CollectionsUtil.getTrimmedString(map, "audio_sink_hash", audioSinkHash);

        probePeriodInSec = CollectionsUtil.getInteger(map, "probe_period_in_sec", probePeriodInSec);

        thisSideIP = SystemUtil.getAValidLocalIP(CollectionsUtil.getTrimmedString(map, "this_side_ip", thisSideIP), true );
        thisSidePort = CollectionsUtil.getInteger(map, "this_side_port", thisSidePort);

        otherSideIP = CollectionsUtil.getTrimmedString(map, "other_side_ip", otherSideIP);
        otherSidePort = CollectionsUtil.getInteger(map, "other_side_port", otherSidePort);

        pttHoldInMSec = CollectionsUtil.getInteger(map, "ptt_hold_ms", pttHoldInMSec);

        audioSamplingFrequency = FSEnum.valueOf( "value" + CollectionsUtil.getTrimmedString(map, "audio_fs", Integer.toString(audioSamplingFrequency.getValue() ) ) );

        sinkDropPacketsModulus = CollectionsUtil.getInteger(map, "sink_drop_packet_modulus", sinkDropPacketsModulus);
        sinkDropPacketsSilenceThreshold = CollectionsUtil.getInteger(map, "sink_drop_packet_silence_threshold", sinkDropPacketsSilenceThreshold);

        invertGPIOTTLLevelsOnVs = CollectionsUtil.getBoolean(map, "invert_gpio_ttl_levels_on_vs", invertGPIOTTLLevelsOnVs);

        fastAudioDeviceTest = CollectionsUtil.getBoolean(map, "fast_audio_device_test", fastAudioDeviceTest);
    }

    public StationBuilder.StationTypeEnum getStationType() {
        return stationType;
    }

    public SoundBridgeConfig setStationType(StationBuilder.StationTypeEnum stationType) {
        this.stationType = stationType;
        return this;
    }

    public StationBuilder.AudioSinkOrSourceTypeEnum getAudioSinkTypeEnum() {
        return audioSinkTypeEnum;
    }

    public SoundBridgeConfig setAudioSinkTypeEnum(StationBuilder.AudioSinkOrSourceTypeEnum audioSinkTypeEnum) {
        this.audioSinkTypeEnum = audioSinkTypeEnum;
        return this;
    }

    public StationBuilder.AudioSinkOrSourceTypeEnum getAudioSourceTypeEnum() {
        return audioSourceTypeEnum;
    }

    public SoundBridgeConfig setAudioSourceTypeEnum(StationBuilder.AudioSinkOrSourceTypeEnum audioSourceTypeEnum) {
        this.audioSourceTypeEnum = audioSourceTypeEnum;
        return this;
    }

    public String getAudioSinkHash() {
        return audioSinkHash;
    }

    public SoundBridgeConfig setAudioSinkHash(String audioSinkHash) {
        this.audioSinkHash = audioSinkHash;
        return this;
    }

    public String getAudioSourceHash() {
        return audioSourceHash;
    }

    public SoundBridgeConfig setAudioSourceHash(String audioSourceHash) {
        this.audioSourceHash = audioSourceHash;
        return this;
    }

    public int getBufferLengthInMSec() {
        return bufferLengthInMSec;
    }

    public SoundBridgeConfig setBufferLengthInMSec(int bufferLengthInMSec) {
        this.bufferLengthInMSec = bufferLengthInMSec;
        return this;
    }

    public int getBufferQueueLength() {
        return bufferQueueLength;
    }

    public SoundBridgeConfig setBufferQueueLength(int bufferQueueLength) {
        this.bufferQueueLength = bufferQueueLength;
        return this;
    }

    public String getThisSideIP() {
        return thisSideIP;
    }

    public SoundBridgeConfig setThisSideIP(String thisSideIP) {
        this.thisSideIP = thisSideIP;
        return this;
    }

    public int getThisSidePort() {
        return thisSidePort;
    }

    public SoundBridgeConfig setThisSidePort(int thisSidePort) {
        this.thisSidePort = thisSidePort;
        return this;
    }

    public String getOtherSideIP() {
        return otherSideIP;
    }

    public SoundBridgeConfig setOtherSideIP(String otherSideIP) {
        this.otherSideIP = otherSideIP;
        return this;
    }

    public int getOtherSidePort() {
        return otherSidePort;
    }

    public SoundBridgeConfig setOtherSidePort(int otherSidePort) {
        this.otherSidePort = otherSidePort;
        return this;
    }

    public int getProbePeriodInSec() {
        return probePeriodInSec;
    }

    public SoundBridgeConfig setProbePeriodInSec(int probePeriodInSec) {
        this.probePeriodInSec = probePeriodInSec;
        return this;
    }

    public int getPttHoldInMSec() {
        return pttHoldInMSec;
    }

    public SoundBridgeConfig setPttHoldInMSec(int pttHoldInMSec) {
        this.pttHoldInMSec = pttHoldInMSec;
        return this;
    }

    public FSEnum getAudioSamplingFrequency() {
        return audioSamplingFrequency;
    }

    public SoundBridgeConfig setAudioSamplingFrequency(FSEnum audioSamplingFrequency) {
        this.audioSamplingFrequency = audioSamplingFrequency;
        return this;
    }

    public int getSinkDropPacketsModulus() {
        return sinkDropPacketsModulus;
    }

    public SoundBridgeConfig setSinkDropPacketsModulus(int sinkDropPacketsModulus) {
        this.sinkDropPacketsModulus = sinkDropPacketsModulus;
        return this;
    }

    public int getSinkDropPacketsSilenceThreshold() {
        return sinkDropPacketsSilenceThreshold;
    }

    public SoundBridgeConfig setSinkDropPacketsSilenceThreshold(int sinkDropPacketsSilenceThreshold) {
        this.sinkDropPacketsSilenceThreshold = sinkDropPacketsSilenceThreshold;
        return this;
    }

    public boolean isInvertGPIOTTLLevelsOnVs() {
        return invertGPIOTTLLevelsOnVs;
    }

    public SoundBridgeConfig setInvertGPIOTTLLevelsOnVs(boolean invertGPIOTTLLevelsOnVs) {
        this.invertGPIOTTLLevelsOnVs = invertGPIOTTLLevelsOnVs;
        return this;
    }

    public boolean isFastAudioDeviceTest() {
        return fastAudioDeviceTest;
    }

    public SoundBridgeConfig setFastAudioDeviceTest(boolean fastAudioDeviceTest) {
        this.fastAudioDeviceTest = fastAudioDeviceTest;
        return this;
    }
}
