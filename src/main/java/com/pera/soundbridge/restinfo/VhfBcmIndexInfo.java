/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.restinfo;

import com.google.gson.annotations.Expose;

public class VhfBcmIndexInfo
{
    public enum Action { add, remove, noop }

    @Expose private final int bcmIndex;
    @Expose private final Action action;

    public static VhfBcmIndexInfo create()
    {
        return new VhfBcmIndexInfo();
    }

    private VhfBcmIndexInfo()
    {
        bcmIndex = -1;
        action = Action.noop;
    }

    public int getBcmIndex()
    {
        return bcmIndex;
    }

    public Action getAction()
    {
        return action;
    }
}
