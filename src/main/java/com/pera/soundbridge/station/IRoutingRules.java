/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.station;

import com.pera.soundbridge.audio.filter.*;

interface IRoutingRules extends AutoCloseable {
    ISignalFilter getFilterProbeToNet();
    ISignalFilter getFilterLocalToNet();
    ISignalFilter getFilterNetToLocal();
    ISignalFilter getFilterNetToNet();
    ISignalFilter getFilterLocalToLocal();
}
