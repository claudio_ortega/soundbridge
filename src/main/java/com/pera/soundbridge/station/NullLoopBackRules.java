/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.station;

import com.pera.soundbridge.audio.filter.ISignalFilter;
import com.pera.soundbridge.vessel.Vessel;

public class NullLoopBackRules implements IRoutingRules
{
    public static NullLoopBackRules create()
    {
        return new NullLoopBackRules();
    }

    @Override
    public ISignalFilter getFilterProbeToNet() {
        return v -> Vessel.NULL;
    }

    @Override
    public ISignalFilter getFilterLocalToNet() {
        return v -> Vessel.NULL;
    }

    @Override
    public ISignalFilter getFilterNetToLocal() {
        return v -> Vessel.NULL;
    }

    @Override
    public ISignalFilter getFilterNetToNet() {
        return v -> Vessel.NULL;
    }

    @Override
    public ISignalFilter getFilterLocalToLocal() {
        return v -> Vessel.NULL;
    }

    @Override
    public void close() {
    }
}
