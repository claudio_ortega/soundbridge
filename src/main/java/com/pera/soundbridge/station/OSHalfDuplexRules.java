/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.station;

import com.google.common.base.*;
import com.pera.soundbridge.audio.filter.ISignalFilter;
import com.pera.soundbridge.vessel.Vessel;

public class OSHalfDuplexRules implements IRoutingRules
{
    private final StationBuilder builder;
    private final ISignalFilter probeToNet;
    private final ISignalFilter localToNet;
    private final ISignalFilter netToLocal;
    private final ISignalFilter netToNet;

    public static OSHalfDuplexRules create(StationBuilder builder )
    {
        return new OSHalfDuplexRules( builder );
    }

    private OSHalfDuplexRules(StationBuilder inBuilder )
    {
        builder = inBuilder;

        probeToNet = v -> {
            Preconditions.checkState( v.isProbe() );
            builder.getDelayRecorder().getFilterA().accept( v );
            return v;
        };

        localToNet = v -> {
            if ( builder.getPttCommandSupplier().get() )
            {
                return v;
            }
            else
            {
                return Vessel.NULL;
            }
        };

        netToLocal = v -> {
            if ( v.isProbe() )
            {
                builder.getDelayRecorder().getFilterB().accept( v );
            }

            return v;
        };

        netToNet = v -> Vessel.NULL;
    }

    @Override
    public ISignalFilter getFilterLocalToNet() {
        return localToNet;
    }

    @Override
    public ISignalFilter getFilterProbeToNet() {
        return probeToNet;
    }

    @Override
    public ISignalFilter getFilterNetToLocal() {
        return netToLocal;
    }

    @Override
    public ISignalFilter getFilterNetToNet() {
        return netToNet;
    }

    @Override
    public ISignalFilter getFilterLocalToLocal() {
        return v -> Vessel.NULL;
    }

    @Override
    public void close() {
    }
}
