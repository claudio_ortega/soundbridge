/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */
package com.pera.soundbridge.station;

import com.pera.soundbridge.audio.filter.ISignalFilter;
import com.pera.soundbridge.vessel.*;

public class FullDuplexRules implements IRoutingRules
{
    private final StationBuilder builder;

    public static FullDuplexRules create(StationBuilder builder )
    {
        return new FullDuplexRules( builder );
    }

    private FullDuplexRules(StationBuilder inBuilder ) {
        builder = inBuilder;
    }

    public ISignalFilter getFilterProbeToNet() {
        return v -> {
            if (v.isProbe()) {
                builder.getDelayRecorder().getFilterA().accept(v);
            }
            return v;
        };
    }

    public ISignalFilter getFilterLocalToNet() {
        return v -> v;
    }

    public ISignalFilter getFilterNetToLocal() {
        return v -> {
            if (v.isProbe()) {
                builder.getDelayRecorder().getFilterB().accept(v);
            }
            return v;
        };
    }

    @Override
    public ISignalFilter getFilterNetToNet() {
        return v -> {
            if ( v.isProbe() ) {
                if ( v.getOriginatorId().equals( builder.getDelayRecorder().getId() ) ) {
                    return Vessel.NULL;
                }
                else {
                    return v;
                }
            }
            else {
                return Vessel.NULL;
            }
        };
    }

    @Override
    public ISignalFilter getFilterLocalToLocal() {
        return v -> Vessel.NULL;
    }

    @Override
    public void close() {
    }
}
