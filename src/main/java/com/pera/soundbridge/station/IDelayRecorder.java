/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2020.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.station;

import com.google.gson.annotations.*;
import com.pera.soundbridge.vessel.*;
import java.util.*;
import java.util.function.*;

public interface IDelayRecorder {
    // *ordered* list for delay -> vessel count on that delay (ms)
    void reset();
    DelayRecorder.Result compute();
    Consumer<Vessel> getFilterA();
    Consumer<Vessel> getFilterB();
    String getId();

    class Result {
        @Expose String id;
        @Expose public Float meanDelayMSec;
        @Expose public Float stdDevDelayMSec;
        @Expose Long lastSerial;
        @Expose public Long totalCount;
        @Expose List<DelayStat> delayFrequency;

        Result() {
            id = "";
            meanDelayMSec = 0.0f;
            stdDevDelayMSec = 0.0f;
            lastSerial = 0L;
            totalCount = 0L;
            delayFrequency = Collections.emptyList();
        }

        static class DelayStat {
            @Expose final Long delayMsec;
            @Expose final Long count;
            DelayStat(Long inDelayMsec, Long inCount )
            {
                delayMsec = inDelayMsec;
                count = inCount;
            }
        }
    }
}
