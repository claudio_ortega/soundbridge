/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.station;

import com.pera.soundbridge.audio.filter.ISignalFilter;
import com.pera.util.TaggedThreadFactory;
import com.pera.soundbridge.vessel.Vessel;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class VSHalfDuplexRules implements IRoutingRules
{
    private final AtomicBoolean pttOn;
    private final ISignalFilter probeToNet;
    private final ISignalFilter localToNet;
    private final ISignalFilter netToLocal;
    private final ISignalFilter netToNet;
    private final ScheduledThreadPoolExecutor executor;

    public static VSHalfDuplexRules create(final StationBuilder inBuilder ) {
        return new VSHalfDuplexRules( inBuilder );
    }

    private VSHalfDuplexRules(final StationBuilder inBuilder ) {

        final int pttHoldMs = inBuilder.getSoundBridgeConfig().getPttHoldInMSec();

        final AtomicLong lastPacketReceivedTime = new AtomicLong(System.currentTimeMillis());

        executor = new ScheduledThreadPoolExecutor(
            1,
            TaggedThreadFactory.create( getClass(), "1" ) );

        pttOn = new AtomicBoolean( false );
        inBuilder.getPttStateConsumer().accept( false );

        executor.scheduleAtFixedRate (
            () -> {
                final boolean newPttValue =  ( System.currentTimeMillis() - lastPacketReceivedTime.get() ) < pttHoldMs;
                pttOn.set( newPttValue );
                inBuilder.getPttStateConsumer().accept( newPttValue );
            },
            0,
            pttHoldMs/4,
            TimeUnit.MILLISECONDS
        );

        probeToNet = v -> Vessel.NULL;

        localToNet = v ->
        {
            if ( v.isProbe() )
            {
                return Vessel.NULL;
            }
            else
            {
                if ( pttOn.get() )
                {
                    return Vessel.NULL;
                }
                else
                {
                    return v;
                }
            }
        };

        netToLocal = v ->
        {
            if ( v.isProbe() )
            {
                return Vessel.NULL;
            }
            else
            {
                lastPacketReceivedTime.set ( System.currentTimeMillis() );
                return v;
            }
        };

        netToNet = v ->
        {
            if ( v.isProbe() )
            {
                return v;
            }
            else
            {
                return Vessel.NULL;
            }
        };
    }

    @Override
    public ISignalFilter getFilterProbeToNet() {
        return probeToNet;
    }

    @Override
    public ISignalFilter getFilterLocalToNet() {
        return localToNet;
    }

    @Override
    public ISignalFilter getFilterNetToLocal() {
        return netToLocal;
    }

    @Override
    public ISignalFilter getFilterNetToNet() {
        return netToNet;
    }

    @Override
    public ISignalFilter getFilterLocalToLocal() {
        return v -> Vessel.NULL;
    }

    @Override
    public void close() {
        executor.shutdown();
    }
}
