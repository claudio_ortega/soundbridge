/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.station;

import com.pera.soundbridge.audio.filter.ISignalFilter;
import com.pera.soundbridge.vessel.Vessel;

public class AudioLoopBackRules implements IRoutingRules
{
    public static AudioLoopBackRules create()
    {
        return new AudioLoopBackRules();
    }

    @Override
    public ISignalFilter getFilterProbeToNet() {
        return v -> Vessel.NULL;
    }

    @Override
    public ISignalFilter getFilterLocalToNet() {
        return v -> Vessel.NULL;
    }

    @Override
    public ISignalFilter getFilterNetToLocal() {
        return v -> Vessel.NULL;
    }

    @Override
    public ISignalFilter getFilterNetToNet() {
        return v -> Vessel.NULL;
    }

    @Override
    public ISignalFilter getFilterLocalToLocal() {
        return v -> v;
    }

    @Override
    public void close() {
    }
}
