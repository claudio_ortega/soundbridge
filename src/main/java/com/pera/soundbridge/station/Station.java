/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.station;

import com.google.common.base.Preconditions;
import com.pera.soundbridge.audio.filter.ISignalFilter;
import com.pera.soundbridge.net.EndpointBuilder;
import com.pera.soundbridge.net.UDPAdaptor;
import com.pera.soundbridge.audio.sink.ISink;
import com.pera.soundbridge.audio.source.ISource;
import com.pera.util.TaggedThreadFactory;
import com.pera.soundbridge.vessel.*;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import org.apache.logging.log4j.*;

public class Station implements IStation
{
    private final Logger logger = LogManager.getLogger();
    private final ISource localSource;
    private final List<ISink> localSinks;
    private final UDPAdaptor udpAdaptor;
    private final ThreadPoolExecutor executor;
    private final AtomicBoolean cancel;
    private final AtomicBoolean operational;
    private final AtomicLong probeSerial;
    private final ISignalFilter probeToNetFilter;
    private final ISignalFilter localToNetFilter;
    private final ISignalFilter netToLocalFilter;
    private final ISignalFilter netToNetFilter;
    private final ISignalFilter localToLocalFilter;
    private final CountDownLatch latch;
    private final ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
    private final long probePeriodInSec;
    private final String stationId;

    public static Station create(
        String stationId,
        StationBuilder.StationTypeEnum stationTypeEnum,
        ISource inLocalSource,
        EndpointBuilder inNetBuilderSink,
        EndpointBuilder inNetBuilderSource,
        List<ISink> inLocalSinks,
        ISignalFilter inProbeToNet,
        ISignalFilter inLocalToNet,
        ISignalFilter inNetToLocal,
        ISignalFilter inNetToNet,
        ISignalFilter inLocalToLocal,
        long probePeriodInSec )
            throws UnknownHostException, SocketException
    {
        return new Station(
            stationId,
            stationTypeEnum,
            inLocalSource,
            inNetBuilderSink,
            inNetBuilderSource,
            inLocalSinks,
            inProbeToNet,
            inLocalToNet,
            inNetToLocal,
            inNetToNet,
            inLocalToLocal,
            probePeriodInSec );
    }

    private Station(
        String inStationId,
        StationBuilder.StationTypeEnum stationTypeEnum,
        ISource inLocalSource,
        EndpointBuilder inNetBuilderSink,
        EndpointBuilder inNetBuilderSource,
        List<ISink> inLocalSinks,
        ISignalFilter inProbeToNet,
        ISignalFilter inLocalToNet,
        ISignalFilter inNetToLocal,
        ISignalFilter inNetToNet ,
        ISignalFilter inLocalToLocal,
        long inProbePeriodInSec )
            throws UnknownHostException, SocketException
    {
        cancel = new AtomicBoolean ( true );
        operational = new AtomicBoolean ( false );

        latch = new CountDownLatch ( 2 );

        executor = new ThreadPoolExecutor(
            2,
            2,
            0,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>( ),
            TaggedThreadFactory.create( getClass ( ), stationTypeEnum.toString() + ":non-time-scheduled") );

        scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor (
            1,
            TaggedThreadFactory.create( getClass(), stationTypeEnum.toString() + ":time-scheduled" ) );

        probeSerial = new AtomicLong( 0 );

        localSource = inLocalSource;
        localSinks = inLocalSinks;

        udpAdaptor = UDPAdaptor.create(
            inNetBuilderSource,
            inNetBuilderSink);

        probeToNetFilter = inProbeToNet;
        localToNetFilter = inLocalToNet;
        netToLocalFilter = inNetToLocal;
        netToNetFilter = inNetToNet;
        localToLocalFilter = inLocalToLocal;

        probePeriodInSec = inProbePeriodInSec;

        stationId = inStationId;
    }

    @Override
    public void init() throws Exception
    {
        udpAdaptor.init();
        localSource.init();
        for (ISink sink : localSinks)
        {
            sink.init();
        }
    }

    @Override
    public void stop() throws InterruptedException
    {
        cancel.set( true );
        udpAdaptor.stop();
        localSource.stop();
        for (ISink sink : localSinks)
        {
            sink.stop();
        }
    }

    @Override
    public boolean waitForStop(long timeout, TimeUnit unit)
        throws InterruptedException
    {
        udpAdaptor.waitForStop(timeout, unit);
        localSource.waitForStop(timeout, unit);
        for (ISink sink : localSinks)
        {
            sink.waitForStop(timeout, unit);
        }
        return latch.await( timeout, unit );
    }

    @Override
    public void start() throws Exception
    {
        Preconditions.checkState( cancel.get() );

        cancel.set( false );
        operational.set ( true );

        // local -> net AND local -> local
        executor.submit(
            () ->
            {
                while (!cancel.get())
                {
                    try
                    {
                        final Vessel vessel = localSource.produce(1, TimeUnit.SECONDS);

                        if ( vessel.isNull() )
                        {
                            logger.debug("no vessel received from local source");
                        }
                        else
                        {
                            // local -> net
                            {
                                final Vessel out = localToNetFilter.filter(vessel);

                                if (out.isNull()) {
                                    logger.debug("vessel dropped by local->net filter");
                                } else {
                                    logger.debug("local->net, sending:{},{}", out.getType().toString(), out.getSerialNumber());
                                    udpAdaptor.consume(out);
                                }
                            }

                            // local -> local
                            {
                                final Vessel out = localToLocalFilter.filter(vessel);

                                if (out.isNull()) {
                                    logger.debug("vessel dropped by local->local filter");
                                } else {
                                    logger.debug("local->local, sending:{},{}", out.getType().toString(), out.getSerialNumber());
                                    for (ISink sink : localSinks)
                                    {
                                        sink.consume(out);
                                    }
                                }
                            }
                        }
                    }

                    catch (InterruptedException e)
                    {
                        logger.error(e.getMessage());
                    }

                    catch (Exception e)
                    {
                        logger.error(e.getMessage(), e);
                    }
                }

                latch.countDown();
            }
        );

        // net -> local AND net -> net
        executor.submit(
            () ->
            {
                while (!cancel.get())
                {
                    try
                    {
                        final Vessel vessel = udpAdaptor.produce(1, TimeUnit.SECONDS);

                        if ( vessel.isNull() )
                        {
                            logger.debug("no vessel received from net source");
                        }
                        else
                        {
                            // net -> local
                            {
                                final Vessel out = netToLocalFilter.filter(vessel);

                                if ( out.isNull() )
                                {
                                    logger.debug("vessel dropped by net->local filter");
                                }

                                else
                                {
                                    logger.debug("net->local:{},{}", out.getType().toString(), out.getSerialNumber() );
                                    for (ISink sink : localSinks)
                                    {
                                        sink.consume(out);
                                    }
                                }
                            }

                            // net -> net
                            {
                                final Vessel out = netToNetFilter.filter(vessel);

                                if ( out.isNull() )
                                {
                                    logger.debug("vessel dropped by net->net filter");
                                }

                                else
                                {
                                    logger.debug("net->net:{},{}", out.getType().toString(), out.getSerialNumber() );
                                    udpAdaptor.consume(out);
                                }
                            }
                        }
                    }

                    catch (InterruptedException e)
                    {
                        logger.error(e.getMessage());
                    }

                    catch (Exception e)
                    {
                        logger.error(e.getMessage(), e);
                    }
                }

                latch.countDown();
            }
        );

        // probe generator -> net
        if ( probePeriodInSec < 1 ) {
            logger.info( "probe producer was not requested" );
        }
        else {
            logger.info( "starting probe producer, probePeriodInSec:{}", probePeriodInSec );
            scheduledThreadPoolExecutor.scheduleAtFixedRate(
                () ->
                {
                    try
                    {
                        final Vessel vessel = Vessel.createProbe(
                            stationId,
                            probeSerial.incrementAndGet());
                        final Vessel out = probeToNetFilter.filter(vessel);
                        if ( ! out.isNull()) {
                            udpAdaptor.consume(out);
                        }
                    } catch (Exception e)
                    {
                        logger.error(e.getMessage(), e);
                    }
                },
                probePeriodInSec,
                probePeriodInSec,
                TimeUnit.SECONDS
            );
        }

        localSource.start();
    }

    @Override
    public void close() throws InterruptedException
    {
        final boolean timeoutLatch = !latch.await(5, TimeUnit.SECONDS);
        logger.debug("latch.await() timeoutLatch:{}", timeoutLatch);
        logger.debug("latch.await() latch.getCount():{}", latch.getCount());

        executor.shutdownNow ( );
        scheduledThreadPoolExecutor.shutdownNow ( );

        udpAdaptor.close();

        localSource.close();

        for (ISink sink : localSinks)
        {
            sink.close();
        }

        operational.set ( false );
    }

    @Override
    public void getCounters(Map<String, Number> m)
    {
        localSource.getCounters(m);
        localSinks.forEach( s -> s.getCounters( m ) );
        udpAdaptor.getCounters(m);
    }

    @Override
    public boolean isOperational()
    {
        return operational.get();
    }
}
