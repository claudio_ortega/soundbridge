/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.station;

import com.pera.soundbridge.audio.IInitiable;
import com.pera.soundbridge.audio.IObservable;
import com.pera.soundbridge.audio.IStartable;

public interface IStation extends IInitiable, IObservable, IStartable {
}
