/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.station;

import com.google.common.base.Preconditions;
import com.pera.soundbridge.audio.common.AudioCommon;
import com.pera.soundbridge.audio.common.DriversInfo;
import com.pera.soundbridge.audio.common.FSEnum;
import com.pera.soundbridge.audio.source.hw.*;
import com.pera.soundbridge.net.EndpointBuilder;
import com.pera.soundbridge.audio.sink.*;
import com.pera.soundbridge.audio.sink.hw.AudioSink;
import com.pera.soundbridge.audio.sink.hw.AudioSinkBuilder;
import com.pera.soundbridge.audio.sink.sim.*;
import com.pera.soundbridge.audio.source.*;
import com.pera.soundbridge.audio.source.sim.*;
import com.pera.util.CollectionsUtil;
import com.pera.util.SystemUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.List;

public class StationFactory implements AutoCloseable {

    private static final Logger logger = LogManager.getLogger();

    private IRoutingRules routingRules;

    private StationFactory()
    {
    }

    public static StationFactory create(  ) {
        return new StationFactory();
    }

    @Override
    public void close() throws Exception {
        if ( routingRules != null ) {
            routingRules.close();
        }
    }

    public IStation createStation( StationBuilder builder ) throws Exception {

        switch (builder.getSoundBridgeConfig().getStationType()) {
            case network_loopback:
                routingRules = NetworkLoopBackRules.create ();
                break;
            case audio_loopback:
                routingRules = AudioLoopBackRules.create ();
                break;
            case full_duplex:
                routingRules = FullDuplexRules.create ( builder );
                break;
            case os:
                routingRules = OSHalfDuplexRules.create ( builder );
                break;
            case vs:
                routingRules = VSHalfDuplexRules.create ( builder );
                break;
            case nil:
                routingRules = NullLoopBackRules.create();
                break;
            default:
                throw new IllegalArgumentException("wrong station type:" + builder.getSoundBridgeConfig().getStationType() );
        }

        final int nChannels = 1;
        final FSEnum samplingFreq = builder.getSoundBridgeConfig().getAudioSamplingFrequency();

        final ISource source;

        switch ( builder.getSoundBridgeConfig().getAudioSourceTypeEnum() )
        {
            case nil:
                source = NullSource.create();
                break;

            case simulator:
                source = SimulatedSource.create (
                    SimulatedSourceBuilder
                        .init ( )
                        .setId("sim-audio-source")
                        .setSampleSizeInBytes ( 2 )
                        .setNumberOfChannels ( nChannels )
                        .setSamplingFrequencyEnum ( samplingFreq )
                        .setDc ( 0.0 )
                        .setGain ( 1.0 )
                        .setSignalFrequency( 220.0 )
                        .setSquareModulationFrequency( 1.0 )
                        .setSquareModulationDutyCycle( 0.5 )
                        .setBufferLengthInMSec( builder.getSoundBridgeConfig().getBufferLengthInMSec() )
                        .setMaximumEnqueuedBuffers( builder.getSoundBridgeConfig().getBufferQueueLength() ) );
                break;

            case hw:

                final String effectiveSourceHash;

                if ( ! builder.getSoundBridgeConfig().getAudioSourceHash().isEmpty() ) {
                    effectiveSourceHash = builder.getSoundBridgeConfig().getAudioSourceHash();
                }
                else {
                    logger.info("fetching audio source driver information, this will take a few seconds...");
                    final DriversInfo driversInfo = AudioCommon.fetchDriversInfoFromHardware( true, true, builder.getSoundBridgeConfig() );
                    AudioCommon.listCompatibleDrivers( true, false, logger::info, driversInfo );
                    logger.info("done fetching audio driver information.");

                    final List<String> effectiveSourceHashList = AudioCommon.getSuggestedHashes(
                        driversInfo,
                        builder.getSoundBridgeConfig().getAudioSourceHash() );

                    Preconditions.checkState(
                        effectiveSourceHashList.size() > 0,
                        "unable to find a compatible audio source driver for configured hash:" + driversInfo );

                    effectiveSourceHash = effectiveSourceHashList.get(0);
                }

                logger.info( "hw source: using hash:{}", effectiveSourceHash );

                source = AudioSource.create(
                    AudioSourceBuilder
                        .init()
                        .setId("hw-audio-source")
                        .setNumberOfChannels( nChannels )
                        .setSamplingFrequencyEnum( samplingFreq )
                        .setAudioMixerDescriptionRegexp(effectiveSourceHash)
                        .setBufferLengthInMSec( builder.getSoundBridgeConfig().getBufferLengthInMSec())
                        .setMaximumEnqueuedBuffers( builder.getSoundBridgeConfig().getBufferQueueLength() )
                        .create ( ) );
                break;

            default:
                throw new IllegalArgumentException( "wrong audio_source_type configured: " + builder.getSoundBridgeConfig().getAudioSourceTypeEnum() );
        }

        final ISink sink;

        switch ( builder.getSoundBridgeConfig().getAudioSinkTypeEnum() )
        {
            case nil:
                sink = NullSink.create();
                break;

            case simulator:
                sink = DebugSink.create( "debug-sink", -1 );
                break;

            case hw:

                final String effectiveSinkHash;

                if ( ! builder.getSoundBridgeConfig().getAudioSinkHash().isEmpty() ) {
                    effectiveSinkHash = builder.getSoundBridgeConfig().getAudioSinkHash();
                }
                else {
                    logger.info("fetching audio sink driver information, this will take a few seconds...");
                    final DriversInfo driversInfo = AudioCommon.fetchDriversInfoFromHardware( true, true, builder.getSoundBridgeConfig() );
                    AudioCommon.listCompatibleDrivers( false, true, logger::info, driversInfo );
                    logger.info("done fetching audio driver information.");

                    final List<String> effectiveSinkHashList = AudioCommon.getSuggestedHashes(
                        driversInfo,
                        builder.getSoundBridgeConfig().getAudioSinkHash());

                    Preconditions.checkState(
                        effectiveSinkHashList.size() > 0,
                        "unable to find a compatible audio sink driver for configured hash:" + driversInfo );

                    effectiveSinkHash = effectiveSinkHashList.get(0);
                }

                logger.info( "hw sink: using hash:{}", effectiveSinkHash );

                sink = AudioSink.create(
                    AudioSinkBuilder
                        .init()
                        .setId("hw-audio-sink")
                        .setNumberOfChannels( nChannels )
                        .setSamplingFrequencyEnum( samplingFreq )
                        .setBufferLengthInMSec( builder.getSoundBridgeConfig().getBufferLengthInMSec())
                        .setAudioMixerDescriptionRegexp(effectiveSinkHash)
                        .setMaximumEnqueuedBuffers( builder.getSoundBridgeConfig().getBufferQueueLength() )
                        .setSinkDropPacketsModulus(  builder.getSoundBridgeConfig().getSinkDropPacketsModulus() )
                        .setSinkDropPacketsSilenceThreshold( builder.getSoundBridgeConfig().getSinkDropPacketsSilenceThreshold() )
                        .create() );
                break;

            default:
                throw new IllegalArgumentException( "wrong audio_sink_type configured" );
        }

        return Station.create(
            builder.getDelayRecorder().getId(),
            builder.getSoundBridgeConfig().getStationType(),
            // local source
            source,
            // net sink
            EndpointBuilder
                .init()
                .setId("net-sink")
                .setIpAddress(builder.getSoundBridgeConfig().getOtherSideIP())
                .setPort(builder.getSoundBridgeConfig().getOtherSidePort())
                .create(),
            // net source
            EndpointBuilder
                .init()
                .setId("net-source")
                .setMaximumEnqueuedBuffers( builder.getSoundBridgeConfig().getBufferQueueLength() )
                .setIpAddress(SystemUtil.getAValidLocalIP(builder.getSoundBridgeConfig().getThisSideIP(), true))
                .setPort(builder.getSoundBridgeConfig().getThisSidePort())
                .create(),
            // local sinks
            CollectionsUtil.createList(sink),
            // rules
            routingRules.getFilterProbeToNet(),
            routingRules.getFilterLocalToNet(),
            routingRules.getFilterNetToLocal(),
            routingRules.getFilterNetToNet(),
            routingRules.getFilterLocalToLocal(),
            builder.getSoundBridgeConfig().getProbePeriodInSec()
        );
    }
}

