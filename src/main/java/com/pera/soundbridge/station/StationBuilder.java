/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.station;

import java.util.function.Consumer;
import java.util.function.Supplier;

import com.pera.soundbridge.restinfo.*;
import com.pera.soundbridge.vessel.*;

public class StationBuilder
{
    public enum StationTypeEnum
    {
        nil,
        os,
        vs,
        network_loopback,
        audio_loopback,
        full_duplex
    }

    public enum AudioSinkOrSourceTypeEnum { hw, simulator, nil }

    private SoundBridgeConfig soundBridgeConfig;
    private Consumer<Boolean> pttStateConsumer;
    private Supplier<Boolean> pttCommandSupplier;
    private IDelayRecorder delayRecorder;

    public static StationBuilder create ()
    {
        return new StationBuilder();
    }

    private StationBuilder()
    {
        soundBridgeConfig = null;
        pttStateConsumer = p -> {};
        pttCommandSupplier = () -> false;
        delayRecorder = new IDelayRecorder() {
            @Override
            public DelayRecorder.Result compute() {
                return null;
            }

            @Override
            public Consumer<Vessel> getFilterA() {
                return vessel -> {};
            }

            @Override
            public Consumer<Vessel> getFilterB() {
                return vessel -> {};
            }

            @Override
            public String getId() {
                return "";
            }

            @Override
            public void reset() {
            }
        };
    }

    public IDelayRecorder getDelayRecorder() {
        return delayRecorder;
    }

    public StationBuilder setDelayRecorder(IDelayRecorder value) {
        delayRecorder = value;
        return this;
    }

    public SoundBridgeConfig getSoundBridgeConfig() {
        return soundBridgeConfig;
    }

    public StationBuilder setSoundBridgeConfig(SoundBridgeConfig newSoundBridgeConfig) {
        soundBridgeConfig = newSoundBridgeConfig;
        return this;
    }

    public Consumer<Boolean> getPttStateConsumer() {
        return pttStateConsumer;
    }

    public StationBuilder setPttStateConsumer(Consumer<Boolean> value) {
        pttStateConsumer = value;
        return this;
    }

    public Supplier<Boolean> getPttCommandSupplier() {
        return pttCommandSupplier;
    }

    public StationBuilder setPttCommandSupplier(Supplier<Boolean> value) {
        pttCommandSupplier = value;
        return this;
    }
}
