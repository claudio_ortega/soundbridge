/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.station;
import com.pera.soundbridge.audio.filter.*;
import com.pera.soundbridge.vessel.*;
import org.apache.logging.log4j.*;

public class NetworkLoopBackRules implements IRoutingRules
{
    private final Logger logger = LogManager.getLogger();

    public static NetworkLoopBackRules create()
    {
        return new NetworkLoopBackRules();
    }

    @Override
    public ISignalFilter getFilterProbeToNet() {
        return v -> Vessel.NULL;
    }

    @Override
    public ISignalFilter getFilterLocalToNet() {
        return v -> Vessel.NULL;
    }

    @Override
    public ISignalFilter getFilterNetToLocal() {
        return v -> Vessel.NULL;
    }

    @Override
    public ISignalFilter getFilterNetToNet() {
        return v ->
        {
            if ( v.getType() == VesselType.Probe ) {
                logger.info("v:{}, {},{}", v::getOriginatorId, () -> v.getType().toString(), v::getSerialNumber);
            }
            return v;
        };
    }

    @Override
    public ISignalFilter getFilterLocalToLocal() {
        return v -> Vessel.NULL;
    }

    @Override
    public void close() {
    }
}
