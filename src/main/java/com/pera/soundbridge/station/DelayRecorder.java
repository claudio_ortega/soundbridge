/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018. 
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge.station;

import com.pera.soundbridge.vessel.*;
import org.apache.commons.lang3.tuple.*;
import org.apache.logging.log4j.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.util.function.*;

public class DelayRecorder implements IDelayRecorder {

    private final Logger logger = LogManager.getLogger();

    private static class TimeStampMeasurement {
        long serialNumber;
        long measuredTime;

        TimeStampMeasurement(long inSerialNumber, long inMeasuredTime) {
            serialNumber = inSerialNumber;
            measuredTime = inMeasuredTime;
        }
    }

    private final BlockingQueue<TimeStampMeasurement> aList;
    private final BlockingQueue<TimeStampMeasurement> bList;
    private final Consumer<Vessel> filterA;
    private final Consumer<Vessel> filterB;
    private final String recorderId;

    public interface TimeProducer {
        long getTime();
    }

    @Override
    public synchronized void reset() {
        aList.clear();
        bList.clear();
    }

    public static DelayRecorder create(
        final VesselType vesselType,
        String inRecorderId,
        TimeProducer timeProducer ) {
        return new DelayRecorder( vesselType, inRecorderId, timeProducer );
    }

    private DelayRecorder(
        final VesselType vesselType,
        String inRecorderId,
        TimeProducer timeProducer ) {

        recorderId = inRecorderId;

        final int QUEUE_MAX_LENGTH = 101;
        aList = new ArrayBlockingQueue<>(QUEUE_MAX_LENGTH);
        bList = new ArrayBlockingQueue<>(QUEUE_MAX_LENGTH);

        filterA = v -> {
            if ( v.getType() == vesselType && v.getOriginatorId().equals( recorderId ) )
            {
                synchronized ( aList ) {
                    logger.debug( "a - v.getSerialNumber():{}, v.isProbe():{}", v.getSerialNumber(), v.isProbe() );
                    aList.add( new TimeStampMeasurement( v.getSerialNumber(), timeProducer.getTime() ) );
                    if ( aList.size() > QUEUE_MAX_LENGTH-1 ) {
                        aList.poll();
                    }
                }
            }
        };

        filterB = v -> {
            if ( v.getType() == vesselType && v.getOriginatorId().equals( recorderId ) )
            {
                synchronized ( bList ) {
                    logger.debug( "b - v.getSerialNumber():{}, v.isProbe():{}", v.getSerialNumber(), v.isProbe() );
                    bList.add( new TimeStampMeasurement( v.getSerialNumber(), timeProducer.getTime() ) );
                    if ( bList.size() > QUEUE_MAX_LENGTH-1 ) {
                        bList.poll();
                    }
                }
            }
        };
    }

    @Override
    public String getId() {
        return recorderId;
    }

    public Consumer<Vessel> getFilterA() {
        return filterA;
    }

    public Consumer<Vessel> getFilterB() {
        return filterB;
    }

    // *ordered* list for delay -> vessel count on that delay (ms)
    @Override
    public synchronized Result compute() {

        final Result result = new Result();

        result.id = recorderId;

        // serial -> delay
        final Map<Long,Long> serialToReferenceTime = new HashMap<>();

        synchronized ( aList ) {
            aList.forEach( e -> serialToReferenceTime.put( e.serialNumber, e.measuredTime ) );
        }

        final List<Pair<Long,Long>> serialToDelayList = new LinkedList<>();

        synchronized ( bList ) {
            bList.forEach(
                e -> {
                    if (serialToReferenceTime.containsKey(e.serialNumber)) {
                        serialToDelayList.add(
                                new ImmutablePair<>(e.serialNumber, e.measuredTime - serialToReferenceTime.get(e.serialNumber)));
                        result.lastSerial = e.serialNumber;
                    }
                }
            );
        }

        // *map* of {delay (ms) -> vessel count with that delay}
        final Map<Long,Long> delayToVesselCountMap = new HashMap<>();

        serialToDelayList.forEach(
            serialToDelayEntry ->
            {
                final long delayInMsec = serialToDelayEntry.getValue();

                if ( ! delayToVesselCountMap.containsKey( delayInMsec ) )
                {
                    delayToVesselCountMap.put( delayInMsec, 1L );
                }
                else
                {
                    final long previousCount = delayToVesselCountMap.get( delayInMsec );
                    delayToVesselCountMap.put( delayInMsec, previousCount + 1L );
                }
            }
        );

        // *ordered* list of {delay in (ms)}
        final List<Long> delaysSorted = new LinkedList<>( delayToVesselCountMap.keySet() );
        delaysSorted.sort( Comparator.naturalOrder() );

        // *ordered* list of tuples {delay (ms) -> vessel count with that delay}
        final List<Pair<Long,Long>> orderedDelayToVesselCountList = new LinkedList<>();
        delaysSorted.forEach(
            delay -> orderedDelayToVesselCountList
                .add( new ImmutablePair<>( delay, delayToVesselCountMap.get( delay ) ) ) );

        result.delayFrequency = new LinkedList<>();

        final AtomicLong mean = new AtomicLong(0);
        final AtomicLong totalCount = new AtomicLong(0);

        orderedDelayToVesselCountList.forEach(
            pair -> {
                result.delayFrequency.add( new Result.DelayStat( pair.getLeft(), pair.getRight() ) );
                mean.addAndGet( pair.getLeft() * pair.getRight() );
                totalCount.addAndGet( pair.getRight() );
            }
        );

        result.totalCount = totalCount.get();

        if ( totalCount.get() > 0 )
        {
            result.meanDelayMSec = ( (float) mean.get()) / totalCount.get();
            double acPower = 0.0;
            for ( final Pair<Long,Long> pair : orderedDelayToVesselCountList )  {
                final double acPortion = ( ( pair.getLeft() - result.meanDelayMSec ) * ( pair.getLeft() - result.meanDelayMSec ) * pair.getRight() );
                acPower += acPortion;
            }
            result.stdDevDelayMSec = (float) Math.sqrt( acPower / result.totalCount );
        }
        else
        {
            result.meanDelayMSec = 0.0f;
            result.stdDevDelayMSec = 0.0f;
        }

        return result;
    }
}
