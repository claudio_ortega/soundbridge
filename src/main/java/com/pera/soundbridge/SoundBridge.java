/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.soundbridge;

import com.google.common.collect.ImmutableList;
import com.pera.soundbridge.audio.common.AudioCommon;
import com.pera.soundbridge.audio.common.DriversInfo;
import com.pera.soundbridge.audio.common.FSEnum;
import com.pera.soundbridge.audio.util.AudioLineUtils;
import com.pera.util.TaggedThreadFactory;
import com.pera.soundbridge.cmdline.SoundBridgeCommandLineParser;
import com.pera.soundbridge.cmdline.SoundBridgeStartupConfigInfo;
import com.pera.soundbridge.gpio.GPIOController;
import com.pera.soundbridge.gpio.GPIOPinCommandInfo;
import com.pera.soundbridge.gpio.IGPIOController;
import com.pera.soundbridge.gpio.NullGPIOController;
import com.pera.soundbridge.restinfo.SoundBridgeConfig;
import com.pera.soundbridge.restinfo.SoundBridgeStatus;
import com.pera.soundbridge.restinfo.VhfBcmIndexInfo;
import com.pera.restserver.RestServer;
import com.pera.restserver.RestServerBuilder;
import com.pera.soundbridge.station.*;
import com.pera.util.*;
import com.pera.soundbridge.vessel.VesselType;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spark.Request;
import spark.Response;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SoundBridge implements ISoundBridge
{
    private static final Logger logger = LogManager.getLogger();

    private final SoundBridgeStatus soundbridgeStatus;
    private final ThreadPoolExecutor executor;
    private volatile RestServer restServer;
    private volatile IStation station;
    private final StationFactory stationFactory;
    private final SoundBridgeConfig soundBridgeConfig;
    private volatile DriversInfo driversInfo;
    private volatile IGPIOController gpioController;
    private final CountDownLatch terminationWaitingLatch;
    private volatile SoundBridgeStartupConfigInfo startupConfig;
    private final IDelayRecorder delayRecorder;

    public static SoundBridge create() {
        return new SoundBridge();
    }

    private SoundBridge()
    {
        driversInfo = DriversInfo.create();
        soundBridgeConfig = SoundBridgeConfig.create();
        soundbridgeStatus = SoundBridgeStatus.create();
        terminationWaitingLatch = new CountDownLatch(1);
        executor = new ThreadPoolExecutor(
            10,
            10,
            5,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>( ),
            TaggedThreadFactory.create( getClass ( ), "rest-api" ) );
        stationFactory = StationFactory.create();
        startupConfig = SoundBridgeStartupConfigInfo.DEFAULT;
        delayRecorder = DelayRecorder.create(
            VesselType.Probe,
            UUID.randomUUID().toString().substring(0, 8),
            System::currentTimeMillis
        );
    }

    @Override
    public SoundBridgeConfig getSoundBridgeConfig() {
        return soundBridgeConfig;
    }

    @Override
    public final DriversInfo getDriversInfo() {
        return AudioCommon.fetchDriversInfoFromHardware(
            true,
            true,
            soundBridgeConfig );
    }

    @Override
    public List<FSEnum> getValidFS() {
        return ImmutableList.of (
            FSEnum.value8000,
            FSEnum.value16000,
            FSEnum.value32000,
            FSEnum.value44100,
            FSEnum.value48000,
            FSEnum.value96000 );
    }

    @Override
    public int start (
        boolean executingFromTestCase,
        boolean loadConfigOnStartup,
        boolean addShutDownHook,
        boolean startStationOnStartup,
        boolean displayEnvironmentAtStartup,
        boolean returnImmediately,
        String ... args  )
    {
        int exitCode = 0;

        try
        {
            final SoundBridgeCommandLineParser commandLineParser = SoundBridgeCommandLineParser.create();
            startupConfig = commandLineParser.getConfigFromArgs( args, SoundBridgeStartupConfigInfo.DEFAULT );

            if ( startupConfig.getHelpRequested() ) {
                commandLineParser.printHelp( System.out );
                System.out.println( "\nversion " + SystemUtil.getGitVersion ( true ) + "\n" );
                return 0;
            }

            if ( startupConfig.isVersionRequested() ) {
                System.out.println( "\nversion " + SystemUtil.getGitVersion ( true ) + "\n" );
                return 0;
            }

            // our test code configures log4j before executing the mainAlt(),
            // so then, we should skip the log configuration whenever coming from test code
            if ( ! executingFromTestCase ) {
                LogConfigurator.singleton().setLevel( startupConfig.getLogLevel() ) ;
            }

            soundbridgeStatus.setLogLevel( LogConfigurator.singleton().getLogLevel() );

            if ( displayEnvironmentAtStartup ) {
                LogUtil.logHeader( logger, args );
            }

            if ( ! SystemUtil.ensureWeHaveAnIPAddressForService( 60 ) ) {
                System.out.println( "there are no local IPs available" );
                return -1;
            }

            if (startupConfig.isCheckSiblings()) {
                final boolean noSiblingsWereFound = iCheckForSiblingApps();
                if ( noSiblingsWereFound ) {
                    System.out.println("no siblings were found");
                }
                return noSiblingsWereFound ? 0: -1;
            }

            boolean listAudioDriversAndQuit = false;
            if (startupConfig.isListAllAudioDrivers()) {
                soundBridgeConfig.setFastAudioDeviceTest(false);
                final DriversInfo info = AudioCommon.fetchDriversInfoFromHardware( false, false, soundBridgeConfig );
                AudioCommon.listAllDrivers(System.out, info);
                listAudioDriversAndQuit = true;
            }

            if (startupConfig.isListCompatibleAudioDrivers()) {
                {
                    soundBridgeConfig.setFastAudioDeviceTest(true);
                    System.out.println( "discovering compatible audio devices using no delay" );
                    final DriversInfo info = AudioCommon.fetchDriversInfoFromHardware( true, true, soundBridgeConfig );
                    AudioCommon.listCompatibleDrivers(true, true, System.out::println, info);
                    iDumpDriverInfoToFile(info);
                }
                {
                    soundBridgeConfig.setFastAudioDeviceTest(false);
                    System.out.println( "discovering compatible audio devices using some delay" );
                    final DriversInfo info = AudioCommon.fetchDriversInfoFromHardware( true, true, soundBridgeConfig );
                    AudioCommon.listCompatibleDrivers(true, true, System.out::println, info);
                    iDumpDriverInfoToFile(info);
                }

                listAudioDriversAndQuit = true;
            }

            if (listAudioDriversAndQuit) {
                return 0;
            }

            if ( loadConfigOnStartup ) {
                if (startupConfig.getConfigPath().isEmpty()) {
                    System.out.println("the config file was not specified");
                    return -1;
                }

                final File configFile = new File(startupConfig.getConfigPath());
                if (!configFile.exists()) {
                    System.out.println(String.format("the config file was not found at path: [%s], quitting.", configFile));
                    return -1;
                }

                soundBridgeConfig.loadFromPropertiesFile(configFile);
            }

            if ( soundBridgeConfig.getStationType() == StationBuilder.StationTypeEnum.vs ) {
                gpioController = GPIOController
                        .create()
                        .init( soundBridgeConfig.isInvertGPIOTTLLevelsOnVs() );
            }
            else {
                gpioController = NullGPIOController.create();
            }

            iInitAppStatus( args );

            if ( ! startupConfig.isUseRest() ) {
                logger.info ( "REST server was not requested" );
            }
            else {
                logger.info ( "REST server was requested" );

                final String restHost = SystemUtil.getAValidLocalIP(
                    startupConfig.getRestConfigHostNameOrIPAddress(),
                    true );

                final boolean isRESTSocketUsed = SystemUtil.canConnectOverTCP(
                    restHost,
                    startupConfig.getRestConfigPort(),
                    1_000 );

                if ( isRESTSocketUsed ) {
                    throw new IllegalStateException(
                        String.format(
                            "there is already a server socket opened on %s:%d REST server cannot be started",
                            restHost,
                            startupConfig.getRestConfigPort() ) );
                }

                iStartupRestServer(
                    startupConfig.getRestConfigHostNameOrIPAddress(),
                    startupConfig.getRestConfigPort()
                );

                iAttachRestAPIMappings();

                logger.info("rest server started on {}:{}, REST server started OK: {}",
                    restServer.getEffectiveIP(),
                    startupConfig.getRestConfigPort(),
                    restServer.isStarted() );
            }

            if ( startStationOnStartup ) {
                startStation();
            }

            if ( addShutDownHook ) {
                iAddShutdownHook();
            }

            if ( !returnImmediately ) {
                logger.info("station started, to stop use control-c or SIGINT");
                terminationWaitingLatch.await();
                logger.info("stopping application");
            }
        }

        catch ( ParseException e )
        {
            logger.debug ( "parsing of command line failed, " + e.getMessage(), e );
            logger.error ( "parsing of command line failed, " + e.getMessage() );
            exitCode = -1;
        }

        catch ( Throwable e )
        {
            logger.error ( e.getMessage (), e );
            exitCode = -1;
        }

        return exitCode;
    }

    @Override
    public DelayRecorder.Result computeDelayResults() {
        return delayRecorder.compute();
    }

    private void iStartupRestServer(String hostNameOrIPAddress, int port )
    {
        restServer = RestServer
            .create (
                RestServerBuilder
                    .create ()
                    .setHostNameOrIpAddress( hostNameOrIPAddress )
                    .setPort ( port )
            )
            .startUp();
    }

    enum RestResult { ok, error, notSupported, noop }

    private void iAttachRestAPIMappings()
    {
        switch ( soundBridgeConfig.getStationType() ) {
            case os:
                iAttachRestAPIMappingsOS();
                iAttachRestAPIDelayStats();
                iAttachRestAPIBasicMappings();
                break;

            case vs:
                iAttachRestAPIMappingsVS();
                iAttachRestAPIBasicMappings();
                break;

            case full_duplex:
                iAttachRestAPIDelayStats();
                iAttachRestAPIBasicMappings();
                break;

            default:
                logger.warn("no REST API is defined for this station type: {}", soundBridgeConfig.getStationType() );
                break;
        }
    }

    private void iAttachRestAPIMappingsVS()
    {
        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/set-gpio-pin-state",
            ( Request aInRequest, Response aInResponse ) ->
            {
                logger.debug ( "aInRequest.pathInfo(): [{}]", aInRequest.pathInfo() );
                logger.debug ( "aInRequest.body (): [{}]", aInRequest.body () );

                final GPIOPinCommandInfo gpioPinCommandInfo = GsonUtil.deSerialize (
                    aInRequest.body (),
                    GPIOPinCommandInfo.class );

                final GPIOController.PinOpResult result = gpioController.setPinState(
                    gpioPinCommandInfo.getBcmIndex(),
                    gpioPinCommandInfo.isCommandedState());

                aInResponse.status( 200 );

                return result;
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/set-vhf-bcm-index",
            ( Request aInRequest, Response aInResponse ) ->
            {
                logger.debug ( "aInRequest.pathInfo(): [{}]", aInRequest.pathInfo() );
                logger.debug ( "aInRequest.body (): [{}]", aInRequest.body () );

                final VhfBcmIndexInfo vhfBcmInfo = GsonUtil.deSerialize (
                    aInRequest.body (),
                    VhfBcmIndexInfo.class );

                final String ret;

                if ( ! gpioController.validateBcmIndex( vhfBcmInfo.getBcmIndex() ) ) {
                    ret = RestResult.error.toString();
                }

                else if ( vhfBcmInfo.getAction() == VhfBcmIndexInfo.Action.add ) {
                    soundbridgeStatus.putInVhfBcmIndexMap( vhfBcmInfo.getBcmIndex(), true );
                    ret = RestResult.ok.toString();
                }

                else if ( vhfBcmInfo.getAction() == VhfBcmIndexInfo.Action.remove ) {
                    soundbridgeStatus.putInVhfBcmIndexMap( vhfBcmInfo.getBcmIndex(), false );
                    ret = RestResult.ok.toString();
                }

                else {
                    ret = RestResult.noop.toString();
                }

                aInResponse.status( 200 );

                return ret;
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.get,
            "/get-line-delay-stats",
            ( Request aInRequest, Response aInResponse ) ->
            {
                aInResponse.status( 404 );
                return RestResult.notSupported.toString();
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/ptt-on",
            ( Request aInRequest, Response aInResponse ) ->
            {
                aInResponse.status( 404 );
                return RestResult.notSupported.toString();
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/ptt-off",
            ( Request aInRequest, Response aInResponse ) ->
            {
                aInResponse.status( 404 );
                return RestResult.notSupported.toString();
            }
        );
    }

    private void iAttachRestAPIDelayStats() {
        restServer.attachMapping (
            RestServer.RESTVerbEnum.get,
            "/get-line-delay-stats",
            ( Request aInRequest, Response aInResponse ) ->
            {
                aInResponse.status( 200 );
                return GsonUtil.serializeAll( delayRecorder.compute() );
            }
        );
    }

    private void iAttachRestAPIMappingsOS()
    {
        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/set-gpio-pin-state",
            ( Request aInRequest, Response aInResponse ) ->
            {
                try {
                    aInResponse.status(404);
                    return RestResult.notSupported.toString();
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/set-vhf-bcm-index",
            ( Request aInRequest, Response aInResponse ) ->
            {
                try {
                    aInResponse.status(404);
                    return RestResult.notSupported.toString();
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/ptt-on",
            ( Request aInRequest, Response aInResponse ) ->
            {
                try {
                    soundbridgeStatus.setPttIsOn(true);
                    aInResponse.status(200);
                    return RestResult.ok.toString();
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/ptt-off",
            ( Request aInRequest, Response aInResponse ) ->
            {
                try {
                    soundbridgeStatus.setPttIsOn(false);
                    aInResponse.status(200);
                    return RestResult.ok.toString();
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );
    }

    private void iAttachRestAPIBasicMappings()
    {
        restServer.attachMapping (
            RestServer.RESTVerbEnum.get,
            "/get-latest-status",
            ( Request aInRequest, Response aInResponse ) ->
            {
                try {
                    final Map<String, Number> counters = new HashMap<>();

                    if (station.isOperational()) {
                        station.getCounters(counters);
                    }

                    aInResponse.status(200);

                    return GsonUtil.serialize(
                        soundbridgeStatus
                            .setStationIsOperational(station.isOperational())
                            .setCounters(counters)
                            .setCurrentTime(System.currentTimeMillis() ) );
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/get-audio-drivers-info",
            ( Request aInRequest, Response aInResponse ) ->
            {
                try {
                    aInResponse.status(200);
                    executor.submit(
                        () -> {
                        logger.info("fetching audio driver information, this will take a few seconds...");
                        driversInfo = AudioCommon.fetchDriversInfoFromHardware( true, true, soundBridgeConfig );
                        AudioCommon.listCompatibleDrivers( true, true, logger::info, driversInfo );
                        logger.info("done fetching audio driver information.");
                    } );
                    return RestResult.ok.toString();
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.get,
            "/get-latest-config",
            ( Request aInRequest, Response aInResponse ) ->
            {
                try {
                    aInResponse.status(200);
                    return GsonUtil.serialize(soundBridgeConfig);
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/set-log-level",
            ( Request aInRequest, Response aInResponse ) ->
            {
                try {
                    final String newLogLevel = aInRequest.body().trim().toLowerCase();
                    soundbridgeStatus.setLogLevel(newLogLevel);
                    LogConfigurator.singleton().setLevel(newLogLevel);

                    aInResponse.status(200);

                    return RestResult.ok.toString();
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/stop-station",
            ( Request aInRequest, Response aInResponse ) ->
            {
                try {
                    executor.submit(
                        () ->
                        {
                            try {
                                stopStation();
                            } catch (Exception ex) {
                                logger.error(ex.getMessage(), ex);
                            }
                        }
                    );

                    aInResponse.status(200);

                    return RestResult.ok.toString();
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/start-station",
            ( Request aInRequest, Response aInResponse ) ->
            {
                try {
                    executor.submit(
                        () ->
                        {
                            try {
                                startStation();
                            } catch (Exception ex) {
                                logger.error(ex.getMessage(), ex);
                            }
                        }
                    );

                    aInResponse.status(200);

                    return RestResult.ok.toString();
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );

        restServer.attachMapping (
            RestServer.RESTVerbEnum.put,
            "/shutdown-app",
            ( Request aInRequest, Response aInResponse ) ->
            {
                try {
                    executor.submit(
                        () ->
                        {
                            try {
                                stop();
                            } catch (Exception ex) {
                                logger.error(ex.getMessage(), ex);
                            }
                        }
                    );

                    aInResponse.status(200);

                    return RestResult.ok.toString();
                }
                catch ( Throwable x ) {
                    logger.error( x.getMessage(), x );
                    aInResponse.status(404);
                    return RestResult.error.toString();
                }
            }
        );
    }

    @Override
    public void startStation() throws Exception
    {
        if ( station != null && station.isOperational() ) {
            logger.info("station is already up, ignoring the start");
        }
        else {
            delayRecorder.reset();

            final StationBuilder stationBuilder = StationBuilder
                .create()
                .setDelayRecorder( delayRecorder )
                .setSoundBridgeConfig(soundBridgeConfig);

            if ( soundBridgeConfig.getStationType() == StationBuilder.StationTypeEnum.os ) {
                stationBuilder.setPttCommandSupplier(soundbridgeStatus::getPttIsOn);
            }

            if ( soundBridgeConfig.getStationType() == StationBuilder.StationTypeEnum.vs ) {
                stationBuilder.setPttStateConsumer(
                    pttState ->
                    {
                        // called back only from the VS about every 50mSec
                        soundbridgeStatus.getVhfBcmIndexMap().forEach(
                            (bcmPin, shouldEnable) ->
                            {
                                final GPIOController.PinOpResult pinOpResult = gpioController.changePinState( bcmPin.intValue(), pttState && shouldEnable );
                                if ( pinOpResult == GPIOController.PinOpResult.fail ) {
                                    logger.warn("ptt on pin:{} failed", bcmPin );
                                }
                            }
                        );
                    }
                );
            }

            logger.info( "creating station of type {}", stationBuilder.getSoundBridgeConfig().getStationType() );

            station = stationFactory.createStation( stationBuilder );
            station.init();
            station.start();
            soundbridgeStatus.setLastStationStartTime( System.currentTimeMillis() );
            soundbridgeStatus.setStationIsOperational( station.isOperational() );
        }
    }

    @Override
    public void stopStation() throws Exception
    {
        if ( station == null || !station.isOperational() ) {
            logger.info("station is already down, ignoring the stop");
        }
        else {
            final Map<String, Number> counters = new HashMap<>();
            station.getCounters(counters);
            logger.debug("Main App - counters ---- BEGIN");
            AudioLineUtils.printCounters(
                 counters,
                (Map.Entry<String,Number> x) -> logger.debug( "    {}:{}", () -> x.getKey(), () -> x.getValue() )
            );
            logger.debug("Main App - counters ---- END");

            station.stop();
            station.close();
            soundbridgeStatus.setLastStationStopTime( System.currentTimeMillis() );
        }
    }

    private void iStopApp() throws Exception
    {
        stationFactory.close();

        if (restServer != null)
        {
            restServer.shutdown();
            restServer.waitForTermination(3, TimeUnit.SECONDS);
            restServer = null;
        }

        if ( gpioController != null ) {
            gpioController.close();
        }

        executor.shutdownNow();
    }

    private void iAddShutdownHook()
    {
        logger.debug ( "iAddShutdownHook - BEGIN" );

        final Thread terminationThread = new Thread (
            () ->
            {
                try
                {
                    logger.debug ( "shutdown hook fired - BEGIN" );
                    stop();
                    logger.debug ( "shutdown hook fired - END" );
                }

                catch ( Exception ex )
                {
                    logger.error ( ex.getMessage(), ex );
                }
            }
        );

        // we want this thread to -prevent- the JVM of exiting,
        // so this should not be a daemon thread, then
        terminationThread.setDaemon ( false );

        Runtime.getRuntime ().addShutdownHook ( terminationThread );

        logger.debug ( "iAddShutdownHook - END" );
    }

    @Override
    public void stop() throws Exception {
        stopStation();
        iStopApp();
        terminationWaitingLatch.countDown();
    }

    private static void iDumpDriverInfoToFile(
        DriversInfo outDriversInfo )
            throws FileNotFoundException, UnsupportedEncodingException
    {
        try ( final PrintStream fileOut = FileUtil.createPrintStreamFromFile(
            new File ( "./log/audio-hw.txt" ),
            false ) )
        {
            AudioCommon.listCompatibleDrivers( true, true, fileOut::println, outDriversInfo );
        }
    }

    private boolean iCheckForSiblingApps()
    {
        final String thisJVMPid = SystemUtil.getPIDForThisProcess( "" );
        logger.debug( "this process id: {}", thisJVMPid );

        final Set<Integer> otherJVMPids = CollectionsUtil.getSetAMinusSetB(
            SystemUtil.getJavaVMsMatching( getClass().getCanonicalName() ),
            CollectionsUtil.createSet( Integer.valueOf(thisJVMPid ) ) );

        if ( otherJVMPids.size() > 0 )
        {
            logger.warn( String.format("siblings apps are started in these other process(es): %s", otherJVMPids));
        }
        else
        {
            logger.info( "no siblings were found" );
        }

        return otherJVMPids.size() == 0;
    }

    private void iInitAppStatus( String[] args ) throws UnknownHostException {
        soundbridgeStatus
            .setSoundBridgeConfig(soundBridgeConfig)
            .setAppStartTime( System.currentTimeMillis() )
            .setAppVersion( SystemUtil.getGitVersion ( true ) )
            .setOsName( System.getProperty ( "os.name" ) )
            .setOsVersion( System.getProperty ( "os.version" ) )
            .setOsArch( System.getProperty ( "os.arch" ) )
            .setNumberOfProcessors( Runtime.getRuntime().availableProcessors() )
            .setCmdLineArgs( String.join( ", ", Arrays.asList( args ) ) )
            .setProcessId( SystemUtil.getPIDForThisProcess("n/a") )
            .setCurrentDirectory( SystemUtil.getCurrentDirectory().getAbsolutePath() )
            .setJavaArgs( String.join( ", ", SystemUtil.getJavaExtraArguments ( ) ) )
            .setJavaHome( SystemUtil.getEnvironmentOrSysProperty ( "java.home" ) )
            .setJavaRuntimeVersion( SystemUtil.getEnvironmentOrSysProperty ( "java.runtime.version" ) )
            .setPttIsOn( false );
    }
}
