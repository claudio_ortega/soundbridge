package com.pera.license;

import com.google.gson.annotations.Expose;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LicenseInfo implements ILicenseInfo {
    @Expose private final String version;
    @Expose private Integer licenseNumber;
    @Expose private String userFirstName;
    @Expose private String userLastName;
    @Expose private String userEmail;
    @Expose private boolean unlimited;
    @Expose private String expirationDate;
    @Expose private String digest;

    static public LicenseInfo create(
        Integer licenseNumber,
        String userFirstName,
        String userLastName,
        String userEmail,
        boolean unlimited,
        String expirationDate ) throws NoSuchAlgorithmException {

        return new LicenseInfo(
            licenseNumber,
            userFirstName,
            userLastName,
            userEmail,
            unlimited,
            expirationDate,
            sha1(licenseNumber, userFirstName, userLastName, userEmail, unlimited, expirationDate) );
    }

    private static String sha1 (
        Integer licenseNumber,
        String userFirstName,
        String userLastName,
        String userEmail,
        boolean unlimited,
        String expirationDate ) throws NoSuchAlgorithmException {

        final String s1 = String.format(
            "%d-%s-%s-%s-%b-%s-(Lclazz)LicenseInfo.class",
            licenseNumber,
            userFirstName,
            userLastName,
            userEmail,
            unlimited,
            expirationDate );

        final MessageDigest digest = MessageDigest.getInstance("SHA-1");
        digest.reset();
        digest.update(s1.getBytes(StandardCharsets.UTF_8));
        return String.format("%040x", new BigInteger(1, digest.digest()));
    }

    public boolean isLegitimate( ) throws NoSuchAlgorithmException {
        return sha1( licenseNumber, userFirstName, userLastName, userEmail, unlimited, expirationDate ).equals(digest);
    }

    public boolean isExpired ( ) {
        if ( unlimited ) {
            return false;
        }
        final LocalDate expirationDate = LocalDate.parse(this.expirationDate, DateTimeFormatter.ISO_LOCAL_DATE );
        return expirationDate.isBefore( LocalDate.now() );
    }

    public String displayString() throws NoSuchAlgorithmException {

        if ( !isLegitimate() ) {
            return "invalid license";
        }

        if ( unlimited ) {
            return String.format( "%s %s on a perpetual license", userFirstName, userLastName );
        }

        return String.format( "%s %s on licensed valid until %s", userFirstName, userLastName, expirationDate );
    }

    @Override
    public String getUserCompleteName() {
        return String.format( "%s %s", userFirstName, userLastName );
    }

    @Override
    public Integer getLicenseNumber() {
        return licenseNumber;
    }

    private LicenseInfo(
        Integer licenseNumber,
        String userFirstName,
        String userLastName,
        String userEmail,
        boolean unlimited,
        String expirationDate,
        String digest) {
        this.version = "1.0.0";
        this.licenseNumber = licenseNumber;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userEmail = userEmail;
        this.unlimited = unlimited;
        this.expirationDate = expirationDate;
        this.digest = digest;
    }

    public String getVersion() {
        return version;
    }

    public LicenseInfo setLicenseNumber(Integer licenseNumber) {
        this.licenseNumber = licenseNumber;
        return this;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public LicenseInfo setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
        return this;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public LicenseInfo setUserLastName(String userLastName) {
        this.userLastName = userLastName;
        return this;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public LicenseInfo setUserEmail(String userEmail) {
        this.userEmail = userEmail;
        return this;
    }

    public boolean isUnlimited() {
        return unlimited;
    }

    public LicenseInfo setUnlimited(boolean unlimited) {
        this.unlimited = unlimited;
        return this;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public LicenseInfo setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public String getDigest() {
        return digest;
    }

    public LicenseInfo setDigest(String digest) {
        this.digest = digest;
        return this;
    }
}

