package com.pera.license;

import java.security.NoSuchAlgorithmException;

public interface ILicenseInfo {
    boolean isLegitimate( ) throws NoSuchAlgorithmException;
    boolean isExpired ( );
    String displayString() throws NoSuchAlgorithmException;
    String getUserCompleteName();
    Integer getLicenseNumber();
}

