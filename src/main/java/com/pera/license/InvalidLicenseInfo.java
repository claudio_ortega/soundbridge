package com.pera.license;

import java.security.NoSuchAlgorithmException;

public class InvalidLicenseInfo implements  ILicenseInfo{
    public boolean isLegitimate( ) throws NoSuchAlgorithmException {
        return false;
    }

    public boolean isExpired ( ){
        return false;
    }

    public String displayString() throws NoSuchAlgorithmException {
        return "invalid license";
    }

    @Override
    public Integer getLicenseNumber() {
        return null;
    }

    @Override
    public String getUserCompleteName() {
        return "invalid user";
    }
}

