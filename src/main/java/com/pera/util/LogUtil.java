
/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.net.InetAddress;
import java.util.Arrays;

public class LogUtil
{
    private LogUtil()
    {
    }

    public static void initLog4j() {
        // this is tricky
        // LogConfigurator.singleton().init() should happen before the first Logger instantiation
        LogConfigurator.singleton().init();
        LogManager.getLogger();
    }

    public static void logHeader(
        Logger logger,
        String... args )
    {
        logger.info( "app version:                 [" + SystemUtil.getGitVersion ( true ) + "]" );
        logger.info( "OS name:                     [" + System.getProperty ( "os.name" ) + "]" );
        logger.info( "OS version:                  [" + System.getProperty ( "os.version" ) + "]" );
        logger.info( "OS arch:                     [" + System.getProperty ( "os.arch" ) + "]" );
        logger.info( "number of processors:        [" + Runtime.getRuntime().availableProcessors() + "]" );
        logger.info( "os type:                     [" + SystemUtil.getOSType () + "]");
        logger.info( "process id:                  [" + SystemUtil.getPIDForThisProcess("n/a") + "]");
        logger.info( "current directory:           [" + SystemUtil.getCurrentDirectory() + "]");
        logger.info( "command line arguments:      [" + Arrays.asList( args ) + "]");
        logger.info( "extra java arguments:        [" + SystemUtil.getJavaExtraArguments () + "]");
        logger.info( "log level:                   [" + LogConfigurator.singleton().getLogLevel() + "]");
        logger.info( "java args:                   [" + SystemUtil.getJavaExtraArguments ( ) + "]");
        logger.info( "java home:                   [" + SystemUtil.getEnvironmentOrSysProperty ( "java.home" ) + "]");
        logger.info( "java runtime version:        [" + SystemUtil.getEnvironmentOrSysProperty ( "java.runtime.version" ) + "]");
        logger.info( "java vm name:                [" + SystemUtil.getEnvironmentOrSysProperty ( "java.vm.name" ) + "]");
        logger.info( "java vm info:                [" + SystemUtil.getEnvironmentOrSysProperty ( "java.vm.info" ) + "]");
        logger.info( "java vm spec version:        [" + SystemUtil.getEnvironmentOrSysProperty ( "java.vm.specification.version" ) + "]");
        logger.info( "java vm spec vendor:         [" + SystemUtil.getEnvironmentOrSysProperty ( "java.vm.specification.vendor" ) + "]");
    }
}
