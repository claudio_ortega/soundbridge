/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import com.google.common.base.Preconditions;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class TaggedThreadFactory implements ThreadFactory
{
    private final AtomicInteger threadNumber = new AtomicInteger(1);
    private final String prefix;

    public static TaggedThreadFactory create (Class inClass, String inTag )
    {
        return new TaggedThreadFactory( inClass, inTag );
    }

    private TaggedThreadFactory(Class inClass, String inTag )
    {
        Preconditions.checkNotNull(inClass);
        Preconditions.checkNotNull(inTag);
        Preconditions.checkArgument(!inTag.isEmpty());
        prefix = String.format( "%s-%s", inClass.getSimpleName(), inTag );
    }

    @Override
    public Thread newThread( Runnable aInRunnable )
    {
        Preconditions.checkNotNull(aInRunnable);
        return new Thread( aInRunnable, prefix + "-" + threadNumber.getAndIncrement() );
    }
}
