/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;


import org.apache.logging.log4j.*;
import org.apache.logging.log4j.core.appender.*;
import org.apache.logging.log4j.core.config.*;
import org.apache.logging.log4j.core.config.builder.api.*;
import org.apache.logging.log4j.core.config.builder.impl.*;

public class LogConfigurator
{
    private static LogConfigurator singleton;

    private Level level;

    public static synchronized LogConfigurator singleton ()
    {
        if ( singleton == null )
        {
            singleton = new LogConfigurator();
        }

        return singleton;
    }

    private LogConfigurator()
    {
    }

    // IMPORTANT !!!! this has to happen BEFORE ANY call to LogManager.getLogger();
    public void init()
    {
        iInit();
    }

    public void setLevel( String logLevel )
    {
        level = Level.valueOf( logLevel);
        Configurator.setLevel( "com.pera", level );
    }

    public synchronized String getLogLevel()
    {
        return level.toString();
    }

    private static void iInit()
    {
        final ConfigurationBuilder<BuiltConfiguration> builder =
            ConfigurationBuilderFactory.newConfigurationBuilder()
                .setStatusLevel(Level.ERROR)
                .setConfigurationName("BuilderTest");

        {
            // console appender
            final AppenderComponentBuilder consoleAppenderBuilder = builder
                .newAppender("Stdout", "CONSOLE")
                .addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT)
                .add(
                    builder
                        .newLayout("PatternLayout")
                        .addAttribute("pattern", "[%-5level] %d{ISO8601} [%c:%L] [%t] - [%m]%n"));

            builder.add(consoleAppenderBuilder);

            builder.add(
                builder.newLogger("com.pera", Level.ERROR)
                    .add(builder.newAppenderRef("Stdout"))
                    .addAttribute("additivity", true));
        }

        {
            // file appender
            final AppenderComponentBuilder fileAppenderBuilder = builder
                .newAppender("rolling", "RollingFile")
                .addAttribute("fileName", "log/log4j.log")
                .addAttribute("filePattern", "log/archive/rolling-%d{MM-dd-yy-hh-mm-ss}-%i.log.gz")
                .add(
                    builder
                        .newLayout("PatternLayout")
                        .addAttribute("pattern", "[%-5level] %d{ISO8601} [%c:%L] [%t] - [%m]%n"))

                .addComponent(
                    builder
                        .newComponent("Policies")
                        .addComponent(
                            builder.newComponent("SizeBasedTriggeringPolicy")
                                .addAttribute("size", "10 MB")
                        ))

                .addComponent(builder.newComponent("DefaultRolloverStrategy").addAttribute("max", "5"));


            builder.add(fileAppenderBuilder);

            builder.add(
                builder.newLogger("com.pera", Level.ERROR)
                    .add(builder.newAppenderRef("rolling"))
                    .addAttribute("additivity", true));
        }

        Configurator.initialize(builder.build());
    }
}
