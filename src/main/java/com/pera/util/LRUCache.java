/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import com.google.common.base.*;
import java.util.*;

public class LRUCache
{
    private final int maxSize;
    private final Map<String, CachedItem> map;
    private int lastSequence;

    private static class CachedItem {
        private final String value;
        private final int sequence;
        private CachedItem(int s, String v ) {
            value = v;
            sequence = s;
        }
    }

    public LRUCache( int inMaxSize )
    {
        lastSequence = 0;
        maxSize = inMaxSize;
        map = new HashMap<>();
    }

    public void set ( String key, String newValue ) {
        if ( map.containsKey( key ) ) {
            map.put( key, new CachedItem( lastSequence, newValue ) );
        }
        else {
            lastSequence++;
            if ( map.size() < maxSize) {
                map.put( key, new CachedItem( lastSequence, newValue ) );
            } else {
                map.remove( findOldest() );
                map.put( key, new CachedItem( lastSequence, newValue ) );
            }
        }
    }

    private String findOldest() {

        int minimum = 0;
        boolean minimumIsSet = false;
        Map.Entry<String, CachedItem> minimumEntry = null;

        for (Map.Entry<String, CachedItem> entry : map.entrySet() ) {
            if ( entry.getValue().sequence < minimum || ! minimumIsSet )
            {
                minimum = entry.getValue().sequence;
                minimumIsSet = true;
                minimumEntry = entry;
            }
        }

        Preconditions.checkNotNull( minimumEntry, "wrong algorithm, FIXME!!" );

        return minimumEntry.getKey();
    }


    public String get ( String key ) {
        if ( map.containsKey( key ) ) {
            return map.get( key ).value;
        }
        else {
            return null;
        }
    }

    public void delete ( String key ) {
        map.remove( key );
    }
}
