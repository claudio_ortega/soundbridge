

/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import com.google.common.base.Preconditions;
import com.google.common.collect.*;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

@SuppressWarnings( "ResultOfMethodCallIgnored" )
public class StringUtil
{
    // you shall not instantiate this class
    private StringUtil() {}

    /**
     *
     * @param aInUnderTest
     * @param aInReference
     * @return  true is null or empty
     */
    public static boolean isNullOrIsEmptyOrIsEqualTo(
            String aInUnderTest,
            String aInReference )
    {
        return aInUnderTest == null || aInUnderTest.isEmpty() || aInUnderTest.equals( aInReference  );
    }

    public static String convertNullIntoEmpty(String aInString)
    {
        return (aInString == null) ? "" : aInString;
    }

    /**
     *
     * @param testString
     * @return  true is null or empty
     */
    public static boolean isNullOrEmpty( String testString )
    {
        return testString == null || testString.isEmpty();
    }

    /**
     * not identical to String.split(), it will ignore repetitions of separator, split() does not
     * @param aInStringToSplit "a b      c"
     * @return {"a", "b", "c"}
     */
    public static List<String> splitOverSpaces( String aInStringToSplit )
    {
        return splitOverString( aInStringToSplit, " ", true );
    }

    public static List<String> splitOverString(
            String aInStringToSplit,
            String aInSeparator )
    {
        return splitOverString( aInStringToSplit, aInSeparator, false );
    }

    public static List<String> splitOverString(
            String aInStringToSplit,
            String aInSeparator,
            boolean aInEliminateEmpties )
    {
        List<String> lTmp = CollectionsUtil.createList(
                aInStringToSplit.split(
                        aInSeparator
                                .replace( "\\", "\\\\" )
                                .replace( "*", "\\*" )
                                .replace( ".", "\\." ) ) );         // add more as needed, but run regression

        List<String> lReturn;

        if ( aInEliminateEmpties )
        {
            lReturn = CollectionsUtil.transformList (
                lTmp,
                aInS -> aInS.isEmpty() ? null : aInS
            );
        }
        else
        {
            lReturn = lTmp;
        }

        return lReturn;
    }

    public static List<String> toLowerCase( final Collection<String> aIn )
    {
        return CollectionsUtil.transformList(
            CollectionsUtil.createList ( aIn ),
            aInS -> aInS.toLowerCase ()
            );
    }

    /**
     * Counts the number of occurrences of substring within a string.
     * @param aInLine the string to look in to
     * @param aInSubString the substring to look for
     * @return the number of occurrences of the substring in the given string
     */
    public static int countOccurrencesOfSubstring(
            final String aInLine,
            final String aInSubString )
    {
        int lCount = 0;
        int lIndex = 0;

        while ((lIndex = aInLine.indexOf(aInSubString, lIndex)) != -1)
        {
            ++lCount;
            ++lIndex;
        }

        return lCount;
    }

    public static String concatenate(
        boolean aInMakeEmptyStringsVisible,
        boolean aInIncludeEmpties,
        String aInSeparator,
        List<String> aInList )
    {
        return concatenate( aInList, "", aInSeparator, aInMakeEmptyStringsVisible, aInIncludeEmpties );
    }

    public static String concatenate(
        boolean aInMakeEmptyStringsVisible,
        boolean aInIncludeEmpties,
        String aInSeparator,
        String ... aInStrings )
    {
        return concatenate( CollectionsUtil.createList( aInStrings ), "", aInSeparator, aInMakeEmptyStringsVisible, aInIncludeEmpties  );
    }

    public static String concatenate( String aInSeparator, List<String> aInList )
    {
        return concatenate( aInList, "", aInSeparator, true, true );
    }

    public static String concatenate( String aInSeparator, String ... aInStrings )
    {
        return concatenate( CollectionsUtil.createList( aInStrings ), "", aInSeparator, true, true );
    }

    public static String concatenate(
            List<String> aInList,
            String aInStringToPrependToEveryItem,
            String aInSeparator,
            boolean aInMakeEmptyStringsVisible,
            boolean aInIncludeEmpties )
    {
        StringBuilder sb = new StringBuilder();

        int lCount = 0;

        for ( String lNext: aInList )
        {
            if ( ( lCount > 0 ) && ( lCount < aInList.size() ) )
            {
                sb.append( aInSeparator );
            }

            sb.append(aInStringToPrependToEveryItem);

            if ( null == lNext  )
            {
                sb.append( "(null)" );
            }
            else if ( lNext.isEmpty() )
            {
                if ( aInIncludeEmpties )
                {
                    sb.append ( aInMakeEmptyStringsVisible ? "(empty)" : "" );
                }
            }
            else
            {
                sb.append( lNext );
            }

            lCount++;
        }

        return sb.toString();
    }

    public static List<String> trim( List<String> aInStringToTrim )
    {
        final List<String> lRet = new LinkedList<>();

        for ( String lNext : aInStringToTrim )
        {
            lRet.add( lNext.trim() );
        }

        return lRet;
    }

    public static List<String> trimListFromEmpties( List<String> aInStringToTrim )
    {
        final List<String> lRet = new LinkedList<>();

        for ( String lNext : aInStringToTrim )
        {
            if ( ! lNext.isEmpty () )
            {
                lRet.add( lNext.trim() );
            }
        }

        return lRet;
    }

    public static boolean isOneOf( String aInTest, List<String> aInStringListToCompare )
    {
        boolean lRet = false;

        for ( String lNext : aInStringListToCompare )
        {
            if ( lNext.equals( aInTest ))
            {
                lRet = true;
                break;
            }
        }

        return lRet;
    }

    public static boolean regexMatchesOneOf( String aInTestRegex, List<String> aInStringListToCompare )
    {
        boolean lRet = false;

        for ( String lNext : aInStringListToCompare )
        {
            if ( lNext.matches( aInTestRegex ) )
            {
                lRet = true;
                break;
            }
        }

        return lRet;
    }

    public static boolean containsRegexChars( String in )
    {
        return in.contains( "*" ) || in.contains ( "?" ) || in.contains ( "+" ) || in.contains ( "." ) || in.contains ( "|" ) || in.contains ( "&" );
    }

    @SuppressWarnings( "ResultOfMethodCallIgnored" )
    public static boolean isPossiblyRegex(String in)
    {
        boolean lRet;

        try
        {
            Pattern.compile(in);
            lRet = true;
        }
        catch( PatternSyntaxException e )
        {
            lRet = false;
        }

        return lRet;
    }

    public static String makeTokenStringReadable( String  in )
    {
        return in.replaceAll( "\\n" , "\\\\n" ).replaceAll( "\\t" , "\\\\t" ).replaceAll( "\\r", "\\\\\r" ).replaceAll( " ", "." );
    }

    public static String byteArrayToString(
            byte[] aInByteArr,
            String aInDelimiter,
            boolean aInUseHex)
    {
        final StringBuilder sb = new StringBuilder();

        for ( byte nextByte : aInByteArr )
        {
            final String lStr;

            if ( aInUseHex )
            {
                lStr = getRightPortionOfString ( "0" + Integer.toHexString( (int) nextByte ), 2);
            }

            else
            {
                lStr = Integer.toString ( (int) nextByte );
            }

            sb.append( aInDelimiter ).append( lStr );
        }

        return sb.toString();
    }

    public static boolean isPossiblyAnInteger( String aInTest )
    {
        boolean lRet;

        try
        {
            //noinspection ResultOfMethodCallIgnored
            Integer.parseInt( aInTest );
            lRet = true;
        }

        catch ( NumberFormatException ex )
        {
            lRet = false;
        }

        return lRet;
    }


    /**
     * changes from this:  .*(01)[WP-ER10
     *         to this:    .*\\(01\\)\\[WP\\-ER10
     * @param aInString
     * @return
     */
    public static String transformSpecialCharsInRegexp(String aInString)
    {
        String lRet = null;

        if ( aInString != null )
        {
            lRet = aInString
                    .replace( "{", "\\{" )
                    .replace( "}", "\\}" )
                    .replace( "(", "\\(" )
                    .replace( ")", "\\)" )
                    .replace( "[", "\\[" )
                    .replace( "]", "\\]" )
                    .replace( "+", "\\+" )
                    .replace( "-", "\\-" );
        }

        return lRet;
    }

    private static final Comparator<String> TRIVIAL_STRING_COMPARATOR = ( aInA, aInB ) -> ( aInA.compareTo( aInB ) );

    private static final Comparator<String> FLOAT_COMPARATOR = ( aInA, aInB ) -> {
        int lReturn;

        try
        {
            Float lA = new Float( aInA );
            Float lB = new Float( aInB );

            Float lMaxAbs = Math.max( Math.abs( lA ), Math.abs( lB ) );
            Float lDiffAbs = Math.abs( lA - lB );

            final float RATIO = 0.001f;   // 0.1 %
            if ( lDiffAbs <= ( lMaxAbs * RATIO ) )
            {
                lReturn = 0;
            }
            else
            {
                lReturn = ( lA > lB ) ? 1 : -1;
            }
        }
        catch ( NumberFormatException ex )
        {
            lReturn = aInA.compareTo( aInB );
        }

        return lReturn;
    };

    public static boolean equalsNullSafe(
            String aInFirst,
            String aInSec )
    {
        return equalsNullSafe(
                aInFirst,
                aInSec,
                TRIVIAL_STRING_COMPARATOR,
                FLOAT_COMPARATOR );
    }

    private static boolean isPossiblyAFloat(String aInString)
    {
        return   // make this better
                countOccurrenceOfChar( aInString, '/' ) == 0 &&
                        countOccurrenceOfChar( aInString, '.' ) == 1;
    }

    private static boolean equalsNullSafe(
            String aInFirst,
            String aInSecond,
            Comparator<String> aInStringComparator,
            Comparator<String> aInFloatComparator )
    {
        boolean lRet;

        if ( null == aInFirst && null == aInSecond )
        {
            lRet = true;
        }

        else if ( null != aInFirst && null != aInSecond )
        {
            if ( isPossiblyAFloat( aInFirst ) && isPossiblyAFloat( aInSecond ) )
            {
                lRet = ( 0 == aInFloatComparator.compare( aInFirst, aInSecond ) );
            }
            else
            {
                lRet = ( 0 == aInStringComparator.compare( aInFirst, aInSecond ) );
            }
        }

        else
        {
            lRet = false;
        }

        return lRet;
    }

    /**
     * @param aInDecimalOrHex
     * @return
     */
    public static long parseLongFromHexString(
            String aInDecimalOrHex,
            int aInRadixFor0xSuffix,
            int aInRadixForNon0xSuffix )
    {
        aInDecimalOrHex = aInDecimalOrHex.trim();

        long ret;

        if ( aInDecimalOrHex.startsWith( "0x" ) )
        {
            ret = Long.parseLong( aInDecimalOrHex.substring(2), aInRadixFor0xSuffix );
        }
        else
        {
            ret = Long.parseLong( aInDecimalOrHex, aInRadixForNon0xSuffix );
        }

        return ret;
    }

    public static boolean isAnHexNumber ( String aInStringToTest )
    {
        boolean lRet = false;

        if (aInStringToTest.startsWith( "0x" ))
        {
            try
            {
                Integer.parseInt( aInStringToTest.substring(2), 16 );
                lRet = true;
            }
            catch ( NumberFormatException ex )
            {
                // we ignore this, there is no other way to avoid the try/catch approach!
            }
        }
        return lRet;
    }

    public static boolean isANumber ( String aInStringToTest )
    {
        boolean lRet = false;

        try
        {
            Integer.parseInt( aInStringToTest );
            lRet = true;
        }
        catch ( NumberFormatException ex )
        {
            // we ignore this, there is no other way to avoid the try/catch approach!
        }

        return lRet;
    }

    public static String stringifyListOfLists( List<List<String>> aInListOfLists )
    {
        StringBuilder lSb = new StringBuilder(  );

        lSb.append( "{" );

        int i=0;
        int size = aInListOfLists.size();

        for  ( List<String> lNext : aInListOfLists )
        {
            lSb.append( "{" ).append( concatenate( ",", lNext ) ).append( "}" );

            if ( i < (size-1) )
            {
                lSb.append( ", " );
            }

            i++;
        }

        lSb.append( "}" );

        return lSb.toString();
    }

    /**
     *
     * @param aInA
     * @param aInB
     * @return          -1 if all the same
     *                  otherwise the first line that differs, zero based
     */
    public static int firstIndexThatDiffers (
            List<List<String>> aInA,
            List<List<String>> aInB )
    {
        int lRet = -1;

        int lMin = Math.min( aInA.size(), aInB.size() );

        for ( int i=0; i<lMin; i++  )
        {
            if ( ! concatenate( " ", aInA.get( i ) ).equals( concatenate( " ", aInB.get( i ) ) ) )
            {
                lRet = i;
                break;
            }
        }

        if ( lRet == -1 )
        {
            if ( aInA.size() > lMin || aInB.size() > lMin )
            {
                lRet = lMin;
            }
        }

        return lRet;
    }

    /**
     * will add "" around a the passed in string if (1) has spaces anywhere and (2) it does not have already the "" around
     * @param aInString
     * @return
     */
    public static String quoteStringIfHasSpacesAndIsNotQuoted( String aInString )
    {
        final String DOUBLE_QUOTE_STRING = "\"";

        if ( ! ( aInString.startsWith( DOUBLE_QUOTE_STRING ) && aInString.endsWith( DOUBLE_QUOTE_STRING ) )
                && aInString.contains(" ") )
        {
            final char DOUBLE_QUOTE_CHAR = '"';
            aInString = DOUBLE_QUOTE_CHAR + aInString.trim() + DOUBLE_QUOTE_CHAR;
        }

        return aInString;
    }

    /**
     * Counts the number of occurrences of a character within a string.
     * @param aInString the string to look in to
     * @param aInTest the character to look for
     * @return the number of occurrences of a character in the given string
     */
    public static int countOccurrenceOfChar(
            String aInString,
            char aInTest)
    {
        int lRet = 0;

        int lIndex = aInString.indexOf( aInTest );

        if ( lIndex >= 0 )
        {
            for ( int i = lIndex; i < aInString.length(); i++ )
            {
                if ( aInString.charAt( i ) == aInTest )
                {
                    lRet++;
                }
            }
        }

        return lRet;
    }

    public static String getStringAfter ( String aInBigString, String aInSubString )
    {
        int index = aInBigString.indexOf( aInSubString );

        if ( index == -1 )
        {
            return null;
        }

        else
        {
            return aInBigString.substring( index + aInSubString.length() );
        }
    }

    public static String stripString(
            String aInLongString,
            List<String> aInShortStringOrRegexpList )
    {
        String lRet = aInLongString;

        for ( String lNextStringOrRegexp : aInShortStringOrRegexpList )
        {
            lRet = stripString(
                    lRet,
                    lNextStringOrRegexp );
        }

        return lRet;
    }

    public static String stripString(
            String aInLongString,
            String aInShortStringOrRegexp )
    {
        return aInLongString.replaceAll( aInShortStringOrRegexp, "" );
    }

    public static String trimOneDownFromPrefixAndSuffix ( boolean aInFailIfNotPrefixedOrSuffixed, String aInLongString, String aInTriedPrefixAndSuffix )
    {
        if ( aInLongString.startsWith( aInTriedPrefixAndSuffix ) != aInLongString.endsWith( aInTriedPrefixAndSuffix ) )
        {
            if ( aInFailIfNotPrefixedOrSuffixed )
            {
                throw new RuntimeException("unbalanced prefix/suffix: [" + aInTriedPrefixAndSuffix+ "], on this string: [" + aInLongString + "]");
            }
        }

        return trimOneDownFromPrefixAndSuffix ( aInFailIfNotPrefixedOrSuffixed, aInLongString, aInTriedPrefixAndSuffix, aInTriedPrefixAndSuffix );
    }

    /**
     *
     * @param aInThrowExceptionIfErrorFound
     * @param aInLongString
     * @param aInPrefix
     * @param aInSuffix
     * @return  null in case of error or exception, depending on input param  aInFailIfNotPrefixedOrSuffixed
     */
    public static String trimOneDownFromPrefixAndSuffix (
            boolean aInThrowExceptionIfErrorFound,
            String aInLongString,
            String aInPrefix,
            String aInSuffix )
    {
        if ( aInPrefix.length() != 1 )
        {
            throw new RuntimeException("prefix length non supported");
        }

        if ( aInSuffix.length() != 1 )
        {
            throw new RuntimeException("suffix length non supported");
        }

        boolean lDoNotSubstring = false;

        if ( aInLongString.length() < 2 )
        {
            if ( aInThrowExceptionIfErrorFound )
            {
                throw new RuntimeException("invalid string [" + aInLongString + "], is not surrounded with: [" + aInPrefix + "," + aInSuffix + "]");
            }
            lDoNotSubstring = true;
        }

        if ( ! aInLongString.startsWith( aInPrefix ) )
        {
            if ( aInThrowExceptionIfErrorFound )
            {
                throw new RuntimeException("invalid string [" + aInLongString + "], does not start with: [" + aInPrefix + "]");
            }
            lDoNotSubstring = true;

        }

        if ( ! aInLongString.endsWith( aInSuffix ) )
        {
            if ( aInThrowExceptionIfErrorFound )
            {
                throw new RuntimeException("invalid string [" + aInLongString + "], does not end with: [" + aInSuffix + "]");
            }
            lDoNotSubstring = true;
        }

        return lDoNotSubstring
                ? aInLongString
                : aInLongString.substring(
                aInPrefix.length(),
                aInLongString.length() - aInSuffix.length() );
    }

    public static String createInstanceOfString( char initValue, int repeatedTimes )
    {
        StringBuilder buff = new StringBuilder();

        for (int i = 0; i < repeatedTimes; i++)
        {
            buff.append(initValue);
        }

        return buff.toString();
    }

    public static String getRightPortionOfString( String aInInput, int aInLen )
    {
        String ret;

        if (aInLen >= aInInput.length())
        {
            ret = aInInput;
        }
        else
        {
            ret = aInInput.substring(aInInput.length() - aInLen);
        }

        return ret;
    }

    // in:   "12.3?78*9"
    // out:  "12\.3.78.*9"
    public static String convertRegexpFromShellToPerl( String aInString )
    {
        String ret = null;

        if (null != aInString)
        {
            // "." -> "\."
            String replacedFirstStage = aInString.replaceAll("\\.", "\\\\.");

            // "*" -> "\.*"
            String replacedSecondStage = replacedFirstStage.replaceAll("\\*", "\\.\\*");

            // "?" -> "\."
            ret = replacedSecondStage.replaceAll("\\?", "\\.");
        }

        return ret;
    }

    public static boolean containsWhiteSpace( final String aInString )
    {
        boolean retVal = false;

        for ( int i = 0; i < aInString.length(); i++ )
        {
            if ( Character.isWhitespace( aInString.codePointAt( i ) ) )
            {
                retVal = true;
                break;
            }
        }

        return retVal;
    }

    public static int getNonReadableCharsCount( final String aInString )
    {
        int lNonReadableCharCount = 0;

        for ( int i = 0; i < aInString.length(); i++ )
        {
            int lValue = aInString.codePointAt( i );

            //noinspection StatementWithEmptyBody
            if ( ( lValue == '\t' ) || // tab
                    ( lValue >= ' ' && lValue <= '~' ) // anything else readable falls in here
                    )
            {
                // this is considered a readable char
            }
            else
            {
                lNonReadableCharCount++;
            }
        }

        return lNonReadableCharCount;
    }

    public static String getLongestMatchFromAnySide( String... aInStrings )
    {
        Preconditions.checkArgument(aInStrings.length > 1);

        String lTmp = aInStrings[0];

        for ( int i=1; i< aInStrings.length; i++ )
        {
            lTmp = getLongestMatchOnABFromAnySide( lTmp, aInStrings[ i ] );
        }

        return lTmp;
    }

    private static String getLongestMatchOnABFromAnySide(
            String aInA,
            String aInB)
    {
        String lRet;

        int lMatchFromTheLeft = getLongerIntersectionLength( aInA, aInB, Side.left );
        int lMatchFromTheRight = getLongerIntersectionLength( aInA, aInB, Side.right );

        if ( lMatchFromTheLeft > lMatchFromTheRight )
        {
            lRet = aInA.substring( 0, lMatchFromTheLeft );
        }
        else
        {
            lRet = aInA.substring( aInA.length() - lMatchFromTheRight );
        }

        return lRet;
    }

    public static List<String> getLimitedLines( String in, int widthLimit )
    {
        final List<String> retList = Lists.newArrayList();

        if ( in.length() > widthLimit )
        {
            final String[] words = in.split(" ");

            final StringBuilder sb = new StringBuilder();

            for ( final String word : words )
            {
                if ( sb.length() + word.length() > widthLimit )
                {
                    retList.add( sb.toString() );
                    sb.setLength(0);
                }

                if ( sb.length() > 0 )
                {
                    sb.append( " " );
                }

                sb.append( word );
            }

            retList.add( sb.toString() );
        }
        else
        {
            retList.add( in );
        }

        return retList;
    }

    private enum Side { left, right }

    private static int getLongerIntersectionLength ( String aInA, String aInB, Side aInSide )
    {
        int lRet;

        if ( aInSide == Side.right )
        {
            lRet = getLongerIntersectionLength(
                    new StringBuffer( aInA ).reverse().toString(),
                    new StringBuffer( aInB ).reverse().toString(),
                    Side.left );
        }
        else
        {
            int lLastMatchingIndex = -1;

            for( int i = 0; i < Math.min( aInA.length(), aInB.length() ); i++ )
            {
                if ( aInA.charAt( i ) == aInB.charAt( i ) )
                {
                    lLastMatchingIndex = i;
                }
                else
                {
                    break;
                }
            }

            lRet = lLastMatchingIndex + 1;
        }

        return lRet;
    }

    public static String replaceSpecialChars( String aInString )
    {
        return aInString.replaceAll( "\t", " ").replaceAll("\"","").replaceAll("\'","`");
    }

    public static List<Integer> getAllIndicesOf(
            String aInStringContainer,
            String aInToFind )
    {
        final List<Integer> lReturn = new LinkedList<>();

        int lNextIndex;

        while ( true )
        {
            lNextIndex = aInStringContainer.indexOf( aInToFind );

            if( lNextIndex == -1 )
            {
                break;
            }
            else
            {
                lReturn.add( lNextIndex );
            }
        }

        return lReturn;
    }

    /**
     *
     * @param aInLookUp      : "May 16 19:25:34 INFO ( default/core/BGP ): BGP peers usage: 1/150"
     * @param aInChar        : ' '
     * @param aInCountOne    : 2
     * @param aInCountTwo    : 3
     * @return               : "INFO"
     */
    public static String getStringInBetweenChars(
            String aInLookUp,
            char aInChar,
            int aInCountOne,
            int aInCountTwo )
    {
        int lIndexOne = -1;
        int lIndexTwo = -1;
        int lMatchCount = 0;

        for ( int i=0; i<aInLookUp.length (); i++ )
        {
            if ( aInLookUp.charAt ( i ) == aInChar )
            {
                if ( lMatchCount == aInCountOne )
                {
                    lIndexOne = i;
                }

                if ( lMatchCount == aInCountTwo )
                {
                    lIndexTwo = i;
                }

                lMatchCount++;
            }
        }

        return aInLookUp.substring ( lIndexOne + 1, lIndexTwo );
    }

    public static String removeLastOccurrenceOfSubstring ( String aInString, String aInSub )
    {
        final int lIndex = aInString.lastIndexOf ( aInSub );

        final String lRet;

        if ( lIndex == -1 )
        {
            lRet = aInString;
        }
        else
        {
            lRet = aInString.substring ( 0, aInString.length () - aInSub.length () );
        }

        return lRet;
    }
}

