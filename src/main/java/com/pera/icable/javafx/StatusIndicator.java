/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 */

package com.pera.icable.javafx;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class StatusIndicator {

    public enum State { up, down, unknown }

    private static final Color COLOR_UP = Color.rgb( 0x16, 0xa7, 0x37, 1.0 );
    private static final Color COLOR_DOWN = Color.rgb( 0xd1, 0x34, 0x30, 1.0 );
    private static final Color COLOR_UNKNOWN_TOGGLE_ON = COLOR_DOWN;
    private static final Color COLOR_UNKNOWN_TOGGLE_OFF = COLOR_DOWN.deriveColor(0,1,1,0.5); // lesser opacity
    private final AtomicInteger toggle;
    private volatile State state;
    private final Label label;
    private final CornerRadii cornerRadii;

    private StatusIndicator() {

        state = State.unknown;
        toggle = new AtomicInteger(0);
        cornerRadii = new CornerRadii(4);
        label = new Label();

        new ScheduledThreadPoolExecutor( 1 ).scheduleAtFixedRate (
            ( ) -> Platform.runLater( this::iUpdateStatus ),
            0,
            333,
            TimeUnit.MILLISECONDS
        );
    }

    public static StatusIndicator create( Runnable onMousePressed ) {
        return new StatusIndicator();
    }

    public StatusIndicator setOnMousePressed ( Runnable onMousePressed ) {
        label.setOnMousePressed( ev -> {
            if ( state != State.unknown ) {
                onMousePressed.run();
            }
        } );
        return this;
    }

    public Label getLabel() {
        return label;
    }

    public void setState ( State in ) {
        state = in;
    }

    private void iUpdateStatus ( )
    {
        if ( state == State.up ) {
            label.setTextFill(Color.WHITE);
            label.setBackground(new Background(new BackgroundFill(COLOR_UP, cornerRadii, Insets.EMPTY)));
            label.setText(" En servicio ");
        }
        else if ( state == State.down ) {
            label.setTextFill(Color.WHITE);
            label.setBackground(new Background(new BackgroundFill(COLOR_DOWN, cornerRadii, Insets.EMPTY)));
            label.setText(" Fuera de servicio ");
        }
        else if ( state == State.unknown ) {
            label.setTextFill(Color.WHITE);
            toggle.set( toggle.incrementAndGet() % 3 );
            label.setBackground(new Background(new BackgroundFill(toggle.get() == 0 ? COLOR_UNKNOWN_TOGGLE_ON : COLOR_UNKNOWN_TOGGLE_OFF, cornerRadii, Insets.EMPTY)));
            label.setText(" Desconectada ");
        }
    }
}