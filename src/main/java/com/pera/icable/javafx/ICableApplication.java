/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 */

package com.pera.icable.javafx;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.gson.annotations.Expose;
import com.pera.license.ILicenseInfo;
import com.pera.license.InvalidLicenseInfo;
import com.pera.license.LicenseInfo;
import com.pera.icable.cmdline.ICableCommandLineParser;
import com.pera.icable.cmdline.ICableStartupConfigInfo;
import com.pera.icable.main.ICableConfig;
import com.pera.registry.restinfo.SubscriberRegistrationInfo;
import com.pera.registry.restinfo.SubscriberRegistrationInfoMap;
import com.pera.registry.restinfo.SubscribersQueryInfo;
import com.pera.soundbridge.ISoundBridge;
import com.pera.soundbridge.SoundBridge;
import com.pera.soundbridge.audio.common.AudioCommon;
import com.pera.soundbridge.audio.common.DriversInfo;
import com.pera.soundbridge.audio.common.FSEnum;
import com.pera.soundbridge.restinfo.SoundBridgeConfig;
import com.pera.soundbridge.station.DelayRecorder;
import com.pera.soundbridge.station.StationBuilder;
import com.pera.util.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.lang.System;

public class ICableApplication extends Application {

    private static final Logger logger = LogManager.getLogger();

    private final ThreadPoolExecutor executor;
    private final Controls controls;
    private final ISoundBridge soundBridge;
    private final AtomicBoolean soundBridgeConnected;
    private final AtomicBoolean registerUserOnTimer;
    private volatile long latestSoundBridgeConnectionTime;
    private volatile ICableConfig config;
    private volatile ILicenseInfo licenseInfo;
    private final long startTime;
    private long lastCheckedTime;
    private Stage stage;
    // we need this hack so we are able to pass this into the back end instance
    private static ICableStartupConfigInfo startupConfig;

    private static final File configFilePath = new File ("./config.json");
    private static final File licenseFilePath = new File( "./license.json" );

    // the registry
    private static final String REGISTRY_URL = "linode.1.poplarlabs.net:7777";

    // buttons
    private static final String START_SESSION = "Start Session";
    private static final String MAKE_CONNECTION = "Make connection";
    private static final String DISCONNECT = "Disconnect";
    private static final String RELOAD_GROUP = "Reload";
    private static final String RELOAD_INPUT = "Reload";
    private static final String IMPORT_license_KEY = "Import";

    // status' strings
    private static final String SELECTION_DISCOVERY_PENDING = "[audio discovery pending]";
    private static final String SELECTION_DISCOVERY_STARTED = "[audio discovery started...]";
    private static final String SELECTION_DONE = "[audio discovery done]";
    private static final String NOT_STARTED = "[not started]";
    private static final String PENDING = "[pending]";
    private static final String CONNECTED = "[connected OK]";
    private static final String CONNECTING = "[connecting...]";
    private static final String DISCONNECTING = "[disconnecting...]";
    private static final String NOT_CONNECTED = "[disconnected]";
    private static final String CONNECTION_ERROR = "[connection error]";
    private static final String REGISTERED = "[session %s]";
    private static final String SESSION_NOT_STARTED = "[session not started]";
    private static final String GROUP_FOUND = "[found]";
    private static final String GROUP_NOT_FOUND = "[not found]";
    private static final String LICENSE_KEY_IS_INVALID = "license is invalid";
    private static final String LICENSE_KEY_EXPIRED = "license has expired";
    private static final String LICENSE_KEY_OK = "[license OK]";
    private static final String REGISTRAR_REACHABLE = "[server reachable]";
    private static final String REGISTRAR_UNREACHABLE = "[server unreachable]";

    private static class ComboMixerInfo {
        @Expose final AudioCommon.MixerInfo mixerInfo;

        public ComboMixerInfo(AudioCommon.MixerInfo mixerInfo) {
            this.mixerInfo = mixerInfo;
        }

        @Override
        public String toString() {
            final String tmp = mixerInfo.description.split( ":" )[1];
            return mixerInfo.hash + " - " + tmp.split( "," )[0].trim() + "" + tmp.split( "," )[1].trim();
        }
    }

    private static class ComboSubscriberRegistrationInfo {
        @Expose final SubscriberRegistrationInfo subscriberRegistrationInfo;

        public ComboSubscriberRegistrationInfo(SubscriberRegistrationInfo subscriberRegistrationInfo) {
            this.subscriberRegistrationInfo = subscriberRegistrationInfo;
        }

        @Override
        public String toString() {
            return
                String.format(
                    "%s - (session %s) - %dHz/%dms",
                    subscriberRegistrationInfo.getClientName(),
                    subscriberRegistrationInfo.getClientId().substring(0, 6),
                    subscriberRegistrationInfo.getAudioSamplingFrequency().getValue(),
                    subscriberRegistrationInfo.getBufferLengthInMSec()
                );
        }
    }

    private static class Controls {
        private final Button registerUserButton = new Button();
        private final Button licenseKeyButton = new Button();
        private final Button reloadGroupButton = new Button();
        private final Button reFetchAudioSourcesButton = new Button();
        private final Button connectButton = new Button();
        private final TextField registrarTextField = new TextField();
        private final TextField groupTextField = new TextField();
        private final TextField licenseTextField = new TextField();
        private final Label registrarStatus = new Label();
        private final Label registrationStatus = new Label();
        private final Label groupFoundStatus = new Label();
        private final Label audioSourceStatus = new Label();
        private final Label connectionStatus = new Label();
        private final Label connectionTime = new Label();
        private final Label licenseStatus = new Label();
        private final ComboBox<ComboSubscriberRegistrationInfo> groupCombo = new ComboBox<>();
        private final ComboBox<ComboMixerInfo> inputAudioCombo = new ComboBox<>();
        private final ComboBox<ComboMixerInfo> outputAudioCombo = new ComboBox<>();
        private final ComboBox<FSEnum> samplingFrequencyCombo = new ComboBox<>();
        private final ComboBox<Integer> windowLengthCombo = new ComboBox<>();
        private final CheckBox useLocalAddressesCheckBox = new CheckBox();
        private final CheckBox fastAudioDeviceDiscoveryCheckBox = new CheckBox();
    }

    public static int main(boolean executingFromTestCase, String... args) {
        try {
            final ICableCommandLineParser commandLineParser = ICableCommandLineParser.create();

            try {
                startupConfig = commandLineParser.getConfigFromArgs(args, ICableStartupConfigInfo.DEFAULT);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return 1;
            }

            if (startupConfig.getHelpRequested()) {
                commandLineParser.printHelp(System.out);
                System.out.println("\nversion " + SystemUtil.getGitVersion(true) + "\n");
                return 0;
            }

            if (startupConfig.isVersionRequested()) {
                System.out.println("\nversion " + SystemUtil.getGitVersion(true) + "\n");
                return 0;
            }

            if ( ! SystemUtil.ensureWeHaveAnIPAddressForService( 60 ) ) {
                System.out.println( "there are no local IPs available" );
                return -1;
            }

            // our test code configures log4j before executing the mainAlt(),
            // so then, we should skip the log configuration whenever coming from test code
            if (!executingFromTestCase) {
                LogConfigurator.singleton().setLevel(startupConfig.getLogLevel());
            }

            LogUtil.logHeader( logger, args );

            // blocking call
            launch();

            return 0;
        } catch (Exception e) {
            logger.error( e.getMessage(), e );
            return 1;
        }
    }

    private static long gracePeriodInMsec = 300_000;

    private long iGetSecondsLeft () {
        return ( gracePeriodInMsec - ( System.currentTimeMillis() - startTime ) ) / 1000;
    }

    private boolean iWasSystimeMovedBack() {
        final long now = System.currentTimeMillis();
        final boolean altered = now < lastCheckedTime;
        lastCheckedTime = now;
        return altered;
    }

    // keep the default constructor as public, javafx needs it apparently..
    public ICableApplication() {
        licenseInfo = new InvalidLicenseInfo();
        startTime = System.currentTimeMillis();
        lastCheckedTime = startTime;
        latestSoundBridgeConnectionTime = -1;
        controls = new Controls();
        soundBridge = SoundBridge.create();
        soundBridgeConnected = new AtomicBoolean(false);
        registerUserOnTimer = new AtomicBoolean(false);

        executor = new ThreadPoolExecutor(
            10,
            10,
            0,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>() );

        // registration task
        new ScheduledThreadPoolExecutor(1).scheduleAtFixedRate(
            () -> {
                if ( registerUserOnTimer.get() ) {
                    Platform.runLater(this::iRegisterMyself);
                }
            },
            0,
            5,
            TimeUnit.MINUTES
        );

        // license check
        new ScheduledThreadPoolExecutor(1).scheduleAtFixedRate(
            () -> {
                try {
                    Platform.runLater(
                        () -> {
                            try {

                                controls.licenseTextField.setText( licenseInfo.displayString() );

                                if ( !licenseInfo.isLegitimate() ) {
                                    controls.licenseStatus.setText( String.format( "[%s, %d seconds left]", LICENSE_KEY_IS_INVALID, iGetSecondsLeft() ) );
                                }
                                else if ( licenseInfo.isExpired() ) {
                                    controls.licenseStatus.setText( String.format( "[%s, %d seconds left]", LICENSE_KEY_EXPIRED, iGetSecondsLeft() ) );
                                }
                                else {
                                    controls.licenseStatus.setText(LICENSE_KEY_OK);
                                }

                                if ( iWasSystimeMovedBack() ) {
                                    Platform.exit();
                                }

                                if ( ( !licenseInfo.isLegitimate() || licenseInfo.isExpired() ) && iGetSecondsLeft() < 0 ) {
                                    Platform.exit();
                                }
                            }

                            catch ( Throwable t ) {
                                logger.warn( t.getMessage(), t );
                            }
                        }
                    );
                }
                catch ( Throwable t ) {
                    logger.warn( t.getMessage(), t );
                }
            },
            0,
            1,
            TimeUnit.SECONDS
        );

        // display of connection time task
        new ScheduledThreadPoolExecutor(1).scheduleAtFixedRate(
            () -> {
                if ( soundBridgeConnected.get() ) {
                    Platform.runLater(
                        () ->
                        controls.connectionTime.setText( DurationFormatUtils.formatDuration(
                            System.currentTimeMillis() - latestSoundBridgeConnectionTime,
                            "[HH:mm:ss]",
                            true ) ) );
               }
            },
            0,
            1,
            TimeUnit.SECONDS
        );

        // delay stats
        new ScheduledThreadPoolExecutor(1).scheduleAtFixedRate(
            () -> {
                if ( soundBridgeConnected.get() ) {

                    Platform.runLater(
                        () -> {
                            final DelayRecorder.Result computeDelayResults = this.soundBridge.computeDelayResults();
                            logger.debug("delay stats.mean:{}", GsonUtil.serialize( computeDelayResults.meanDelayMSec ) );

                            if ( computeDelayResults.totalCount == 0 ) {
                                controls.connectionStatus.setText( "no delay stats yet" );
                            }

                            else {
                                final ComboSubscriberRegistrationInfo subscriberRegistrationInfo = controls.groupCombo.getSelectionModel().getSelectedItem();
                                final Integer windowLength = controls.windowLengthCombo.getSelectionModel().getSelectedItem();

                                if ( subscriberRegistrationInfo != null && windowLength != null && latestSoundBridgeConnectionTime > 0 ) {
                                    logger.debug("window size this side:{}", windowLength );
                                    logger.debug("window size other side:{}", GsonUtil.serialize( subscriberRegistrationInfo.subscriberRegistrationInfo.getBufferLengthInMSec() ) );
                                    controls.connectionStatus.setText( String.format(
                                        "[total delay %.0f/%.0fms]",
                                        ( 0.5 * computeDelayResults.meanDelayMSec ) + ( windowLength + subscriberRegistrationInfo.subscriberRegistrationInfo.getBufferLengthInMSec() ),
                                          0.5 * computeDelayResults.stdDevDelayMSec )
                                    );
                                }
                            }
                        }
                    );
                }
            },
            0,
            15,
            TimeUnit.SECONDS
        );
    }

    @Override
    public void start( Stage primaryStage ) throws Exception {

        stage = primaryStage;

        iLoadLicenseKeyFromCurrentDirectory();
        iLoadConfigFromPersistence();
        iInitializeControlTooltips();
        iInitializeControlsFromConfig();

        final GridPane gridPane = iCreatePane();

        {
            final Scene scene = new Scene( gridPane );
            primaryStage.setScene(scene);
            primaryStage.setTitle("ICable - the internet long audio connector" + " -- " + SystemUtil.getGitVersion ( true ) ) ;
            primaryStage.setResizable(false);
            primaryStage.show();

            executor.submit(
                () -> {
                    Platform.runLater( () -> controls.registrarTextField.setDisable( true ) );
                    Platform.runLater( () -> controls.licenseTextField.setDisable( true ) );
                    Platform.runLater( controls.connectButton::requestFocus );

                    SystemUtil.sleepMsec( 500 );
                    Platform.runLater( this::iConnectToRegistry );

                    SystemUtil.sleepMsec( 500 );
                    Platform.runLater( this::iRegisterMyself );

                    SystemUtil.sleepMsec( 500 );
                    Platform.runLater( this::iReloadGroup);

                    SystemUtil.sleepMsec( 500 );
                    Platform.runLater(this::iLoadAudioDriversInfo);

                    iAttachListenersToControls();
                }
            );
        }
    }

    @Override
    public void stop() throws Exception {
        logger.info("quitting the app");
        executor.shutdown();
        soundBridge.stop();

        iUnRegisterMyself();
        iLoadConfigFromControls();
        iSaveConfig();

        super.stop();
    }

    private void iLoadConfigFromControls() {
        config.getSoundBridgeConfig().setAudioSamplingFrequency(controls.samplingFrequencyCombo.getValue());
        config.getSoundBridgeConfig().setBufferLengthInMSec(controls.windowLengthCombo.getValue());
        config.getSoundBridgeConfig().setOtherSideIP("");
        config.getSoundBridgeConfig().setThisSideIP("");

        if ( controls.inputAudioCombo.getSelectionModel().getSelectedItem() == null )  {
            config.getSoundBridgeConfig().setAudioSourceHash("");
        }
        else {
            config.getSoundBridgeConfig().setAudioSourceHash( controls.inputAudioCombo.getSelectionModel().getSelectedItem().mixerInfo.hash );
        }

        if ( controls.outputAudioCombo.getSelectionModel().getSelectedItem() == null )  {
            config.getSoundBridgeConfig().setAudioSinkHash("");
        }
        else {
            config.getSoundBridgeConfig().setAudioSinkHash( controls.outputAudioCombo.getSelectionModel().getSelectedItem().mixerInfo.hash );
        }

        config.getSoundBridgeConfig().setFastAudioDeviceTest( controls.fastAudioDeviceDiscoveryCheckBox.isSelected() );
        config.setConnectIntoLocalAddresses( controls.useLocalAddressesCheckBox.isSelected() );
        config.setGroupName( controls.groupTextField.getText() );

        if ( controls.groupCombo.getSelectionModel().getSelectedItem() == null )  {
            config.setFriendId("");
        }
        else {
            config.setFriendId( controls.groupCombo.getSelectionModel().getSelectedItem().subscriberRegistrationInfo.getClientId() );
        }
    }

    private void iLoadConfigFromPersistence() throws Exception {
        if ( !configFilePath.exists() ) {
            logger.info( "the config file was not found at path:{}, starting with default values", configFilePath.getCanonicalPath() );
            config = ICableConfig.create()
                .setClientId( UUID.randomUUID().toString() )
                .setRegistryHostNameAndPort( REGISTRY_URL )
                .setGroupName("")
                .setConnectIntoLocalAddresses( false )
                .setSoundBridgeConfig( SoundBridgeConfig.create()
                    .setAudioSamplingFrequency( FSEnum.value44100 )
                    .setBufferLengthInMSec( 50 )
                    .setFastAudioDeviceTest( true )
                );
        }
        else {
            config = GsonUtil.readJsonFromFile(configFilePath, ICableConfig.class );
        }

        logger.info( "config at startup:{}", GsonUtil.serialize( config ) );
    }

    private void iSaveConfig() throws Exception {
        GsonUtil.writeJsonToFile( configFilePath, config );
    }

    private void iLoadLicenseKey() {
        try {
            if ( soundBridgeConnected.get() ) {
                logger.info( "reloading license ignored" );
                return;
            }

            final FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open license Key");
            fileChooser.setInitialDirectory( new File(".") );
            final File chosenLicenseFilePath = fileChooser.showOpenDialog( stage );
            if ( chosenLicenseFilePath != null ) {
                final String licenseKeyAsString = new String(FileUtil.getFileAsBytes(chosenLicenseFilePath));
                licenseInfo = GsonUtil.deSerialize( licenseKeyAsString, LicenseInfo.class );
                FileUtil.writeLinesIntoFile( ImmutableList.of ( licenseKeyAsString ), licenseFilePath );
            }
        }
        catch ( Throwable t ) {
            logger.error( t.getMessage(), t );
        }
    }

    private void iLoadLicenseKeyFromCurrentDirectory() {
        try {
            if ( !licenseFilePath.canRead() ) {
                logger.info("the license key file was not found at: {}", licenseFilePath);
            }
            else {
                final String licenseKey = new String(FileUtil.getFileAsBytes(licenseFilePath) );
                licenseInfo = GsonUtil.deSerialize( licenseKey, LicenseInfo.class );
            }
        }
        catch ( Throwable t ) {
            logger.info("the license key was not loaded");
        }
    }

    private void iLoadAudioDriversInfo() {

        if ( soundBridgeConnected.get() ) {
            logger.info( "load drivers action ignored" );
            return;
        }

        final boolean fastAudioDeviceDiscovery = controls.fastAudioDeviceDiscoveryCheckBox.isSelected();
        controls.audioSourceStatus.setText( SELECTION_DISCOVERY_STARTED );
        
        executor.submit(
            () -> {
                try {
                    soundBridge.getSoundBridgeConfig().setFastAudioDeviceTest( fastAudioDeviceDiscovery );

                    final DriversInfo driversInfo = soundBridge.getDriversInfo();
                    logger.info( "obtained driversInfo:{}", GsonUtil.serialize( driversInfo ) );

                    Platform.runLater(
                        () ->
                        {
                            // source section
                            {
                                controls.inputAudioCombo.getItems().clear();
                                int indexToSelect = -1;
                                int index = 0;
                                for (AudioCommon.MixerInfo m : driversInfo.getCompatibleSourceDescription()) {
                                    controls.inputAudioCombo.getItems().add(new ComboMixerInfo(m));
                                    if (m.hash.equals(config.getSoundBridgeConfig().getAudioSourceHash())) {
                                        indexToSelect = index;
                                    }
                                    index++;
                                }

                                if (indexToSelect != -1) {
                                    controls.inputAudioCombo.getSelectionModel().select(indexToSelect);
                                }
                                else if ( controls.inputAudioCombo.getItems().size() > 0 ) {
                                    controls.inputAudioCombo.getSelectionModel().select(0);
                                }
                            }

                            // sink section
                            {
                                controls.outputAudioCombo.getItems().clear();
                                int indexToSelect = -1;
                                int index = 0;
                                for (AudioCommon.MixerInfo m : driversInfo.getCompatibleSinkDescription()) {
                                    controls.outputAudioCombo.getItems().add(new ComboMixerInfo(m));
                                    if (m.hash.equals(config.getSoundBridgeConfig().getAudioSinkHash())) {
                                        indexToSelect = index;
                                    }
                                    index++;
                                }

                                if (indexToSelect != -1) {
                                    controls.outputAudioCombo.getSelectionModel().select(indexToSelect);
                                }
                                else if ( controls.outputAudioCombo.getItems().size() > 0 ) {
                                    controls.outputAudioCombo.getSelectionModel().select(0);
                                }
                            }

                            controls.audioSourceStatus.setText( SELECTION_DONE );
                        }
                    );
                }
                catch ( Throwable t ) {
                    logger.error( t.getMessage(), t );
                }
            }
        );
    }

    private void iConnectToRegistry() {
        controls.registrarStatus.setText(PENDING);
        executor.submit(
            () -> {
                try {
                    final String server = iGetServer( config.getRegistryHostNameAndPort() );
                    final int port = iGetPort( config.getRegistryHostNameAndPort() );
                    final RestUtil.RestReturn ret = RestUtil.sendGet(
                        "http",
                        server,
                        port,
                        "/test",
                        2000);
                    logger.info("iConnectToRegistry(), lRestReturn: [{}]", ret );
                    Platform.runLater(() -> controls.registrarStatus.setText(ret.getStatus() == 200 ? REGISTRAR_REACHABLE : REGISTRAR_UNREACHABLE));
                } catch (Throwable t) {
                    logger.info("t:{}", t.getMessage());
                    Platform.runLater(() -> controls.registrarStatus.setText(REGISTRAR_UNREACHABLE));
                }
            }
        );
    }

    private synchronized void iRegisterMyself() {

        if ( soundBridgeConnected.get() ) {
            logger.info( "registration ignored" );
            return;
        }

        try {
            final String server = iGetServer( controls.registrarTextField.getText() );
            final int port = iGetPort( controls.registrarTextField.getText() );
            final String userName = licenseInfo.getUserCompleteName();
            final String groupName = controls.groupTextField.getText();
            final FSEnum fsEnum = controls.samplingFrequencyCombo.getSelectionModel().getSelectedItem();
            final Integer windowLength = controls.windowLengthCombo.getSelectionModel().getSelectedItem();

            if ( !licenseInfo.isLegitimate() ) {
                logger.info (LICENSE_KEY_IS_INVALID);
                return;
            }

            if ( licenseInfo.isExpired() ) {
                logger.info (LICENSE_KEY_EXPIRED);
                return;
            }

            controls.registrationStatus.setText(PENDING);
            executor.submit(
                () -> {
                    try {
                        final SubscriberRegistrationInfo registrationInfo =  SubscriberRegistrationInfo
                            .create()
                            .setClientId( config.getClientId() )
                            .setClientName( userName )
                            .setClientLanIPAddress( SystemUtil.getAValidLocalIP( "", true ) )
                            .setBufferLengthInMSec( windowLength )
                            .setAudioSamplingFrequency( fsEnum )
                            .setGroupName( groupName )
                            .setLicenseNumber( licenseInfo.getLicenseNumber() );

                        final RestUtil.RestReturn ret = RestUtil.sendPut(
                            "http",
                            server,
                            port,
                            "/register",
                            1000,
                            registrationInfo);

                        logger.info("iRegisterMyself(), registrationInfo: [{}]", GsonUtil.serialize( registrationInfo ) );
                        logger.info("iRegisterMyself(), lRestReturn: [{}]", ret );
                        Platform.runLater(() -> controls.registrationStatus.setText(
                            ret.getStatus() == 200
                                ? String.format( REGISTERED, config.getClientId().substring( 0, 6 ) )
                                : SESSION_NOT_STARTED) );
                    }
                    catch (Throwable t) {
                        logger.warn("t:{}", t.getMessage());
                        Platform.runLater(() -> controls.registrationStatus.setText(SESSION_NOT_STARTED));
                    }
                }
            );
        }

        catch ( Throwable t ) {
            logger.error( t.getMessage(), t );
        }
    }

    private void iUnRegisterMyself() {

        final String server = iGetServer( controls.registrarTextField.getText() );
        final int port = iGetPort( controls.registrarTextField.getText() );

        try {
            final SubscriberRegistrationInfo registrationInfo =  SubscriberRegistrationInfo
                .create()
                .setClientId( config.getClientId() );

            final RestUtil.RestReturn ret = RestUtil.sendPut(
                "http",
                server,
                port,
                "/unregister",
                1000,
                registrationInfo);

            logger.info("iUnRegisterMyself(), registrationInfo: [{}]", GsonUtil.serialize( registrationInfo ) );
            logger.info("iUnRegisterMyself(), lRestReturn: [{}]", ret );

            Platform.runLater(() -> controls.registrationStatus.setText(SESSION_NOT_STARTED) );
        }
        catch (Throwable t) {
            logger.warn("t:{}", t.getMessage());
            Platform.runLater(() -> controls.registrationStatus.setText(SESSION_NOT_STARTED));
        }
    }

    private void iReloadGroup() {

        try {

            if ( soundBridgeConnected.get() ) {
                logger.info( "reloading group ignored" );
                return;
            }

            if ( !licenseInfo.isLegitimate() ) {
                logger.info (LICENSE_KEY_IS_INVALID);
                return;
            }

            if ( licenseInfo.isExpired() ) {
                logger.info (LICENSE_KEY_EXPIRED);
                return;
            }

            final String server = iGetServer( controls.registrarTextField.getText() );
            final int port = iGetPort( controls.registrarTextField.getText() );
            final SubscribersQueryInfo queryInfo = SubscribersQueryInfo
                .create()
                .setGroupName( controls.groupTextField.getText() );

            executor.submit(
                () -> {
                    try {
                        final RestUtil.RestReturn ret = RestUtil.sendPut(
                            "http",
                            server,
                            port,
                            "/get-registrations",
                            1000,
                            queryInfo);

                        logger.info("lRestReturn: [{}]", ret );

                        final SubscriberRegistrationInfoMap subscriberRegistrationInfoMap =
                            GsonUtil.deSerialize(
                                ret.getContent(),
                                SubscriberRegistrationInfoMap.class );

                        logger.info("SubscriberRegistrationInfoMap: [{}]", GsonUtil.serialize( subscriberRegistrationInfoMap ) );

                        Platform.runLater(
                            () -> {
                                controls.groupCombo.getItems().clear();

                                int indexToSelect = -1;
                                int index = 0;

                                for( final Map.Entry<String, SubscriberRegistrationInfo> registrationInfoEntry : subscriberRegistrationInfoMap.getMap().entrySet() ) {
                                    controls.groupCombo.getItems().add( new ComboSubscriberRegistrationInfo(registrationInfoEntry.getValue() ) );
                                    if ( config.getFriendId().equals( registrationInfoEntry.getValue().getClientId() ) ) {
                                        indexToSelect = index;
                                    }
                                    index++;
                                }

                                if ( indexToSelect != -1 ) {
                                    controls.groupCombo.getSelectionModel().select( indexToSelect );
                                }
                                else if ( controls.groupCombo.getItems().size() > 0 ) {
                                    controls.groupCombo.getSelectionModel().select(0);
                                }

                                if ( controls.groupCombo.getItems().isEmpty() ) {
                                    controls.groupFoundStatus.setText(GROUP_NOT_FOUND);
                                }
                                else {
                                    controls.groupFoundStatus.setText(ret.getStatus() == 200 ? GROUP_FOUND : GROUP_NOT_FOUND);
                                }
                            }
                        );
                    }
                    catch ( Throwable t ) {
                        controls.groupFoundStatus.setText(GROUP_NOT_FOUND);
                        logger.error ( t.getMessage(), t );
                    }
                }
            );
        }
        catch (Throwable t) {
            logger.warn("t:{}", t.getMessage());
            Platform.runLater(() -> controls.groupFoundStatus.setText(GROUP_NOT_FOUND));
        }
    }

    private void iConnect() {
        try {
            if ( controls.groupCombo.getSelectionModel().getSelectedItem() == null ||
                 controls.inputAudioCombo.getSelectionModel().getSelectedItem() == null ||
                 controls.outputAudioCombo.getSelectionModel().getSelectedItem() == null ||
                 controls.samplingFrequencyCombo.getSelectionModel().getSelectedItem() == null ||
                 controls.windowLengthCombo.getSelectionModel().getSelectedItem() == null ) {
                logger.warn( "ignoring command, some controls are not loaded yet" );
                return;
            }

            if ( soundBridgeConnected.get() ) {
                logger.warn( "ignoring command, already connected" );
                return;
            }

            controls.connectionStatus.setText(CONNECTING);
            controls.reloadGroupButton.requestFocus();

            final SubscriberRegistrationInfo subscriberRegistrationInfo = controls.groupCombo.getSelectionModel().getSelectedItem().subscriberRegistrationInfo;
            final boolean useLocalAddress = controls.useLocalAddressesCheckBox.isSelected();
            final boolean fastAudioDeviceDiscovery = controls.fastAudioDeviceDiscoveryCheckBox.isSelected();
            final ComboMixerInfo inputAudioMixerInfo = controls.inputAudioCombo.getSelectionModel().getSelectedItem();
            final ComboMixerInfo outputAudioMixerInfo = controls.outputAudioCombo.getSelectionModel().getSelectedItem();
            final FSEnum fsEnum = controls.samplingFrequencyCombo.getSelectionModel().getSelectedItem();
            final Integer windowLength = controls.windowLengthCombo.getSelectionModel().getSelectedItem();

            executor.submit(
                () ->
                {
                    try {
                        final int status = soundBridge.start(
                            false,
                            false,
                            false,
                            false,
                            false,
                            true,
                            "-use-rest", "false",
                            "-log-level", startupConfig.getLogLevel() );

                        logger.info("status:{}", status);
                        Preconditions.checkArgument(status == 0);

                        soundBridge.getSoundBridgeConfig()
                            .setProbePeriodInSec( 5 )
                            .setStationType(StationBuilder.StationTypeEnum.full_duplex)
                            .setAudioSourceTypeEnum(StationBuilder.AudioSinkOrSourceTypeEnum.hw)
                            .setAudioSinkTypeEnum(StationBuilder.AudioSinkOrSourceTypeEnum.hw)
                            .setThisSideIP(SystemUtil.getAValidLocalIP("", true))
                            .setAudioSamplingFrequency( fsEnum )
                            .setBufferLengthInMSec( windowLength )
                            .setAudioSourceHash( inputAudioMixerInfo.mixerInfo.hash )
                            .setAudioSinkHash( outputAudioMixerInfo.mixerInfo.hash )
                            .setFastAudioDeviceTest( fastAudioDeviceDiscovery )
                            .setOtherSideIP( useLocalAddress
                                ? subscriberRegistrationInfo.getClientLanIPAddress()
                                : subscriberRegistrationInfo.getClientWanIPAddress() );

                        logger.info(
                            "before startStation(), soundBridge.getSoundBridgeConfig():{}",
                            GsonUtil.serialize(soundBridge.getSoundBridgeConfig()));

                        soundBridge.startStation();

                        Platform.runLater(
                            () -> {
                                // register for the second time with both the
                                // windowLength and the samplingFreq, as other side will benefit of knowing
                                iRegisterMyself();

                                // mark status in the controls
                                controls.connectionStatus.setText(CONNECTED);
                                controls.connectButton.requestFocus();
                                latestSoundBridgeConnectionTime = System.currentTimeMillis();
                                soundBridgeConnected.set(true);
                                iUpdateControlsEnableState();
                            }
                        );
                    }
                    catch (Throwable t) {
                        logger.error(t.getMessage(), t);
                        Platform.runLater(
                            () -> {
                                controls.connectionStatus.setText(CONNECTION_ERROR);
                                controls.connectButton.requestFocus();
                                soundBridgeConnected.set(false);
                                iUpdateControlsEnableState();
                            }
                        );
                    }
                }
            );
        }

        catch (Throwable t) {
            logger.error(t.getMessage(), t);
        }
    }

    private void iDisconnect() {
        try {
            if ( ! soundBridgeConnected.get() ) {
                logger.warn( "ignoring command, already disconnected" );
                return;
            }

            controls.connectionStatus.setText(DISCONNECTING);
            controls.reloadGroupButton.requestFocus();

            executor.submit(
                () -> {
                    try {
                        soundBridge.stopStation();

                        Platform.runLater(
                                () -> {
                                    try {
                                        controls.connectionStatus.setText(NOT_CONNECTED);
                                        controls.connectButton.setText(MAKE_CONNECTION);
                                        controls.connectButton.requestFocus();
                                        soundBridgeConnected.set(false);
                                        iUpdateControlsEnableState();
                                    }
                                    catch (Throwable t) {
                                        logger.error(t.getMessage(), t);
                                    }
                                }
                        );
                    }

                    catch ( Throwable t1 ) {
                        logger.error(t1.getMessage(), t1);
                        Platform.runLater(
                            () -> {
                                try {
                                    controls.connectionStatus.setText(CONNECTION_ERROR);
                                    controls.connectButton.setText(MAKE_CONNECTION);
                                    controls.connectButton.requestFocus();
                                    iUpdateControlsEnableState();
                                } catch (Throwable t2) {
                                    logger.error(t2.getMessage(), t2);
                                }
                            }
                        );
                    }
                }
            );
        }
        catch (Throwable t1) {
            logger.error(t1.getMessage(), t1);
        }
    }

    private void iUpdateControlsEnableState() {
        final boolean connected = soundBridgeConnected.get();
        for (Control node : ImmutableList.of(
                controls.groupTextField,
                controls.licenseKeyButton,
                controls.reloadGroupButton,
                controls.reFetchAudioSourcesButton,
                controls.registerUserButton,
                controls.groupCombo,
                controls.inputAudioCombo,
                controls.outputAudioCombo,
                controls.samplingFrequencyCombo,
                controls.windowLengthCombo,
                controls.useLocalAddressesCheckBox,
                controls.fastAudioDeviceDiscoveryCheckBox ) ) {
            node.disableProperty().set ( connected );
        }
        controls.connectButton.setText( connected ? DISCONNECT : MAKE_CONNECTION );
    }

    private void iInitializeControlTooltips() {
        controls.registerUserButton.setTooltip(new Tooltip("show yourself and your group name, so your friend and you may find each other"));
        controls.licenseKeyButton.setTooltip(new Tooltip("use this to load a new license file"));
        controls.reloadGroupButton.setTooltip(new Tooltip("reload those that have the same group name as you"));
        controls.reFetchAudioSourcesButton.setTooltip(new Tooltip("reload any compatible audio hardware present in your system"));
        controls.connectButton.setTooltip(new Tooltip("connect to (or disconnect from) the person showing in the group combo"));
        controls.registrarTextField.setTooltip(new Tooltip("the server allowing people to connect with each other"));
        controls.groupTextField.setTooltip(new Tooltip("the name of your group, so you may only see people in it, and only them could see you"));
        controls.groupCombo.setTooltip(new Tooltip("those in your same group that are registered to the network"));
        controls.windowLengthCombo.setTooltip(new Tooltip("look here for the lowest value that does not produce breaks in the sound you hear,\nyou'll need to disconnet and reconnect"));
        controls.useLocalAddressesCheckBox.setTooltip(new Tooltip("use when needing to connect with somebody else inside your own LAN/Wifi"));
    }
    
    private void iInitializeControlsFromConfig() {
        controls.registrarTextField.setText( config.getRegistryHostNameAndPort() );
        controls.groupTextField.setPromptText( "type a group name not shorter than 6 characters" );

        final List<FSEnum> validFS = soundBridge.getValidFS();
        logger.info( "validFS:{}", GsonUtil.serialize( validFS ) );
        controls.samplingFrequencyCombo.getItems ( ).clear ( );
        controls.samplingFrequencyCombo.getItems ( ).addAll ( validFS );
        controls.samplingFrequencyCombo.getSelectionModel ( ).select ( 0 );
        controls.samplingFrequencyCombo.setValue( config.getSoundBridgeConfig().getAudioSamplingFrequency() );

        controls.windowLengthCombo.getItems ( ).clear ( );
        controls.windowLengthCombo.getItems ( ).addAll ( ImmutableList.of( 5, 10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 150, 200 ) );
        controls.windowLengthCombo.getSelectionModel ( ).select ( 0 );
        controls.windowLengthCombo.setValue( config.getSoundBridgeConfig().getBufferLengthInMSec() );

        controls.reFetchAudioSourcesButton.setText( RELOAD_INPUT );
        controls.connectButton.setText(MAKE_CONNECTION);
        controls.connectionStatus.setText( NOT_CONNECTED );
        controls.audioSourceStatus.setText( SELECTION_DISCOVERY_PENDING );

        controls.registrarStatus.setText(NOT_STARTED);

        controls.registerUserButton.setText(START_SESSION);
        controls.registrationStatus.setText(SESSION_NOT_STARTED);

        controls.groupFoundStatus.setText(NOT_STARTED);
        controls.groupCombo.getItems().clear();
        controls.reloadGroupButton.setText(RELOAD_GROUP);

        controls.fastAudioDeviceDiscoveryCheckBox.setSelected( config.getSoundBridgeConfig().isFastAudioDeviceTest() );
        controls.useLocalAddressesCheckBox.setSelected( config.isConnectIntoLocalAddresses() );
        controls.groupTextField.setText( config.getGroupName() );
        controls.licenseTextField.setPromptText( "will close soon without a valid license key" );
        controls.licenseKeyButton.setText(IMPORT_license_KEY);
        controls.licenseStatus.setText( "[license key is missing]" );
    }

    private void iAttachListenersToControls() {
        controls.licenseKeyButton.setOnAction( e -> iLoadLicenseKey());

        controls.registerUserButton.setOnAction( e -> {
            registerUserOnTimer.set(true);
            iRegisterMyself();
            executor.submit(
                () -> {
                    SystemUtil.sleepMsec( 1_000 );
                    Platform.runLater(this::iReloadGroup);
                }
            );
        });

        controls.reloadGroupButton.setOnAction(e ->
        {
            executor.submit(
                this::iReloadGroup
            );
        });

        controls.reFetchAudioSourcesButton.setOnAction( e -> iLoadAudioDriversInfo());

        controls.connectButton.setOnAction(
            e -> {
                if ( soundBridgeConnected.get() ) {
                    iDisconnect();
                }
                else {
                    iConnect();
                }
            }
        );
    }

    private static int iGetPort(String serverAndPort ) {
        return Integer.parseInt( serverAndPort.split(":")[1] );
    }

    private static String iGetServer(String serverAndPort ) {
        return serverAndPort.split(":")[0];
    }

    private GridPane iCreatePane() {

        final GridPane gridPane = new GridPane();

        gridPane.setGridLinesVisible(false);

        {
            gridPane.setPadding(new Insets(20, 20, 20, 20));
            gridPane.setVgap(10);
            gridPane.setHgap(30);

            final ColumnConstraints column1 = new ColumnConstraints();
            column1.setHgrow( Priority.ALWAYS );
            final ColumnConstraints column2 = new ColumnConstraints();
            column2.setHgrow( Priority.ALWAYS );
            final ColumnConstraints column3 = new ColumnConstraints();
            column3.setHgrow( Priority.ALWAYS );
            final ColumnConstraints column4 = new ColumnConstraints( 250 );
            column4.setHgrow( Priority.ALWAYS );
            gridPane.getColumnConstraints().addAll( column1, column2, column3, column4 );
        }

        final AtomicInteger row = new AtomicInteger(0);

        {
            iFillNextRow( row, gridPane, new Label("Registration server"), new Label(), controls.registrarTextField, controls.registrarStatus );
        }

        final int buttonWidth = 130;

        {
            iControlButtonSize(controls.licenseKeyButton, buttonWidth );
            iFillNextRow( row, gridPane, new Label("Your License"), controls.licenseKeyButton, controls.licenseTextField, controls.licenseStatus );
        }

        {
            iControlButtonSize(controls.registerUserButton, buttonWidth );
            iFillNextRow( row, gridPane, new Label("Your Group name"), controls.registerUserButton, controls.groupTextField, controls.registrationStatus );
        }

        iAddSeparator( gridPane, row.incrementAndGet() );

        {
            iControlButtonSize( controls.reloadGroupButton, buttonWidth );
            iControlComboSize( controls.groupCombo);
            iFillNextRow( row, gridPane, new Label("Others in your same group"), controls.reloadGroupButton, controls.groupCombo, controls.groupFoundStatus);
        }

        iAddSeparator( gridPane, row.incrementAndGet() );

        {
            //iFillNextRow( row, gridPane, new Label("Fast device discovery (experimental)"), new Label(), controls.fastAudioDeviceDiscoveryCheckBox, new Label() );
            iControlButtonSize( controls.reFetchAudioSourcesButton, buttonWidth );
            iControlComboSize( controls.inputAudioCombo );
            iFillNextRow( row, gridPane, new Label("Audio input"), controls.reFetchAudioSourcesButton, controls.inputAudioCombo, controls.audioSourceStatus);
            iControlComboSize( controls.outputAudioCombo );
            iFillNextRow( row, gridPane, new Label("Audio output"), new Label(), controls.outputAudioCombo, new Label() );
        }

        iAddSeparator( gridPane, row.incrementAndGet() );

        {
            iFillNextRow( row, gridPane, new Label("Sampling frequency"), new Label(), controls.samplingFrequencyCombo, new Label() );
            iFillNextRow( row, gridPane, new Label("Window length in milliseconds"), new Label(), controls.windowLengthCombo, new Label() );
        }

        {
            iFillNextRow(row, gridPane, new Label("Use a local network connection"), new Label(), controls.useLocalAddressesCheckBox, controls.connectionTime);
        }

        iAddSeparator( gridPane, row.incrementAndGet() );

        {
            iControlButtonSize( controls.connectButton, 150 );
            iFillNextRow( row, gridPane, new Label("Connect audio"), new Label(), controls.connectButton, controls.connectionStatus );
        }

        return gridPane;
    }

    private void iControlButtonSize( Button r, int width ) {
        r.setPrefSize( width, 30 );
    }

    private void iControlComboSize(ComboBox r ) {
        r.setPrefSize( 450, 30 );
    }

    private static void iFillNextRow(
        AtomicInteger row,
        GridPane gridPane,
        Node nodeOnColumn0,
        Node nodeOnColumn1,
        Node nodeOnColumn2,
        Node nodeOnColumn3 ) {

        row.incrementAndGet();
        gridPane.add( nodeOnColumn0, 0, row.get() );
        gridPane.add( nodeOnColumn1, 1, row.get() );
        gridPane.add( nodeOnColumn2, 2, row.get() );
        gridPane.add( nodeOnColumn3, 3, row.get() );
    }

    private static void iAddSeparator(GridPane gridPane, int row) {
        final Separator separator = new Separator();
        gridPane.add(separator, 0, row);
        GridPane.setColumnSpan(separator, 4);
    }
}


