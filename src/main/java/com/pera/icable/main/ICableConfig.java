/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.icable.main;

import com.google.gson.annotations.Expose;
import com.pera.soundbridge.restinfo.SoundBridgeConfig;

public class ICableConfig
{
    @Expose private String registryHostNameAndPort;
    @Expose private String clientId;
    @Expose private String friendId;
    @Expose private String groupName;
    @Expose private SoundBridgeConfig soundBridgeConfig;
    @Expose private boolean connectIntoLocalAddresses;

    public static ICableConfig create()
    {
        return new ICableConfig();
    }

    private ICableConfig() {
        registryHostNameAndPort = "";
        clientId = "";
        friendId = "";
        soundBridgeConfig = SoundBridgeConfig.create();
        connectIntoLocalAddresses = false;
        groupName = "";
    }

    public SoundBridgeConfig getSoundBridgeConfig() {
        return soundBridgeConfig;
    }

    public ICableConfig setSoundBridgeConfig( SoundBridgeConfig soundBridgeConfig ) {
        this.soundBridgeConfig = soundBridgeConfig;
        return this;
    }

    public String getRegistryHostNameAndPort() {
        return registryHostNameAndPort;
    }

    public ICableConfig setRegistryHostNameAndPort( String registryHostNameAndPort ) {
        this.registryHostNameAndPort = registryHostNameAndPort;
        return this;
    }

    public String getClientId() {
        return clientId;
    }

    public ICableConfig setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public boolean isConnectIntoLocalAddresses() {
        return connectIntoLocalAddresses;
    }

    public ICableConfig setConnectIntoLocalAddresses(boolean connectIntoLocalAddresses) {
        this.connectIntoLocalAddresses = connectIntoLocalAddresses;
        return this;
    }

    public String getGroupName() {
        return groupName;
    }

    public ICableConfig setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public String getFriendId() {
        return friendId;
    }

    public ICableConfig setFriendId(String friendId) {
        this.friendId = friendId;
        return this;
    }
}
