/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.icable.main;

import com.pera.icable.javafx.ICableApplication;
import com.pera.util.LogUtil;

public class ICableMain
{
    public static void main( String[] args )
    {
        LogUtil.initLog4j();
        System.exit(
            ICableApplication.main( false, args )
        );
    }
}
