/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 */

package com.pera.icable.cmdline;

public class ICableStartupConfigInfo
{
    private boolean helpRequested;
    private boolean versionRequested;
    private String logLevel;

    public static ICableStartupConfigInfo create()
    {
        return new ICableStartupConfigInfo();
    }

    public static final ICableStartupConfigInfo DEFAULT = create();

    private ICableStartupConfigInfo()
    {
        helpRequested = false;
        versionRequested = false;
        logLevel = "warn";
    }

    public boolean isVersionRequested() {
        return versionRequested;
    }

    public ICableStartupConfigInfo setVersionRequested(boolean value) {
        versionRequested = value;
        return this;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public ICableStartupConfigInfo setLogLevel(String value) {
        logLevel = value;
        return this;
    }

    public ICableStartupConfigInfo setHelpRequested(boolean value)
    {
        helpRequested = value;
        return this;
    }

    public boolean getHelpRequested()
    {
        return helpRequested;
    }
}

