/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 */

package com.pera.icable.cmdline;

import com.pera.util.CommandLineParserUtil;
import com.pera.util.SystemUtil;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import java.io.IOException;
import java.io.PrintStream;

public class ICableCommandLineParser
{
    private final OptionParser parser;

    public static ICableCommandLineParser create()
    {
        return new ICableCommandLineParser();
    }

    private ICableCommandLineParser()
    {
        parser = new OptionParser();
    }

    public ICableStartupConfigInfo getConfigFromArgs(
        String[] args,
        ICableStartupConfigInfo defaultValues )
    {
        final OptionSpec<Void> help = parser
            .accepts( "help", "prints this help and exits" )
            .forHelp();

        final OptionSpec<Void> version = parser
            .accepts( "version", "prints the app version and exits" );

        final OptionSpec<String> logLevel = parser
            .accepts( "log-level", "sets the log level, it should be any of these: {error, warn, info, debug, trace}" )
            .withRequiredArg()
            .ofType( String.class )
            .defaultsTo( defaultValues.getLogLevel() );

        final OptionSet options = parser.parse( args );

        final ICableStartupConfigInfo startupConfig = ICableStartupConfigInfo.create();

        startupConfig.setHelpRequested( options.has( help ) );
        startupConfig.setVersionRequested( options.has( version ) );
        startupConfig.setLogLevel( options.valueOf( logLevel ) );

        return startupConfig;
    }

    public void printHelp ( PrintStream ps ) throws IOException
    {
        ps.println();
        ps.println("ICable - the long wire - carries sound across the internet." );
        ps.println();
        ps.println("git version: " + SystemUtil.getGitVersion( true ) );
        ps.println();
        ps.println("Usage: ");
        ps.println();
        parser.formatHelpWith( new CommandLineParserUtil.HelpFormatter() );
        parser.printHelpOn( ps );
        ps.println();
        ps.println( "All command line options should start either with a single or double hyphen," );
        ps.println( "followed by the minimum number of letters that singles out an option." );
    }
}

