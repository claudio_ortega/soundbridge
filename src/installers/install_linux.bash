#!/usr/bin/env bash

JRE_DIR=jre1.8.0_172

if [ -e ${JRE_DIR} ];
then
    echo "jre dir is already installed at "${JRE_DIR}
else
    echo "installing jre dir at "${JRE_DIR}
    curl --location-trusted -O https://bitbucket.org/claudio_ortega/soundbridge-public/downloads/jre-8u172-linux-x64.tar.gz
    tar xf jre-8u172-linux-x64.tar.gz
fi

cp env_linux.bash env.bash
echo "installation is done."