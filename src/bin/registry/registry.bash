#!/usr/bin/env bash
cd $(dirname ${0})
source env.bash
exec ${JAVA_BIN} -Xmx300m -Xms300m -cp *.jar com.pera.registry.main.RegistryMain ${*}
cd -
