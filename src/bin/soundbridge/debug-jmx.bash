#!/usr/bin/env bash
cd $(dirname ${0})
source env.bash
exec ${JAVA_BIN}  -Xmx300m -Xms300m \
    -Dcom.sun.management.jmxremote \
    -Dcom.sun.management.jmxremote.port=9010 \
    -Dcom.sun.management.jmxremote.local.only=false \
    -Dcom.sun.management.jmxremote.authenticate=false \
    -Dcom.sun.management.jmxremote.ssl=false \
    -cp *.jar \
    com.pera.soundbridge.main.SoundBridgeMain ${*}

cd -
