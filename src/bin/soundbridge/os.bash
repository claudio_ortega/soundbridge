#!/usr/bin/env bash
cd $(dirname ${0})
source env.bash
exec ${JAVA_BIN} -Xmx300m -Xms300m -cp *.jar com.pera.soundbridge.main.SoundBridgeMain --use-rest --config-file=config-os.properties
cd -
