
### windows/linux os installation

#### 1. download OS/GUI from bitbucket

create the soundbridge directory

    mkdir -p soundbridge/bin
    mkdir -p soundbridge/jre
    
    cd soundbridge/bin
    (download latest from https://bitbucket.org/claudio_ortega/soundbridge/downloads)
    (download latest from https://bitbucket.org/claudio_ortega/soundbridge-gui/downloads)


#### 2. get a copy of java 

copy whole java jre 1.8.._65 (or later) directory under the jre subdirectory

    cd ../jre
    cp -r <path-to-jre> .
    
    
#### 3. modify app.* to point to the controlled path for java/w executable(s)

in all app*.ba* scripts, replace 
    
    java -> ../jre.../java

and

    javaw -> ../jre.../javaw 


#### 4. modify config file for the OS app

os.bash uses this config file:

    ./config-os.properties

fill in these: 

    station_type=os
    this_side_port=5130
    other_side_port=5130

    
#### 4. modify config file for the GUI app

the gui app uses this config file:

    gui/config-gui.properties

in that file fill in these entries: 

    rest_server_os=(fill in OS ip)
    rest_port_os=9090
    rest_server_vs=(fill in VS ip)
    rest_port_vs=9090
    
    # for bcm pin chart numbers, see
    #    http://pinout.xyz
    # or execute on a console in the r-pi
    #    pinout <enter>
    bcm_index_vhf1=17
    bcm_index_vhf2=27
    bcm_index_auxiliary_1=5
    bcm_index_auxiliary_2=6
    bcm_index_auxiliary_3=23
    bcm_index_auxiliary_4=24
    poll_status_period_sec=10


#### 5. start OS/GUI

windows
    
    in different command line terminals
    
    cd soundbridge/bin
    ./os.bat

    cd soundbridge/bin
    ./gui.bat
       
       
OS-X/linux    
    
    in different command line terminals
    
    cd soundbridge/bin
    ./os.bash

    cd soundbridge/bin
    ./gui.bash
       
    

        

    
