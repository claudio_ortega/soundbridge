## Abstract 

There are currently several applications carrying sound over the internet.
The apps show delays in the order of 100 mSec.

SoundBridge has been tested and proven to produce delays as low as 17 mSec in each direction
in between analog audio sources in Redwood City and Fremont, CA


## Measurement setup

We injected a burst of 50 cycles of a 1KHz sinusoidal repeated at 1 second rate, 
from a Rigol DG1022Z signal generator into an analog to USB professional grade audio interface
AUDIOBOX USB96 attached to a computer (1). This side is located in Redwood City, 
running SoundBridge in full duplex mode.
The audio line is sampled monoaural at 48KHz with 16 bits of dynamic range.
 
The opposite end consists on a second copy of SoundBridge running on another linux machine (2) 
located on the cloud somewhere in Fremont CA in a linode.com cloud data center.  
That copy of SoundBridge is running in loopback mode, which is designed to simply bounce back 
the received data to the sender in machine (1).

We attached channels 1/2 of a Rigol DS1104Z scope on both analog input and output lines
of the audio interface. See fig 2.
  

## Measurements

### Network round trip delay 

The application network round trip delay is by definition the delay measured in between 
the point in time a packet with samples leaves the app in one side,
and the moment that the same packet gets received back, 
after bouncing against a copy of Soundbridge running in loopback mode on the other side of the link.
In that mode the app and merely copies any received packet back into the network.

The network round trip delay is measured on the copy of SoundBridge running 
in full-duplex mode on one of the sides.
The measurement is obtained by keeping track of the serial numbers inside 
special delay probe packets, sent over the link every 10 seconds.

The round trip delay distribution is mono-modal and shows a peak around 9 mSecs. 
See file delay-rwc-fremont.xls in the same directory as this file.
PS: you may compare the round trip delay with Buenos Aires, which shows its peak at around 220 mSec,
almost 25 times longer. See fig 3.


### Total round trip delay

The total round trip delay is the most important measurable characteristic.
It is by definition the delay measured in between input and output lines on
the USB interface attached to machine (1).

Refer to fig 1. The yellow trace is the input, the cyan trace is the output. The scale is 5mSec/Div.
We measured consistently a total round trip delay of about 35 mSec, which suggests 
about 17 mSec delay in each direction.


## Conclusions

In this test we have proven that two copies of SoundBridge 
are capable of sustaining a CD quality sound link with 17 mSec of delay in each direction
over a public internet connection in between Redwood City and Fremont CA.

This is not an estimation, but a direct measurement.
The measured delay is equivalent to the one under normal conditions 
in between two sound sources set 6 meters apart. 


### Appendix A - How to reproduce the test

SoundBridge version 

    download 3e045e3a77 from:
    https://bitbucket.org/claudio_ortega/soundbridge/downloads/distribution-2020-08-10-06.10.08-3e045e3a77.zip
    

Computer (1) full duplex mode

    Dell Optiplex 7010 SF, 16GB, 4 cores, running Linux Ubuntu 20.02
    ./soundbridge.bash -conf=full-duplex.properties --use-rest 
    (config file is in the same directory with this file)
    
    The round trip delay stats were obtained after a few minutes:
    curl --location --request GET 'http://<IP-address>:9090/get-line-delay-stats'
    
    
Computer (2) network loopback mode

    Linux VM in the cloud, 1GB RAM, 2 CPU cores, running Ubuntu 20.02    
    ./soundbridge.bash -conf=config-network-loopback.properties  
    (config file is in the same directory with this file) 


Lab equipment setup

    Equipment:
        Oscilloscope: Rigol DS1104Z, channels 1/2 are used
        Signal Generator: Rigol DG1022Z, output #1, sinusoidal burst 1KHz, 1 sec, 50 cycles, 50 Ohms out impedance
        Audio interface: PreSonus AudioBox USB96, control positions: 
        channel 1:0%, channel 2:75%, mixer:100%, main: 0dB, phones:0%
    
    Connections: 
        USB cable between Computer (1) and audio interface USB jack
        Generator channel #1 into audio interface input 2 
        TRS 1/4" between audio interface input 2 and scope channel #1
        TS 1/4" between audio interface Main Output Left and scope channel #2 
    
    
    
### Appendix B - Figures


![fig1](fig1.jpg)

Fig 1 - Analog end to end round trip delay measurement showing 35 mSec in total.


![fig2](fig2.jpg)

Fig 2 - Close-up into the connections.


![fig3](fig3.png)

Fig 3 - Network round trip delay distribution Redwood City-Fremont in mSec.






