##### git related commands

    git log 
    git stash 
    git stash list
    git stash pop <list-index>
    git pull --rebase
    git branch
    git checkout <branch-name>
    git describe --tags
    git reset --hard origin/<branch-name>


##### to get a list of running soundbridge apps 

    ./app.bash -ch

or 

    jps
    
or 

    ps -ef | fgrep Main

##### to execute a command that you want to persist after the shell is closed
 
    nohup <cmd> & 

##### list files open per process

    ls -l /proc/<pid>/fd

##### list open ports per process

    netstat -lntu
    
##### kill a process    
    
    kill <pid>    

##### show end of log file during app execution

    tail -f <file-name>   
    
##### show all files used by a process 

    lsof -p <pid>
    
##### handle app lifecycle with supervisorctl 

    sudo supervisorctl status all 
    sudo supervisorctl start all 
    sudo supervisorctl stop all 
            
##### copy files with scp

    scp ./build/distributions/bin/soundbridge-uber-all-1.0-SNAPSHOT.jar pi@rpi-poplar-3:~/soundbridge
    scp pi@rpi-poplar-3:~/soundbridge/*.log  ./some-dir-this-side
    
    
##### unzip/untar file

    gunzip x.gz
    tar xvf x.tar
