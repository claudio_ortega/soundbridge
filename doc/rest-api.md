The documentation for the REST api is under this link:

    https://documenter.getpostman.com/view/2049051/T1LJnVFY

Here is an example of a configuration message's body for a VS station:

    
    {
      "configMap": {
        "this_side_ip": "",
        "audio_source_type": "hw",
        "other_side_ip": "192.168.0.203",
        "audio_source_hash": "",
        "other_side_port": "5130",
        "buffer_queue_length": "100",
        "audio_sink_type": "hw",
        "buffer_length_ms": "20",
        "station_type": "vs",
        "this_side_port": "5131",
        "audio_sink_hash": ""
      },
      "valid": true
    }
    
