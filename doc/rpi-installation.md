
### r-pi VS installation

#### 1. Follow rpi installation guide

generic

    https://www.raspberrypi.org/documentation/installation/installing-images/README.md

from MAC

    https://www.raspberrypi.org/documentation/installation/installing-images/mac.md

from Windows

    https://www.raspberrypi.org/documentation/installation/installing-images/windows.md


- install full version with desktop and recommended software, if in mac OS, use balena
  Etcher to copy the .img file into a flash memory card of 16 GB

    https://downloads.raspberrypi.org/raspbian_full_latest
    
- download java JDK 8 for ARM 32 bits (as JRE seems missing)
  - goto https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html
  - download jdk-8u261-linux-arm32-vfp-hflt.tar.gz into ~/soundbridge/jre
   
#### 2. Start the first local session.

- raspi-config, interfacing options, add ssh


    sudo raspi-config


- write down the mac(s)


    ifconfig | fgrep ether


- augment wifi information


    sudo nano /etc/wpa_supplicant/wpa_supplicant.conf

    <bof>
    network={
        ssid="HomeOneSSID"
        psk="passwordOne"
        priority=1
        id_str="homeOne"
    }
    
    network={
        ssid="HomeTwoSSID"
        psk="passwordTwo"
        priority=2
        id_str="homeTwo"
    }
    <eof>


#### 3. Start the first ssh session. 

- add the MACs to the DHCP server config for a permanent IP assignment
- connect using ssh for the first time

    
#### 4. Installation

unzip latest distribution from

    https://bitbucket.org/claudio_ortega/soundbridge/downloads/
   
under 
   
    /home/pi/soundbridge


#### 5. Change config file for the VS app.

vs.bash uses this config file:

    /home/pi/soundbridge/config-vs.properties

fill in the IP address for the OS in the key:

    station_type=vs
    this_side_port=5130
    other_side_ip=(fill in the OS ip)
    other_side_port=5130

inside env.bash, fill in the path for the java executable found under bin jre dir,
   ie:

    vi env.bash
    ..
    export JAVA_BIN=./jdk1.8.0_261/bin/java
    

#### 6. Remove private keys from ssh directory 

remove the ssh directory if it exists

    rm -rf ~/.ssh
    
    
    
#### 7. Startup/watchdog mechanism for VS app

App start at boot time and watchdog keep alive mechanism are both based on supervisord

-- references

    http://supervisord.org/
    https://serversforhackers.com/c/process-monitoring-with-supervisord
    https://www.youtube.com/watch?v=8ZVf0T1I2vw
    
-- installation:

    sudo apt-get update
    sudo apt-get install -y supervisor
    sudo service supervisor start

-- initial config:

    keep file /etc/supervisor/supervisord.conf with the default content
     
    sudo cp ./soundbridge.conf /etc/supervisor/conf.d
    sudo supervisorctl reload
    sudo supervisorctl reread
    sudo supervisorctl update 
    
-- day to day operation:

    sudo supervisorctl status all 
    sudo supervisorctl stop all 
    sudo supervisorctl start all 

    
#### 8. Backup. Clone the micro SD-card

-- copy the content of the SD card into a backup file

    diskutil list (identify diskX at the sd card reader)
    diskutil unmountDisk /dev/diskX
    sudo dd if=/dev/diskX of=~/Desktop/raspbian_backup.img

-- copy the backup file content onto another SD card

    diskutil list (identify diskX at the sd card reader)
    diskutil unmountDisk /dev/diskX
    sudo dd if=~/raspbian_backup.img of=/dev/diskX    

    
    
    
