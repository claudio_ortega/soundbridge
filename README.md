## SoundBridge - Radio voice and PTT operation over IP ##


### Introduction ###

SoundBridge operates aviation control tower VHF radios over the internet.


The aviation VHF radio interface
--------------------------------

The aviation VHF radio interface comprises two aspects:

1. Voice: a half duplex audio channel, normally band limited into the 300Hz-3KHz band

2. Operate TX: also known as PTT (Push To Talk) control. 
This control is activated by pushing on a button located on the control tower operator's microphone.
This button activates the transmitter (located in the tower's surroundings). 
The transmitter is shut off when the button is released, and the VHF receiver is simultaneously activated.

SoundBridge is an extension mechanism for these two interfaces over an internet link.
It allows an operator sitting in a control tower in airport A (virtual tower) 
to operate the radio link in between the control tower in airport B (real tower) and aircraft
land or take off from airport B, usually in any point of the globe with internet access.

All the described operation is half duplex. 
However, SoundBridge can be used in full duplex mode via some initial file configuration.


The two stations
----------------

The solution works over two named stations: the Operator Station (OS) and the VHF Station (VS).
Both OS and VS comprise an audio interface that samples the audio channel at 8KHz, 
with an ADC resolution of 16 bits. In both cases the sound samples are sent over UDP 
directly to the other side. In its present state, the solution has no addressing capabilities, 
ie: it is a one to one communication channel.


Half duplex operation
---------------------

In its initial state and before any PTT-on is received, 
the application captures sound on the audio interface VS-VHF on the VS side and 
sends it continuously over UDP to the OS side.

The OS takes the sound samples out from the received UDP packets, converts them back 
into an analog signal in the operator's audio interface.

Imagine the PTT is now activated in the OS operator's console, typically by a GUI app 
that uses SB's REST API with the OS station. 
The OS starts sending sound from the OS's sound interface into the UDP connection towards the VS.
Upon reception of this UDP traffic, the VS simultaneously activates the PTT interface in the VHF radio, 
while converting the samples inside the UDP packets back as an analog signal into the VS station's 
VHF radio interface.

When the command PTT-off from the operator console is received by the OS over rest API, 
then all comes back into its original state, in which the sound samples flow over UDP 
from the VS towards the OS.


Full duplex operation
---------------------
The app works also in full duplex via a configuration change.
In this configuration there is no PTT functionality and both stations 
have identical functionality. There is no concept of OS/VS anymore.
Both sides are same, and the sound captures in each side is sent to the other side
over UDP and continuously. There is no eco cancellation, so headsets should be used at least 
in ones of the two sides.


Configuration
-------------

The configuration for both OS/VS resides in one single config file under the src/bin directory.
This configuration might be applied either at start time from the file, or later on via a REST api.


REST API
--------

Both OS/VS accept REST api commands, including: turn-up/shutdown, config read/write, 
PTT on/off, enable vhf1/vhf2, miscellaneous on/off switches, log level change, and status reading. 

The PTT on/off is only accepted by the OS, the miscellaneous switches and vh1/2 only by the VS, 
and any other command is accepted by both OS and VS. 
A more detailed definition of the rest API may be found in doc/rest-api.md


Dependencies
------------

Both OS/VS need a rest API client to produce the commands. SoundBridge-GUI is one instance of such client.
The GUI app is a sibling repo of this one, hosted in https://bitbucket.org/claudio_ortega/soundbridge-gui

The OS/VS may work either in a raspberry PI or on a windows or MAC PC. 
However, the typical deployment will likely be as the OS in a windows machine and the VS in a raspberry PI 3b+.

All components: GUI, OS and VS, need Java JRE 1.8.0_121-b13 or later to build and execute.

The VS uses a USB to analog audio AD/DA adapter, reference follows:
 
Inno-Maker USB Sound Card 5V Powered PCM2912 Mini USB Sound Card DAC Decoder and Record Board
https://smile.amazon.com/gp/product/B07D1ZYMZ9/ref=oh_aui_search_asin_title?ie=UTF8&psc=1


Delay characterization
----------------------
The application measures round trip delay continuously. The histogram of such delay can be obtained
as part of the OS status REST api, for both application and network debugging purposes.
The app has been tested successfully in its half duplex mode over links with 250 mS of 
round trip delay (Arg/USA). 
This app provides an audio link with very little delay when used in between points with 
small enough network delay, like in between any two points of the SF BAy Area. 
See the measurement of such application in [here](doc/delay-tests/delay-full-duplex.md).


Pending Issues
--------------

All pending issues are in here:
https://bitbucket.org/claudio_ortega/soundbridge/issues


How to get SoundBridge
----------------------

The SoundBridge documentation, source code, app downloading, and record of any known outstanding issues are all hosted at: 
https://bitbucket.org/claudio_ortega/soundbridge




