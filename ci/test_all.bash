#!/usr/bin/env bash

# use --debug for more output
./gradlew test --rerun-tasks \
   -Dtest.single=TestRegressionSuite*
