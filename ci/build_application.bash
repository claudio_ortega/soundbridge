#!/usr/bin/env bash

./gradlew \
    generateGitVersion \
    build \
    removeGitVersionFile \
    generatePackage \
    packageDistribution \
    -x test