#!/usr/bin/env bash

echo "BITBUCKET_REPO_OWNER=["${BITBUCKET_REPO_OWNER}"]"
echo "BITBUCKET_REPO_SLUG=["${BITBUCKET_REPO_SLUG}"]"

for next_file in ./build/distributions/*.zip
do
    echo "attempting to upload the file:"${next_file}

    echo "current branch is: ["${BITBUCKET_BRANCH}"]"

    curl_command="curl -X POST \
        --user ${BITBUCKET_REPO_OWNER}:${APP_PASSWORD} \
        https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads \
        --form files=@${next_file}"

    curl_command="curl -X POST \
        --user ${BITBUCKET_REPO_OWNER}:${APP_PASSWORD} \
        https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/soundbridge-public/downloads \
        --form files=@${next_file}"

    echo "will now execute upload command: ["${curl_command}"]"

    ${curl_command}

    if [[ ($? -eq 0) ]]; then
        echo "upload succeeded for the file:"${next_file}
    else
        echo "upload failed for the file:"${next_file}
        exit -1
    fi
done


